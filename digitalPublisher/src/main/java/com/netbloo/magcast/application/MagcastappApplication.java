package com.netbloo.magcast.application;

import com.netbloo.magcast.helpers.MCErrorLog;
import com.netbloo.magcast.helpers.MCServicesHelper;

import android.app.Application;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import io.branch.referral.Branch;

public class MagcastappApplication extends Application {
	public void onCreate() {
		super.onCreate();

		// Branch logging for debugging
		Branch.enableLogging();

		// Branch object initialization
		Branch.getAutoInstance(this);

		// Setup handler for uncaught exceptions.
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable e) {
				handleUncaughtException(thread, e);
			}
		});
		
		CookieSyncManager.createInstance(this);
		CookieManager.getInstance().setAcceptCookie(true);
		CookieManager.setAcceptFileSchemeCookies(true);

		MCServicesHelper.getInstance().startServicesAtApplicationCreated(this);
	}

	public void handleUncaughtException(Thread thread, Throwable e) {
		Log.d("exception", thread.toString());

		e.printStackTrace(); // not all Android versions will print the stack
								// trace automatically
		
		MCErrorLog.sendError(e, getApplicationContext());

		System.exit(1); // kill off the crashed app
	}
}
