package com.netbloo.magcast.entities;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.netbloo.magcast.constants.PageType;

public class Thumbnail extends BaseEntity {
	public String imagePath;
	public int orderNo;
	public int pageType;
	
	public Thumbnail(XmlPullParser parser) {
		super();
		
		String ns = null;
		try {
			parser.require(XmlPullParser.START_TAG, ns, "thumbnail");
			while (parser.next() != XmlPullParser.END_TAG) {
	            if (parser.getEventType() != XmlPullParser.START_TAG) {
	                continue;
	            }
	            String name = parser.getName();
	            if (name.equals("order")) {
	            	String orderNoString = readTag(parser, "order");
	            	this.orderNo = Integer.parseInt(orderNoString);
	            } else if (name.equals("image")) {
	            	this.imagePath = readTag(parser, "image");
	            } else if (name.equals("type")) {
	            	String pageTypeString = readTag(parser, "type");
	            	if (pageTypeString.equals("magvertiserAd")) {
	            		this.pageType = PageType.ADVERTISEMENT;
	            	}
	            } else {
	                skip(parser);
	            }
	        }
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
