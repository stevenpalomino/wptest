package com.netbloo.magcast.entities;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.netbloo.magcast.helpers.MCXmlParser;

public class BaseEntity extends MCXmlParser {
	// Processes title tags in the feed.
    protected String readTag(XmlPullParser parser, String tagName) throws IOException, XmlPullParserException {
    	String ns = null;
        parser.require(XmlPullParser.START_TAG, ns, tagName);
        String string = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, tagName);
        return string;
    }
    
    // For the tags title and summary, extracts their text values.
    protected String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
}
