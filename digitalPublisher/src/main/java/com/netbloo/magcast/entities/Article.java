package com.netbloo.magcast.entities;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class Article extends BaseEntity {
	private String author;
	private String fullPath;
	private String summary;
	private String title;
	
	public Article(XmlPullParser parser, String fullPath) {
		super();
		
		this.fullPath = fullPath;
		
		String ns = null;
		try {
			parser.require(XmlPullParser.START_TAG, ns, "article");
			while (parser.next() != XmlPullParser.END_TAG) {
	            if (parser.getEventType() != XmlPullParser.START_TAG) {
	                continue;
	            }
	            String name = parser.getName();
	            if (name.equals("author")) {
	            	this.author = readTag(parser, "author");
	            } else if (name.equals("summary")) {
	            	this.summary = readTag(parser, "summary");
	            } else if (name.equals("title")) {
	            	this.title = readTag(parser, "title");
	            } else {
	                skip(parser);
	            }
	        }
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getAuthor() {
		return author;
	}
	public String getFullPath() {
		return fullPath;
	}
	public String getSummary() {
		return summary;
	}
	public String getTitle() {
		return title;
	}
}
