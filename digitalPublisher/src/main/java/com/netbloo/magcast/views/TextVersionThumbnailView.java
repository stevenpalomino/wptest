package com.netbloo.magcast.views;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.entities.Article;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TextVersionThumbnailView extends RelativeLayout {
	
	private TextView authorTextView;
	private ImageView bulletImageView;
	private ImageView bulletOnImageView;
	private TextView titleTextView;
	
	public TextVersionThumbnailView(Context context) {
		super(context);
	}

	public TextVersionThumbnailView(Article article, Context context) {
		super(context);
		
		LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.text_version_thumbnail_view, this);
		
		this.authorTextView = (TextView) findViewById(R.id.authorTextView);
		this.bulletImageView = (ImageView) findViewById(R.id.bulletImageView);
		this.bulletOnImageView = (ImageView) findViewById(R.id.bulletOnImageView);
		this.titleTextView = (TextView) findViewById(R.id.titleTextView);
		
		this.authorTextView.setText(article.getAuthor());
		this.titleTextView.setText(article.getTitle());
	}

	public void select() {
		this.bulletImageView.setVisibility(INVISIBLE);
		this.bulletOnImageView.setVisibility(VISIBLE);
	}
	
	public void deselect() {
		this.bulletImageView.setVisibility(VISIBLE);
		this.bulletOnImageView.setVisibility(INVISIBLE);
	}
}
