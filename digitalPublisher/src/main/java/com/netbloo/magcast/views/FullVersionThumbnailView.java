package com.netbloo.magcast.views;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.bebaibcebe.badabebaibcebe.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FullVersionThumbnailView extends RelativeLayout {
	private ImageView coverImageView;
	private TextView pageNumberTextView;
	
	public FullVersionThumbnailView(Context context) {
		super(context);
	}

	public FullVersionThumbnailView(String coverImagePath, int pageNumber, Context context) {
		super(context);
		
		LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.full_version_thumbnail_view, this);
		
		this.coverImageView = (ImageView) findViewById(R.id.coverImageView);
		this.pageNumberTextView = (TextView) findViewById(R.id.pageNumberTextView);
		
		this.pageNumberTextView.setText(Integer.toString(pageNumber));
		
		try {
			FileInputStream fis = new FileInputStream(coverImagePath);
			Bitmap bitmap = BitmapFactory.decodeStream(fis);
			fis.close();
			this.coverImageView.setImageBitmap(bitmap);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public void setOnImageViewClickListener(View.OnClickListener listener) {
		this.coverImageView.setOnClickListener(listener);
	}
}
