package com.netbloo.magcast.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class MaxWidthRelativeLayout extends RelativeLayout {
	
	private int mMaxWidth = 710;

	public MaxWidthRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MaxWidthRelativeLayout(Context context) {
		super(context);
	}

	public MaxWidthRelativeLayout(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	    //get measured height
	    if(getMeasuredWidth() > mMaxWidth * getResources().getDisplayMetrics().density){
	        setMeasuredDimension(mMaxWidth * (int)getResources().getDisplayMetrics().density, getMeasuredHeight());
	    }
	}
	
	

}
