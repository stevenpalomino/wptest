package com.netbloo.magcast.views;

import com.netbloo.magcast.helpers.PlayWebViewVideoHelper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.webkit.WebView;

public class VideoWebView extends WebView {
	private String baseUrl;
	private String data;
	private String mimeType;
	private String encoding;
	private String historyUrl;
	
	public VideoWebView(Context context) {
		super(context);
	}

	public VideoWebView(Context context, AttributeSet attrs, int defStyle,
			boolean privateBrowsing) {
		super(context, attrs, defStyle, privateBrowsing);
		
		registerBroadcastReceivers();
	}

	public VideoWebView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		registerBroadcastReceivers();
	}
	
	public VideoWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		registerBroadcastReceivers();
	}
	
	private void registerBroadcastReceivers() {
		LocalBroadcastManager.getInstance(getContext()).registerReceiver(
				this.activityOnGoBack,
				new IntentFilter(PlayWebViewVideoHelper.ON_GO_BACK));
		
		LocalBroadcastManager.getInstance(getContext()).registerReceiver(
				this.activityOnPauseReceiver,
				new IntentFilter(PlayWebViewVideoHelper.ON_PAUSE));
		
		LocalBroadcastManager.getInstance(getContext()).registerReceiver(
				this.activityOnResumeReceiver,
				new IntentFilter(PlayWebViewVideoHelper.ON_RESUME));
	}

	@Override
	public void loadDataWithBaseURL(String baseUrl, String data,
			String mimeType, String encoding, String historyUrl) {
		this.baseUrl = baseUrl;
		this.data = data;
		this.mimeType = mimeType;
		this.encoding = encoding;
		this.historyUrl = historyUrl;

		super.loadDataWithBaseURL(baseUrl, data, mimeType, encoding, historyUrl);
	}

	@Override
	public void reload() {
		if (this.data == null) {
			super.reload();
		} else {
			this.loadDataWithBaseURL(this.baseUrl, this.data, this.mimeType, this.encoding, this.historyUrl);
		}
	}
	
	private BroadcastReceiver activityOnGoBack = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (VideoWebView.this.canGoBack()) {
				VideoWebView.this.goBack();
    		}
		}
	};
	
	private BroadcastReceiver activityOnPauseReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			VideoWebView.this.onPause();
		}
	};
	
	private BroadcastReceiver activityOnResumeReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (VideoWebView.this.getContentHeight() != 0) {
				VideoWebView.this.onResume();
    		}
		}
	};
	
	
}
