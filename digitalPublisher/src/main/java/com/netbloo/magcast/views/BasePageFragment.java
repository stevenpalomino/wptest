package com.netbloo.magcast.views;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.activities.BasePreviewActivity;
import com.netbloo.magcast.activities.SubscriptionActivity;
import com.netbloo.magcast.activities.WebViewActivity;
import com.netbloo.magcast.constants.MCMagazineStatus;
import com.netbloo.magcast.constants.MCProductStatus;
import com.netbloo.magcast.customInterfaces.BillingActivity;
import com.netbloo.magcast.helpers.HtmlHelper;
import com.netbloo.magcast.helpers.MCAlertDialog;
import com.netbloo.magcast.helpers.MCAnalyticsHelper;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.helpers.PlayWebViewVideoHelper;
import com.netbloo.magcast.helpers.UrlActionHelper;
import com.netbloo.magcast.models.MCMagazine;
import com.netbloo.magcast.models.MCProduct;
import com.netbloo.magcast.models.MCSubscriptionProduct;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.PopupWindow;

@SuppressLint({ "ValidFragment", "SetJavaScriptEnabled" })
abstract public class BasePageFragment extends Fragment {
	protected String pageUrl;
	protected Activity parentActivity;
	protected ViewGroup rootView;
	protected WebView webView;
	
	public BasePageFragment() {
		super();
	}
	
	@Override
	public void onAttach(Activity activity) {
		this.parentActivity = activity;
		
		super.onAttach(activity);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if (context instanceof Activity){
			this.parentActivity = (Activity) context;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		
		rootView = (ViewGroup) inflater.inflate(
				R.layout.preview_page_view, container, false);
		
		this.pageUrl = getArguments().getString("url");
		this.parentActivity = getActivity();
		webView = (WebView) rootView.findViewById(R.id.webView);
		
		webView.setBackgroundColor(Color.argb(1, 0, 0, 0));
		/*  */
		/*webView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});*/

		WebSettings webSettings = webView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setBuiltInZoomControls(true);
		webSettings.setDisplayZoomControls(false);
		webSettings.setAllowFileAccess(true);

		BasePreviewActivity parentActivity = (BasePreviewActivity) this.parentActivity;
		PlayWebViewVideoHelper playWebVideoHelper = parentActivity
				.getPlayVideoHelper();
		playWebVideoHelper.setWebViewForVideoPlay(this.parentActivity, webView, null);
		
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				
				webViewFinishedToLoad();
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				BasePreviewActivity parentActivity = (BasePreviewActivity) BasePageFragment.this.parentActivity;
				ViewGroup rootView = BasePageFragment.this.rootView;

				Log.d("magcast url load", url);

				UrlActionHelper actionHelper = new UrlActionHelper();
				if (!actionHelper.performAction(url, parentActivity, rootView)) {
					return super.shouldOverrideUrlLoading(view, url);
				}

				return true;
			}
		});
		
		return rootView;
	}

	@Override
	public void onDestroyView() {
		if (this.webView != null) {
			this.webView.loadUrl("about:blank");
		}
		
		super.onDestroyView();
	}

	@Override
	public void onResume() {
		super.onResume();
		
		if ((this.pageUrl != null) && (this.parentActivity != null) && (webView != null)) {
			// Read data from html file
			loadPageHtml();
		}
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		
		
		super.onViewCreated(view, savedInstanceState);
	}

	protected void loadPageHtml() {
		try {
			InputStream fis = this.parentActivity.getContentResolver().openInputStream(
					Uri.parse(this.pageUrl));
			byte[] buffer = new byte[1024];
			StringBuffer cachedData = new StringBuffer("");
			int length = 0;
			while ((length = fis.read(buffer)) != -1) {
				cachedData.append(new String(buffer, 0, length));
				buffer = new byte[1024];
			}
			fis.close();

			String htmlContent = cachedData.toString();
			htmlContent = this.prepareHtmlContent(htmlContent);

			webView.loadDataWithBaseURL(this.pageUrl, htmlContent, "text/html", "utf-8", null);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	protected String prepareHtmlContent(String htmlContent) {
		return HtmlHelper.prepareAppHtml(htmlContent);
	}
	
	protected void webViewFinishedToLoad() {
		
	}
}
