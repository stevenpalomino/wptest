package com.netbloo.magcast.views;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.activities.HomeViewActivity;
import com.netbloo.magcast.activities.SubscriptionActivity;
import com.netbloo.magcast.constants.MCMagazineStatus;
import com.netbloo.magcast.constants.MCProductStatus;
import com.netbloo.magcast.helpers.HttpRequestHelper;
import com.netbloo.magcast.helpers.MCAnalyticsHelper;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.models.MCMagazine;
import com.netbloo.magcast.models.MCProduct;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;

public class ProductInfoView extends RelativeLayout {
	private MCProduct product;

	private Context parentContext;

	private BroadcastReceiver productsUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String productId = intent.getStringExtra("productId");
			if ((product != null)
					&& (productId.equals(product.getProductId()))) {
				productStatusUpdated();
			}
		}
	};

	// View components
	private ProgressBar activityIndicator;
	private Button archiveButton;
	private Button buyButton;
	private ImageView coverImageView;
	private Button closeButton;
	private Button downloadButton;
	private ImageView freeIssue;
	private ImageView specialIssue;
	private ImageView privateIssue;
	private WebView productSummary;
	private ProgressBar progressView;
	private Button subscriptionButton;
	private Button viewButton;

	public ProductInfoView(Context context) {
		super(context);

		LayoutInflater layoutInflater = (LayoutInflater) this.getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.product_info_view, this);

		this.parentContext = context;

		this.loadViewComponents();
	}

	public void setOnCloseButtonClickListener(
			View.OnClickListener onCloseButtonClickListener) {
		closeButton.setOnClickListener(onCloseButtonClickListener);
	}

	@SuppressLint("SetJavaScriptEnabled")
	protected void loadViewComponents() {
		activityIndicator = (ProgressBar) findViewById(R.id.activityIndicator);
		archiveButton = (Button) findViewById(R.id.archiveButton);
		buyButton = (Button) findViewById(R.id.buyButton);
		closeButton = (Button) findViewById(R.id.closeButton);
		coverImageView = (ImageView) findViewById(R.id.coverImageView);
		downloadButton = (Button) findViewById(R.id.downloadButton);
		freeIssue = (ImageView) findViewById(R.id.freeIssue);
		specialIssue = (ImageView) findViewById(R.id.specialIssue);
		privateIssue = (ImageView) findViewById(R.id.privateIssue);
		productSummary = (WebView) findViewById(R.id.productSummary);
		progressView = (ProgressBar) findViewById(R.id.progressView);
		subscriptionButton = (Button) findViewById(R.id.subscribeButton);
		viewButton = (Button) findViewById(R.id.viewButton);

		this.progressView.setMax(100);
		
		MCPublisher publisher = MCPublisher.sharedPublisher(this.parentContext);
		MCMagazine magazine = publisher.getMagazine();
		if (magazine.getSubscriptionStatus() == MCMagazineStatus.MCMagazineStatusPurchased) {
			this.subscriptionButton.setVisibility(GONE);
		}

		assignActionsForButtons();

		// Configure web view
		WebSettings webSettings = this.productSummary.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webSettings.setBuiltInZoomControls(false);
		webSettings.setUseWideViewPort(false);
		webSettings.setLoadWithOverviewMode(true);
		webSettings.setAllowFileAccess(true);
		webSettings.setSupportZoom(false);

		HomeViewActivity mainActivity = (HomeViewActivity) this.parentContext;
		mainActivity.getPlayVideoHelper().setWebViewForVideoPlay(mainActivity,
				this.productSummary, this);

		LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(this.productsUpdateReceiver, new IntentFilter(MCProduct.PRODUCT_STATUS_UPDATED));
	}

	private void assignActionsForButtons() {
		this.archiveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				product.archive(parentContext);
			}
		});

		this.buyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				HomeViewActivity parentActivity = (HomeViewActivity)getContext();
				product.purchase(parentActivity.getPlayStoreBillingHelper());
			}
		});

		this.downloadButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				product.download();
			}
		});

		this.subscriptionButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Activity parentActivity = (Activity) parentContext;
				Intent webViewIntent = new Intent(parentActivity,
						SubscriptionActivity.class);
				parentActivity.startActivity(webViewIntent);
			}
		});

		this.viewButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				product.view((Activity) parentContext);
			}
		});
	}

	public void setProduct(MCProduct product) {
		this.product = product;

		new ImageLoadingTask().execute();

		// Setup labels
		String summaryHtml = this.product.getSummary();
		summaryHtml = summaryHtml.replaceAll("\n", "<br/>");
		if (this.product.getVideoHtml().length() != 0) {
			summaryHtml = "<div style=\"text-align: center;\">"
					+ this.product.getVideoHtml() + "</div><p>" + summaryHtml
					+ "</p>";
		}

		summaryHtml = "<html>" +
						"<meta name=\"viewport\" content=\"width=device-width\">" + 
							"<head>" + 
								"<style>" + 
									"body {" + 
										"padding: 0px;" +
										"margin: 0px;" + 
										"font-family: Helvetica;" +
										"font-size: 15px;" + 
										"background:transparent;" +
										"width: 100%" + 
									"}" +
									"@media screen and (min-width: 690px) {" +
										"iframe,video {" + 
											"width:690px !important;" + 
											"height:388px !important;" + 
										"}" + 
									"}" + 
									"@media screen and (max-width: 609px) {" +
										"iframe,video {" + 
											"width:580px !important;" + 
											"height:326px !important;" + 
										"}" + 
									"}" + 
									"@media screen and (max-width: 559px) {" + 
										"iframe,video {" +
											"width:340px !important;" + 
											"height: 191px !important;" + 
										"}" +
									"}" + 
									"@media screen and (max-width: 334px) {" + 
										"iframe,video {" + 
											"width:230px !important;" +
											"height: 129px !important;" + 
										"}" + 
									"}" + 
								"</style>" +
							"</head>" + 
							"<body>" + summaryHtml + "</body>" + 
						"</html>";
		this.productSummary.loadData(summaryHtml, "text/html; charset=utf-8", "utf-8");

		if (this.product.isSpecial()) {
			this.specialIssue.setVisibility(View.VISIBLE);
		} else {
			this.specialIssue.setVisibility(View.INVISIBLE);
		}

		if (this.product.isSubscribersOnly()) {
			this.privateIssue.setVisibility(View.VISIBLE);
		} else {
			this.privateIssue.setVisibility(View.INVISIBLE);
		}
		
		if (this.product.isFree()) {
			this.freeIssue.setVisibility(VISIBLE);
		} else {
			this.freeIssue.setVisibility(INVISIBLE);
		}

		this.productStatusUpdated();
		
		String eventDescription = "Info " + this.product.getName();
		MCAnalyticsHelper.logEvent(eventDescription, true, getContext());

		Map<String,String> map =  new HashMap<String,String>();
		map.put("product cost", product.getRawPrice());
		map.put("currency code", product.getCurrencyCode());

		Log.i(TAG, "~~~~~~~ setProduct: "+map);
//		try {
//			HttpRequestHelper.logEventWithEventCodeAndCustomVars("testCode", map,getContext());
//		} catch (JSONException e) {
//			e.printStackTrace();
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}

	}

	private class ImageLoadingTask extends AsyncTask<Void, Integer, Void> {
		Bitmap bitmap;

		@Override
		protected Void doInBackground(Void... params) {
			this.bitmap = product.getCoverBitmap();

			publishProgress(1);

			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			super.onProgressUpdate(progress);

			if (bitmap != null) {
				coverImageView.setImageBitmap(bitmap);
			}
		}
	}

	private int previousStatus = -1;

	public void productStatusUpdated() {
		if (this.previousStatus != this.product.getStatus()) {
			this.archiveButton.setEnabled(false);
			this.buyButton.setEnabled(false);
			this.downloadButton.setEnabled(false);
			this.viewButton.setEnabled(false);

			this.activityIndicator.setVisibility(View.GONE);
			this.archiveButton.setVisibility(View.GONE);
			this.buyButton.setVisibility(View.GONE);
			this.downloadButton.setVisibility(View.GONE);
			this.progressView.setVisibility(View.GONE);
			this.viewButton.setVisibility(View.GONE);
			
			this.progressView.setProgress(0);
			
			this.downloadButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.layer_button_download));
			this.downloadButton.setTextColor(getResources().getColor(R.color.blue));
			this.downloadButton.setText(R.string.download);
		}

		switch (this.product.getStatus()) {
		case MCProductStatus.MCProductStatusAvailable:
			this.archiveButton.setEnabled(true);
			this.archiveButton.setVisibility(View.VISIBLE);
			this.viewButton.setEnabled(true);
			this.viewButton.setVisibility(View.VISIBLE);
			break;
		case MCProductStatus.MCProductStatusDownloading:
			this.activityIndicator.setVisibility(View.VISIBLE);
			this.downloadButton.setVisibility(View.VISIBLE);
			this.progressView.setVisibility(View.VISIBLE);

			if ((this.product.getDownloaded() != 0)
					&& (this.product.getFileSize() != 0)) {
				int progress = (int) (this.progressView.getMax()
						* this.product.getDownloaded() / this.product
						.getFileSize());
				this.progressView.setProgress(progress);
			}
			
			if (this.previousStatus != this.product.getStatus()) {
				this.downloadButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.layer_button_dark));
				this.downloadButton.setTextColor(Color.parseColor("#666666"));
			}

			break;
		case MCProductStatus.MCProductStatusNotAvailable:
			if (this.product.isSubscribersOnly()) {
				this.downloadButton.setEnabled(true);
				this.downloadButton.setVisibility(VISIBLE);
			} else {
				this.buyButton.setEnabled(true);
				this.buyButton.setVisibility(VISIBLE);
			}
			break;
		case MCProductStatus.MCProductStatusPurchased:
			this.downloadButton.setEnabled(true);
			this.downloadButton.setVisibility(View.VISIBLE);
			break;
		case MCProductStatus.MCProductStatusPurchasing:
			this.activityIndicator.setVisibility(View.VISIBLE);
			this.buyButton.setVisibility(View.VISIBLE);
			break;
		}

		this.previousStatus = this.product.getStatus();
	}

	@Override
	protected void onDetachedFromWindow() {
		this.productSummary.stopLoading(); 
		this.productSummary.loadUrl("about:blank");
		this.productSummary.reload();
		
		this.productSummary.setWebChromeClient(null);
		this.productSummary.setWebViewClient(null);
		
	    this.removeView(this.productSummary);
	    this.productSummary.removeAllViews();
	    this.productSummary.clearHistory();
	    this.productSummary.onPause();
	    this.productSummary.destroy();
	    this.productSummary = null;
	    
	    HomeViewActivity mainActivity = (HomeViewActivity) this.parentContext;
	    mainActivity.getPlayVideoHelper().setWebViewForVideoPlay(null, null, null);
	    
	    LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(this.productsUpdateReceiver);
		
		super.onDetachedFromWindow();
	}
}
