package com.netbloo.magcast.views;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.constants.MCProductStatus;
import com.netbloo.magcast.helpers.HttpRequestHelper;
import com.netbloo.magcast.helpers.PlayWebViewVideoHelper;
import com.netbloo.magcast.models.MCProduct;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class IssueSmallView extends RelativeLayout {
protected MCProduct product;
	
	BroadcastReceiver productsUpdateReceiver = new BroadcastReceiver() {
		   @Override
		   public void onReceive(Context context, Intent intent) {
			   String productId = intent.getStringExtra("productId");
			   if ((IssueSmallView.this.product != null)
					  && (productId.equals(IssueSmallView.this.product.getProductId()))) {
				   IssueSmallView.this.productStatusUpdated();
			   }
		   }
		};
	
	private Context parentContext;
	
	private boolean productInfoViewDisplayed;

	// View components
	private ProgressBar activityIndicator;
	private ImageView coverImageView;
	private ImageView freeIssue;
	private ImageView infoImageView;
	private ImageView specialIssue;
	private ImageView privateIssue;
	private ProgressBar progressView;
	
	// Image loading
	private ImageLoadingTask imageLoadingTask;
	
	public IssueSmallView(MCProduct product, Context context) {
		super(context);  
		
		this.parentContext = context;
		
		this.loadLayout();
		
		this.setProduct(product);


	}
	
	public Context getParentContext() {
		return parentContext;
	}

	public IssueSmallView(Context context) {
		super(context);
		
		this.loadLayout();
	}
	
	public IssueSmallView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		this.loadLayout();
	}
	
	public IssueSmallView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		this.loadLayout();
	}
	
	protected void finalize() {
		Log.d("fin", "dd");
	}
	
	@Override
	protected void onDetachedFromWindow() {
		LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(this.productsUpdateReceiver);
		
		super.onDetachedFromWindow();
	}

	protected void loadLayout() {
		LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.issue_small_view, this);

		this.loadViewComponents();
	}
	
	protected void loadViewComponents() {
		LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(this.productsUpdateReceiver, new IntentFilter(MCProduct.PRODUCT_STATUS_UPDATED));
		
		activityIndicator = (ProgressBar)findViewById(R.id.activityIndicator);
		coverImageView = (ImageView)findViewById(R.id.coverImageView);
		freeIssue = (ImageView)findViewById(R.id.freeIssue);
		infoImageView = (ImageView) findViewById(R.id.infomag);
		specialIssue = (ImageView)findViewById(R.id.specialIssue);
		progressView = (ProgressBar)findViewById(R.id.progressView);
		privateIssue = (ImageView)findViewById(R.id.privateIssue);
		
		this.activityIndicator.setVisibility(INVISIBLE);
		
		this.progressView.setVisibility(INVISIBLE);
		this.progressView.setMax(100);
	}
	
	public MCProduct getProduct() {
		return product;
	}
	
	public void setProduct(MCProduct product) {
		this.productInfoViewDisplayed = false;
		
		if (this.product != null) {
			this.product.cancelCoverDownload();
		}
		
		this.product = product;
		
		if (this.imageLoadingTask != null) {
			this.imageLoadingTask.cancel(true);
		}
		
		this.coverImageView.setImageBitmap(null);
		this.imageLoadingTask = new ImageLoadingTask();
		this.imageLoadingTask.executeOnExecutor(ImageLoadingTask.THREAD_POOL_EXECUTOR);
		
		if (this.product.isSpecial()) {
			this.specialIssue.setVisibility(VISIBLE);
		} else {
			this.specialIssue.setVisibility(INVISIBLE);
		}
		
		if (this.product.isSubscribersOnly()) {
			this.privateIssue.setVisibility(VISIBLE);
		} else {
			this.privateIssue.setVisibility(INVISIBLE);
		}
		
		if (this.product.isFree()) {
			this.freeIssue.setVisibility(VISIBLE);
		} else {
			this.freeIssue.setVisibility(INVISIBLE);
		}
		
		this.productStatusUpdated();

		coverImageView.setOnClickListener(new OnClickListener() {
		    public void onClick(View v) {
		    	if (IssueSmallView.this.product.getStatus() == MCProductStatus.MCProductStatusAvailable) {
		    		IssueSmallView.this.product.view((Activity)IssueSmallView.this.getContext());
		    	} else {
		    		IssueSmallView.this.displayProductInfo();
		    	}
		    }
		});
		
		infoImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				IssueSmallView.this.displayProductInfo();
			}
		});
	}
	
	private void displayProductInfo() {
		if (!productInfoViewDisplayed) {
    		productInfoViewDisplayed = true;
    		
    		final ProductInfoView productInfo = new ProductInfoView(IssueSmallView.this.parentContext);
	    	final RelativeLayout contentLayout = (RelativeLayout)((Activity)IssueSmallView.this.getContext()).findViewById(R.id.mainContainer);
	    	contentLayout.addView(productInfo);
	    	
	        productInfo.setProduct(IssueSmallView.this.product);
	        
	        PlayWebViewVideoHelper.enableDisableViewGroup(contentLayout, false, productInfo);
	        
	        productInfo.setOnCloseButtonClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					contentLayout.removeView(productInfo);
					
					PlayWebViewVideoHelper.enableDisableViewGroup(contentLayout, true, null);
					
					productInfoViewDisplayed = false;
				}
			});
    	}
	}
	
	private class ImageLoadingTask extends AsyncTask<Void, Integer, Void> {
		Bitmap bitmap;
		
        @Override
        protected Void doInBackground(Void... params) {
        	this.bitmap = IssueSmallView.this.product.getCoverBitmap();
        	if (this.bitmap != null) {
        		this.bitmap.prepareToDraw();
        	}
        	
        	if (this.isCancelled()) {
        		this.bitmap = null;
        	}
 
            return null;
        }
        
        @Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			if ((this.bitmap != null) && !this.isCancelled()) {
            	coverImageView.setImageBitmap(this.bitmap);
            }
			this.bitmap = null;
		}
    }
	
	public void productStatusUpdated() {
		this.progressView.setVisibility(INVISIBLE);
		
		switch (this.product.getStatus()) {
		case MCProductStatus.MCProductStatusAvailable:
			this.activityIndicator.setVisibility(INVISIBLE);
			break;
		case MCProductStatus.MCProductStatusDownloading:
			if (this.activityIndicator.getVisibility() != VISIBLE) {
				this.activityIndicator.setVisibility(VISIBLE);
			}
			
			this.progressView.setVisibility(VISIBLE);
			
			if ((this.product.getDownloaded() != 0) && (this.product.getFileSize() != 0)) {
				int progress = (int) (this.progressView.getMax() * this.product.getDownloaded() / this.product.getFileSize());
				this.progressView.setProgress(progress);
			}
			
			String progressLabelText = "%.2fMB/%.2fMB";
			progressLabelText = String.format(progressLabelText, (float)this.product.getDownloaded() / 1000000, (float)this.product.getFileSize() / 1000000);
			break;
		case MCProductStatus.MCProductStatusNotAvailable:
			this.activityIndicator.setVisibility(INVISIBLE);
			break;
		case MCProductStatus.MCProductStatusPurchased:
			this.activityIndicator.setVisibility(INVISIBLE);
			break;
		case MCProductStatus.MCProductStatusPurchasing:
			this.activityIndicator.setVisibility(INVISIBLE);
			this.activityIndicator.setVisibility(VISIBLE);
			break;
		}
	}
}
