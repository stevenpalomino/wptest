package com.netbloo.magcast.views;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.activities.HomeViewActivity;
import com.netbloo.magcast.activities.ResourcesActivity;
import com.netbloo.magcast.activities.WebViewActivity;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.models.MCMagazine;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class BottomBarView extends LinearLayout {
	private Activity parentActivity;
	
	// View components
	private TextView deviceTokenTextView;
	private Button homeButton;
	private TextView magazineTitleTextView;
	private TextView needHelpTextView;
	private Button optinUrlButton;
	private TextView poweredByTextView;
	private Button resourcesButton;
	private Button settingsButton;
	private RelativeLayout settingsView;
	private TextView summaryTextView;
	private TextView versionTextView;

	public BottomBarView(Context context) {
		super(context);
		
		this.loadView();
	}
	
	public BottomBarView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		this.loadView();
	}
	
	public BottomBarView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		this.loadView();
	}
	
	private void loadView() {
		LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.bottom_bar_view, this);
		
		// Find components
		this.deviceTokenTextView = (TextView) findViewById(R.id.deviceTokenTextView);
		this.homeButton = (Button) findViewById(R.id.homeButton);
		this.magazineTitleTextView = (TextView) findViewById(R.id.magazineTitleTextView);
		this.needHelpTextView = (TextView) findViewById(R.id.needHelpTextView);
		this.optinUrlButton = (Button) findViewById(R.id.optinButton);
		this.poweredByTextView = (TextView) findViewById(R.id.poweredByTextView);
		this.resourcesButton = (Button) findViewById(R.id.resourcesButton);
		this.settingsButton = (Button) findViewById(R.id.settingsButton);
		this.settingsView = (RelativeLayout) findViewById(R.id.settingsView);
		this.summaryTextView = (TextView) findViewById(R.id.summaryTextView);
		this.versionTextView = (TextView) findViewById(R.id.versionTextView);
		
		if (this.settingsView.getLayoutParams() != null) {
			this.settingsView.getLayoutParams().height = 0;
		}
		
		this.summaryTextView.setMovementMethod(ScrollingMovementMethod.getInstance());
		
		updateSettingsContent();
		
		// Click on contact us link
		this.needHelpTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MCPublisher publisher = MCPublisher.sharedPublisher(getContext());
				MCMagazine magazine = publisher.getMagazine();
				
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL  , new String[]{magazine.getSupportEmail()});
				i.putExtra(Intent.EXTRA_SUBJECT, "");
				
				try {
					Context context = getContext();
					PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
					String mailBody = "Version: " + pInfo.versionName + "\nDevice token: " + publisher.getDeviceToken() + "\nDevice ID: "+ publisher.getDeviceId();
					i.putExtra(Intent.EXTRA_TEXT, mailBody);
				} catch (NameNotFoundException e) {
					e.printStackTrace();
				}
				
				try {
				    getContext().startActivity(Intent.createChooser(i, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
				    Toast.makeText(getContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		// Click on magcasting link
		this.poweredByTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String magcastingUrl = "http://www.magcasting.co";
				
				MCPublisher publisher = MCPublisher.sharedPublisher(getContext());
				MCMagazine magazine = publisher.getMagazine();
				String affiliateId = magazine.getAffiliateId();
				if ((affiliateId != null) && (affiliateId.length() > 0)) {
					magcastingUrl += "/partnerid.php?ref=" + affiliateId;
				}
				
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(magcastingUrl));
				getContext().startActivity(browserIntent);
			}
		});
		
		// Prevent touch propagation
		this.settingsView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});
		
		// Setup click listeners for buttons
		this.homeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				BottomBarView.this.parentActivity.finish();				
			}
		});
		
		this.optinUrlButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MCPublisher publisher = MCPublisher.sharedPublisher(BottomBarView.this.getContext());
				MCMagazine magazine = publisher.getMagazine();
				
				Intent webViewIntent = new Intent(BottomBarView.this.parentActivity, WebViewActivity.class);
				webViewIntent.putExtra("htmlContent", magazine.getOptinPage());
				webViewIntent.putExtra("displayTopBar", false);
				webViewIntent.putExtra("scaleToDefaultPageWidth", true);
				BottomBarView.this.parentActivity.startActivity(webViewIntent);
			}
		});
		
		this.resourcesButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {				
				Intent resourcesIntent = new Intent(BottomBarView.this.parentActivity, ResourcesActivity.class);
				BottomBarView.this.parentActivity.startActivity(resourcesIntent);
			}
		});
		
		this.settingsButton.setOnClickListener(new View.OnClickListener() {
			private boolean settingsDisplayed;
			
		    public void onClick(View v) {
		    	if (settingsDisplayed == false) {
		    		settingsView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		    	} else {
		    		ValueAnimator anim = ValueAnimator.ofInt(settingsView.getMeasuredHeight(), 0);
			        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			            @Override
			            public void onAnimationUpdate(ValueAnimator valueAnimator) {
			                int val = (Integer) valueAnimator.getAnimatedValue();
			                ViewGroup.LayoutParams layoutParams = settingsView.getLayoutParams();
			                layoutParams.height = val;
			                settingsView.setLayoutParams(layoutParams);
			            }
			        });
			        anim.start();
		    	}

		    	settingsDisplayed = !settingsDisplayed;
		    }
		});
		
		// Listen to publisher updates
		MCPublisher publisher = MCPublisher.sharedPublisher(this.getContext());
		LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(
				this.publisherUpdatedSuccessFullyReceiver,
				new IntentFilter(MCPublisher.PUBLISHER_DID_UPDATE));
		publisher.setBroadCastManager(LocalBroadcastManager.getInstance(this.getContext()));
	}
	
	private void updateSettingsContent() {
		MCPublisher publisher = MCPublisher.sharedPublisher(this.getContext());
		MCMagazine magazine = publisher.getMagazine();
		if (magazine != null) {
			this.deviceTokenTextView.setText(publisher.getDeviceToken());
			this.magazineTitleTextView.setText(magazine.getName());
			this.summaryTextView.setText(magazine.getTeaser());
			 
			try {
				Context context = this.getContext();
				PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
				this.versionTextView.setText("v" + pInfo.versionName);
			} catch (NameNotFoundException e) {
				e.printStackTrace();
			}
			
			if (!magazine.isDoOptinPage()) {
				this.optinUrlButton.setVisibility(INVISIBLE);
			}
			
			if (publisher.numberOfResources() == 0) {
				this.resourcesButton.setVisibility(INVISIBLE);
			}
		}
	}
	
	public void setParentActivity(Activity parentActivity) {
		this.parentActivity = parentActivity;
		
		if (parentActivity instanceof HomeViewActivity) {
			this.homeButton.setVisibility(INVISIBLE);
		} else {
			this.optinUrlButton.setVisibility(INVISIBLE);
			this.resourcesButton.setVisibility(INVISIBLE);
		}
	}
	
	private BroadcastReceiver publisherUpdatedSuccessFullyReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			updateSettingsContent();
		}
	};
}
