package com.netbloo.magcast.views;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.activities.TextVersionActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.webkit.WebView;

@SuppressLint("ValidFragment")
public class TextVersionPageFragment extends BasePageFragment {
	public TextVersionPageFragment() {
		super();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		LocalBroadcastManager.getInstance(parentActivity).registerReceiver(
				this.fontSizeChangedReceiver,
				new IntentFilter(TextVersionActivity.FONT_SIZE_CHANGED));
	}

	@Override
	protected String prepareHtmlContent(String htmlContent) {
		htmlContent = super.prepareHtmlContent(htmlContent);

		String stylesForVideoSize = "@media screen and (max-width: 1023px) {" +
						"iframe,video {" + 
						"width:476px !important;" + 
						"height: 268px !important;" + 
					"}" + 
				"}" +  
				"@media screen and (max-width: 599px) {" +
					"iframe,video {" + 
						"width:320px !important;" + 
						"height: 191px !important;" + 
					"}" + 
				"}" + 
				"@media screen and (max-width: 350px) {" + 
					"iframe,video {" +
						"width:210px !important;" + 
						"height: 129px !important;" + 
					"}" + 
				"}" + 
				"body { background: #fff;";
		htmlContent = htmlContent.replace("body {", stylesForVideoSize);

		String stylesToLoadFonts = "<style type=\"text/css\">" + "@font-face {"
				+ "font-family: \"LucidaGrande\";"
				+ "src: url(\"file:///android_asset/fonts/LucidaGrande.ttc\");"
				+ "}";
		htmlContent = htmlContent.replace("<style type=\"text/css\">",
				stylesToLoadFonts);
		
		TextVersionActivity activity = (TextVersionActivity) parentActivity;
		htmlContent = htmlContent.replace("font-size: 100%; // content font size", "font-size: " + activity.getCurrentFontSize() + "%; // content font size");
		
		// Fix problem with font loading on 4.2.2
		htmlContent = htmlContent.replace("text-rendering: optimizeLegibility;", "");

		return htmlContent;
	}

	// Listen to publisher notifications

	private BroadcastReceiver fontSizeChangedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			TextVersionActivity activity = (TextVersionActivity) parentActivity;
			WebView webView = (WebView) rootView.findViewById(R.id.webView);;
			if (webView != null) {
				webView.loadUrl("javascript:(function() {" +
						"document.getElementById('content').style.fontSize='" + activity.getCurrentFontSize() + "%';" +
					"})()");
			}
		}
	};
}
