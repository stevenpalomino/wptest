package com.netbloo.magcast.views;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import com.bebaibcebe.badabebaibcebe.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ResourceCell extends RelativeLayout {
	private static HashMap<String, Bitmap> iconsCache;
	
	// Subviews
	private TextView descriptionTextView;
	private ImageView iconImageView;
	private TextView titleTextView;
	
	// Icon image url
	private String iconImageUrl;
	
	public ResourceCell(Context context) {
		super(context);
		
		LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.resource_cell_view, this);
		
		this.descriptionTextView = (TextView) findViewById(R.id.descriptionTextView);
		this.iconImageView = (ImageView) findViewById(R.id.iconImageView);
		this.titleTextView = (TextView) findViewById(R.id.titleTextView);
		
		if (ResourceCell.iconsCache == null) {
			ResourceCell.iconsCache = new HashMap<String, Bitmap>();
		}
	}
	
	public void setDescription(String description) {
		this.descriptionTextView.setText(description);
	}
	
	public void setIconImageUrl(String iconImageUrl) {
		if ((this.iconImageUrl == null)
				|| (this.iconImageUrl.equals(iconImageUrl) == false)) {
			this.iconImageUrl = iconImageUrl;
			this.iconImageView.setImageBitmap(null);
			
			if (ResourceCell.iconsCache.get(iconImageUrl) == null) {
				new ImageLoadingTask().execute();
			} else {
				iconImageView.setImageBitmap(ResourceCell.iconsCache.get(iconImageUrl));
			}
		}
	}
	
	public void setTitle(String title) {
		this.titleTextView.setText(title);
	}
	
	private class ImageLoadingTask extends AsyncTask<Void, Integer, Void> {
		Bitmap bitmap;
		
        @Override
        protected Void doInBackground(Void... params) {
        	try {
    			URL url = new URL(iconImageUrl);
    			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
    			conn.setDoInput(true);
    			conn.connect();
    			InputStream is = conn.getInputStream();
    			this.bitmap = BitmapFactory.decodeStream(is); 
    		} catch (MalformedURLException e) {
    			e.printStackTrace();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
 
            return null;
        }
        
        @Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			if (this.bitmap != null) {
            	iconImageView.setImageBitmap(this.bitmap);
            	ResourceCell.iconsCache.put(iconImageUrl, this.bitmap);
            }
		}
    }
}
