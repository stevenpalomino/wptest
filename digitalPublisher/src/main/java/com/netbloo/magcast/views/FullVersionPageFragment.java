package com.netbloo.magcast.views;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.constants.Constants;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.ImageView;
import android.widget.RelativeLayout;

@SuppressLint({ "ValidFragment", "DefaultLocale" })
public class FullVersionPageFragment extends BasePageFragment {
	ImageView imageView;
	
	public FullVersionPageFragment() {
		super();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		
		this.imageView = (ImageView)this.rootView.findViewById(R.id.imageView);
		DisplayMetrics displayMetrics = getDisplayMetrics();
		RelativeLayout.LayoutParams imageViewLayoutParams = new RelativeLayout.LayoutParams(displayMetrics.widthPixels, Constants.DEFAULT_PAGE_HEIGHT);
		int topMargin = (displayMetrics.heightPixels - Constants.DEFAULT_PAGE_HEIGHT) / 2;
		imageViewLayoutParams.setMargins(0, topMargin, 0, 0);
		this.imageView.setLayoutParams(imageViewLayoutParams);
		
		return view;
	}

	@Override
	protected String prepareHtmlContent(String htmlContent) {
		htmlContent = super.prepareHtmlContent(htmlContent);
		
		htmlContent = htmlContent
				.replace(
						"<meta name=\"viewport\" content=\"width=device-width/\" >",
						"<meta name=\"viewport\" content=\"width=768, target-densitydpi=device-dpi\" >");
		htmlContent = htmlContent.replace(
				"<meta name=\"viewport\" content=\"width=device-width\" >",
				"<meta name=\"viewport\" content=\"width=768, target-densitydpi=device-dpi\" >");
		
		DisplayMetrics displayMetrics = getDisplayMetrics();
		int bodyHeight = (int)(displayMetrics.heightPixels / getScaling());
		
		String marginStyles = "<style type=\"text/css\">" +
				"#contentAlignment {height: " + bodyHeight + "px;display: table-cell; vertical-align: middle;}";
		htmlContent = htmlContent.replace("<style type=\"text/css\">", marginStyles);
		
		// Add one more element to make resizing work properly
		htmlContent = htmlContent.replace("<div id=\"content\"", "<div id='contentAlignment'><div id=\"content\"");
		htmlContent = htmlContent.replace("</body>", "</div></body>");
		
		// Replace old youtube videos tags with iframe
		Pattern pattern = Pattern.compile("(<object width='([0-9]+)px' height='([0-9]+)px'><param name='movie' value='(http://www\\.youtube\\.com[^\']+).+?</object>)");
		Matcher matcher = pattern.matcher(htmlContent);
		String oldYoutubeObject = null;
		String width = null;
		String height = null;
		String url = null;
		String youtubeIframe = null;
		while (matcher.find()) {
			oldYoutubeObject = matcher.group(1);
			width = matcher.group(2);
			height = matcher.group(3);
			url = matcher.group(4);
			
			if (url.contains("youtube")) {
				url = url.replace("/v/", "/embed/");
				url = url.replace("version=3&", "");
				youtubeIframe = "<iframe width='" + width + "px' height='" + height + "px' src='" + url + "' frameborder='0' allowfullscreen></iframe>";
				htmlContent = htmlContent.replace(oldYoutubeObject, youtubeIframe);
			}
		}
		
		// Get page background color
		this.imageView.setBackgroundColor(0xffffffff);
		Pattern backgroundColorPattern = Pattern.compile("#content\\s*\\{[^\\}]*?background-color\\s*:\\s*(.*);");
		Matcher backgroundColorMatcher = backgroundColorPattern.matcher(htmlContent);
		if (backgroundColorMatcher.find()) {
			String backgroundColorString = backgroundColorMatcher.group(1);
			if (backgroundColorString.length() == 4) {
				backgroundColorString = "#" + backgroundColorString.charAt(1) + backgroundColorString.charAt(1) + backgroundColorString.charAt(2) + backgroundColorString.charAt(2) + backgroundColorString.charAt(3) + backgroundColorString.charAt(3); 
			}
			if (backgroundColorString.contains("##")){
				backgroundColorString = backgroundColorString.replace("##", "#");
			}
			if (backgroundColorString != null) {
				Log.d("EE", backgroundColorString);
				int backgroundColor = Color.parseColor(backgroundColorString);
				this.imageView.setBackgroundColor(backgroundColor);
			}
		}
		
		// Get page height to make colourder background while page is loading
		Pattern pageHeightPattern = Pattern.compile("#content\\s*\\{[^\\}]*?height\\s*:\\s*(.*)px;");
		Matcher pageHeightMatcher = pageHeightPattern.matcher(htmlContent);
		if (pageHeightMatcher.find()) {
			String pageHeightString = pageHeightMatcher.group(1);
			int pageHeight = (int)(Float.parseFloat(pageHeightString) * this.getScaling());
			RelativeLayout.LayoutParams imageViewLayoutParams = new RelativeLayout.LayoutParams(displayMetrics.widthPixels, pageHeight);
			int topMargin = (displayMetrics.heightPixels - pageHeight) / 2;
			imageViewLayoutParams.setMargins(0, topMargin, 0, 0);
			this.imageView.setLayoutParams(imageViewLayoutParams);
		}
		
		return htmlContent;
	}

	@Override
	protected void webViewFinishedToLoad() {
		super.webViewFinishedToLoad();
	}

	@Override
	public void onResume() {
		WebSettings webSettings = webView.getSettings();
		webSettings.setUseWideViewPort(true);
		webSettings.setLoadWithOverviewMode(true);
		
		int initialScale = (int)(getScaling() * 100);
		webView.setInitialScale(initialScale);
		
		super.onResume();		
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
	}

	private float getScaling() {
		float scaling = 1;
		DisplayMetrics displayMetrics = getDisplayMetrics();
		scaling = (float)displayMetrics.widthPixels / Constants.DEFAULT_PAGE_WIDTH;
		return scaling;
	}
	
	private DisplayMetrics getDisplayMetrics() {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		this.parentActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		
		return displayMetrics;
	}
}
