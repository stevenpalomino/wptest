package com.netbloo.magcast.models.magazineSettings;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by izmaylav on 4/29/16.
 */
public class MCPrivateSubscription {

    private String promoText;
    private String emailFromName;
    private String emailFromEmail;
    private String registrationWelcomeEmail;
    private String forgotYourPasswordEmail;
    private String outgoingWebhook;

    public MCPrivateSubscription(JSONObject json) {
        try {
            this.promoText = json.getString("promoText");
        } catch (JSONException e) {
        }

        try {
            this.emailFromName = json.getString("emailFromName");
        } catch (JSONException e) {
        }

        try {
            this.emailFromEmail = json.getString("emailFromEmail");
        } catch (JSONException e) {
        }

        try {
            this.registrationWelcomeEmail = json.getString("registrationWelcomeEmail");
        } catch (JSONException e) {
        }

        try {
            this.forgotYourPasswordEmail = json.getString("forgotYourPasswordEmail");
        } catch (JSONException e) {
        }

        try {
            this.outgoingWebhook = json.getString("outgoingWebhook");
        } catch (JSONException e) {
        }
    }

    public String getPromoText() {
        return promoText;
    }

    public String getEmailFromName() {
        return emailFromName;
    }

    public String getEmailFromEmail() {
        return emailFromEmail;
    }

    public String getRegistrationWelcomeEmail() {
        return registrationWelcomeEmail;
    }

    public String getForgotYourPasswordEmail() {
        return forgotYourPasswordEmail;
    }

    public String getOutgoingWebhook() {
        return outgoingWebhook;
    }
}
