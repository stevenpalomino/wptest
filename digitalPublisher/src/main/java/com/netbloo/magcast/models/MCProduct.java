package com.netbloo.magcast.models;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.activities.BasePreviewActivity;
import com.netbloo.magcast.activities.FullVersionActivity;
import com.netbloo.magcast.activities.TextVersionActivity;
import com.netbloo.magcast.constants.Constants;
import com.netbloo.magcast.constants.MCProductDisplayType;
import com.netbloo.magcast.constants.MCProductStatus;
import com.netbloo.magcast.helpers.Decompress;
import com.netbloo.magcast.helpers.HashingHelper;
import com.netbloo.magcast.helpers.HttpRequestHelper;
import com.netbloo.magcast.helpers.MCAlertDialog;
import com.netbloo.magcast.helpers.MCAnalyticsHelper;
import com.netbloo.magcast.helpers.MCPublisher;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import static android.content.ContentValues.TAG;

public class MCProduct extends MCBaseProduct {
	// Constants

	public static final String PRODUCT_WAS_PURCHASED = "productWasPurchased";
	public static final String PRODUCT_WAS_DOWNLOADED = "productWasDownloaded";

	// Notifications

	public static final String PRODUCT_STATUS_UPDATED = "productStatusDidUpdate";
	
	// Product attributes

	private boolean available;
	private String coverPath;
	private Date date;
	private int defaultDisplayType;
	private int displayType;
	private boolean downloading;
	private long downloaded;
	private long fileSize;
	private String formattedDate;
	private boolean free;
	private boolean invisible;
	private String iosProductId;
	private String issueNo;
	private boolean needUpdate;
	private int published;
	private boolean purchased;
	private boolean special;
	private boolean subscribersOnly;
	private String summary;
	private String videoHtml;
	private String errorString;

	// Cover download
	private boolean cancelCoverDownload;

	public MCProduct(JSONObject json, Context context) {
		super(json, context);

		updateProduct(json);
		restoreDownload();
	}
	
	public boolean updateProduct(JSONObject json) {
		boolean wasUpdated = false;
		
		try {
			productId = json.getString("androidProductId");
		} catch (JSONException e) {
			
		}
		if (productId.length() == 0) {
			productId = "falseproductid";
		}
		
		try {
			iosProductId = json.getString("productID");
		} catch (JSONException e) {
			
		}
		
		try {
			available = json.getBoolean("available");
		} catch (JSONException e) {
		}

		try {
			String defaultDisplayType = json.getString("iPhoneDefaultDisplayType");
			if (defaultDisplayType.equals("text")) {
				this.defaultDisplayType = MCProductDisplayType.TEXT;
			} else {
				this.defaultDisplayType = MCProductDisplayType.FULL;
			}
		} catch (JSONException e) {
		}
		
		try {
			String displayType = json.getString("displayType");
			if (displayType.equals("text")) {
				this.displayType = MCProductDisplayType.TEXT;
			} else if (displayType.equals("all")) {
				this.displayType = MCProductDisplayType.ALL;
			} else {
				this.displayType = MCProductDisplayType.FULL;
			}
		} catch (JSONException e) {
		}
		
		try {
			coverPath = json.getString("coverPath");
		} catch (JSONException e) {
		}

		try {
			published = json.getInt("published");
		} catch (JSONException e) {
		}

		try {
			purchased = json.getBoolean("purchased");
		} catch (JSONException e) {
		}

		try {
			coverPath = json.getString("coverPath");
		} catch (JSONException e) {
		}

		String issueDate = null;
		try {
			issueDate = json.getString("issueDate");
		} catch (JSONException e2) {
		}

		if (issueDate != null) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z", Locale.getDefault());
				date = formatter.parse(issueDate);
			} catch (ParseException e1) {
			}

			if (date != null) {
				SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());
				formattedDate = formatter.format(date);
			}
		}

		try {
			free = json.getBoolean("isFree");
		} catch (JSONException e) {
		}

		try {
			invisible = json.getBoolean("isInvisible");
		} catch (JSONException e) {
		}

		try {
			issueNo = json.getString("issueNo");
		} catch (JSONException e) {
		}

		try {
			special = json.getBoolean("isSpecial");
		} catch (JSONException e) {
		}
		
		try {
			subscribersOnly = json.getBoolean("isSubscribersOnly");
		} catch (JSONException e) {
		}

		try {
			summary = json.getString("summary");
		} catch (JSONException e) {
		}
		
		try {
			videoHtml = json.getString("videoHtml");
		} catch (JSONException e) {
		}
		
		return wasUpdated;
	}

	public boolean isAvailable() {
		return available;
	}

	public String getCoverPath() {
		return coverPath;
	}

	public int getDefaultDisplayType() {
		return defaultDisplayType;
	}

	public int getDisplayType() {
		return displayType;
	}

	public Date getDate() {
		return date;
	}

	public boolean isDownloading() {
		return downloading;
	}

	public long getDownloaded() {
		return downloaded;
	}

	public long getFileSize() {
		return fileSize;
	}

	public String getFormattedDate() {
		return formattedDate;
	}

	public boolean isFree() {
		return free;
	}
	
	public String getIosProductId() {
		return iosProductId;
	}

	public String getIssueNo() {
		return issueNo;
	}

	public String getName() {
		return "Mag Issue " + this.getIssueNo();
	}

	public boolean isNeedUpdate() {
		return needUpdate;
	}

	public int getPublished() {
		return published;
	}

	public boolean isPurchased() {
		return purchased;
	}
	
	public void setPurchased(boolean purchased) {
		this.purchased = purchased;
	}

	public boolean isInvisible() { return invisible; }

	public boolean isSpecial() {
		return special;
	}
	
	public boolean isSubscribersOnly() {
		return this.subscribersOnly;
	}

	public String getSummary() {
		return summary;
	}

	public int getStatus() {
		int productStatus = MCProductStatus.MCProductStatusNotAvailable;

		if (this.purchased || this.free) {
			productStatus = MCProductStatus.MCProductStatusPurchased;
		}

		// Check if product is already purchased, free of available by subscription. Then allow to download it, if it wasn't downloaded
		if (this.purchased || this.free) {
			productStatus = MCProductStatus.MCProductStatusPurchased;
		}

		if (this.downloading) {
			productStatus = MCProductStatus.MCProductStatusDownloading;
		} else if (this.isPurchasing()) {
			productStatus = MCProductStatus.MCProductStatusPurchasing;
		} else if (this.isDownloaded()) {
	        productStatus = MCProductStatus.MCProductStatusAvailable;
	    }

		return productStatus;
	}

	public String getPrice() {
		if (this.isSubscribersOnly()) {
			return context.getString(R.string.subscribers_only);
		} else if (this.isSpecial()) {
			return context.getString(R.string.special_issue);
		} else if (this.needUpdate) {
			return context.getString(R.string.update);
		} else if (!this.free
	        && ((this.getStatus() != MCProductStatus.MCProductStatusPurchased) 
	        		&& (this.getStatus() != MCProductStatus.MCProductStatusAvailable)
	        		&& (this.getStatus() != MCProductStatus.MCProductStatusDownloading))) {
	        return super.getPrice();
	    } else if (!this.free 
	    		&& ((this.getStatus() == MCProductStatus.MCProductStatusPurchased) 
	    				|| (this.getStatus() == MCProductStatus.MCProductStatusAvailable)
	    				|| (this.getStatus() == MCProductStatus.MCProductStatusDownloading))) {
	    	return context.getString(R.string.purchased);
	    } else if (!this.free) {
	    	return "";
	    } else {
	    	return context.getString(R.string.free);
	    }
	}
	
	public String getVideoHtml() {
		return videoHtml;
	}
	
	// Should be executed asynchronously
	public Bitmap getCoverBitmap() {
		cancelCoverDownload = false;
		
		Bitmap bitmap = getCoverBitmapFromCache();
		if (bitmap == null || bitmap.getByteCount() < 10000) {
			bitmap = downloadCoverBitmapFromServer();
			int tries = 0;
			while (bitmap.getByteCount() < 10000 && tries < 3) {
				bitmap = downloadCoverBitmapFromServer();
				tries++;
			}
			if (bitmap.getByteCount() < 10000){
				bitmap = null;
			}else{
				saveCoverBitmapInCache(bitmap);
			}
		}
		
		return bitmap;
	}
	
	private Bitmap getCoverBitmapFromCache() {
		Bitmap bitmap = null;
		try {
			String issueCoverFilename = getIssueCoverPath();
			InputStream fis = this.context.openFileInput(issueCoverFilename);
			bitmap = BitmapFactory.decodeStream(fis);
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bitmap;
	}
	
	private Bitmap downloadCoverBitmapFromServer() {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e1) {}
		
		Thread currentThread = Thread.currentThread();
		if (cancelCoverDownload || currentThread.isInterrupted()) {
			cancelCoverDownload = false;
			return null;
		}
		
		Bitmap bitmap = null;
		try {
			URL url = new URL(getCoverPath());
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setDoInput(true);
			conn.connect();
			InputStream is = conn.getInputStream();
			bitmap = BitmapFactory.decodeStream(is);
			is.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bitmap;
	}
	
	private void saveCoverBitmapInCache(Bitmap bitmap) {
		if (bitmap != null) {
			try {
				FileOutputStream fos = this.context.openFileOutput(getIssueCoverPath(), Context.MODE_PRIVATE);
				bitmap.compress(CompressFormat.PNG, 100, fos);
				fos.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
    			e.printStackTrace();
    		}
		}
	}
	
	private String getIssueCoverPath() {
		return "issue" + getIssueNo() + ".png";
	}
	
	public void cancelCoverDownload() {
		cancelCoverDownload = true;
	}
	
	public void setPurchasing(boolean purchasing) {
		this.purchasing = purchasing;
		
		this.sendProductDidUpdateNotification();
	}
	
	public void archive(Context context) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);
		alertDialogBuilder.setTitle(context.getString(R.string.archive_alert_title));
		alertDialogBuilder.setMessage(context
				.getString(R.string.archive_alert_text));
		alertDialogBuilder.setPositiveButton(
				context.getString(R.string.archive),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int which) {
						File file = new File(getDestinationPath());
						deleteRecursive(file);
						
						sendProductDidUpdateNotification();
						
						String eventDescription = "Delete " + getName();
						MCAnalyticsHelper.logEvent(eventDescription, true, getContext());
					}
				});
		alertDialogBuilder.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {}
		});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
	
	private void deleteRecursive(File fileOrDirectory) {
	    if (fileOrDirectory.isDirectory())
	        for (File child : fileOrDirectory.listFiles())
	            deleteRecursive(child);

	    fileOrDirectory.delete();
	}

	public void download() {
		if ((getStatus() != MCProductStatus.MCProductStatusDownloading)
				&& (getStatus() != MCProductStatus.MCProductStatusAvailable)) {
			this.downloaded = 0;
			this.downloading = true;
			
			this.sendProductDidUpdateNotification();

			new Thread(new Runnable() {
				@Override
				public void run() {
					boolean error = false;
					try {
						if (isFree()){
							if (MCPublisher.sharedPublisher(context).isHasBranchUserCode(context)){
								Log.i(TAG, "FREE ISSUE LOG ~~~~~");
								try {
									HttpRequestHelper.logEventWithEventCode("android_free_issue_download", context);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						}

						SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
						Boolean isUsingNewURL = preferences.getBoolean("isUsingNewURL", false);
						String productRequestUrl;
						if (isUsingNewURL){
							productRequestUrl = Constants.NEW_SERVER_PRODUCTS_URL + "/" + getProductId();
						}else{
							productRequestUrl = Constants.SERVER_PRODUCTS_URL + "/" + getProductId();
						}

						JSONObject productInfo = HttpRequestHelper.makePostRequest(productRequestUrl, null, getContext());
						if ((productInfo != null)
								&& (productInfo.getString("status").equals("success") == true)) {
							String contentUrl = productInfo.getJSONObject("result").getString("zipPath");
							downloadIssueWithContentUrl(contentUrl);
						} else {
							error = true;
							if (productInfo != null) {
								errorString = productInfo.getString("result");
							}
						}
					} catch (MalformedURLException e) {
						error = true;
					} catch (IllegalStateException e) {
						error = true;
					} catch (IOException e) {
						error = true;
					} catch(Exception e) { 
						error = true;
				    }
					
					if (error) {
						((Activity) getContext()).runOnUiThread(new Runnable() {
							public void run() {
								if (isSubscribersOnly() && !isPurchased()) {
									MCAlertDialog.showErrorWithText(MCProduct.this.context.getString(R.string.you_should_be_active_subscriber_to_download), getContext());
								} else {
									MCAlertDialog.showErrorWithText(String.format(MCProduct.this.context.getString(R.string.cant_get_issue_from_server)+"\n"+errorString), getContext());
								}
							}
						});
					}

					MCProduct.this.downloading = false;				
					MCProduct.this.sendProductDidUpdateNotification();
				}
			}).start();
		}
	}
	
	public void downloadForFree() {
		this.downloaded = 0;
		this.downloading = true;
		
		this.sendProductDidUpdateNotification();

		new Thread(new Runnable() {
			@Override
			public void run() {
				boolean error = false;
				try {
					String productHash = HashingHelper.md5("mca" + issueNo + "------" + getProductId() + "product");
					String productRequestUrl = Constants.SERVER_PRODUCTS_URL + "/" + getProductId() + "/" + productHash;
					JSONObject productInfo = HttpRequestHelper.makePostRequest(productRequestUrl, null, getContext());
					if ((productInfo != null)
							&& (productInfo.getString("status").equals("success") == true)) {
						String contentUrl = productInfo.getJSONObject("result").getString("zipPath");
						downloadIssueWithContentUrl(contentUrl);
					} else {
						error = true;
					}
				} catch (MalformedURLException e) {
					error = true;
				} catch (IllegalStateException e) {
					error = true;
				} catch (IOException e) {
					error = true;
				} catch(Exception e) { 
					error = true;
			    }
				
				if (error) {
					((Activity) getContext()).runOnUiThread(new Runnable() {
						public void run() {
							MCAlertDialog.showErrorWithText(MCProduct.this.context.getString(R.string.cant_get_issue_from_server), getContext());
						}
					});
				}

				MCProduct.this.downloading = false;				
				MCProduct.this.sendProductDidUpdateNotification();
			}
		}).start();
	}
	
	private void downloadIssueWithContentUrl(String contentUrl) throws IOException {
		// Save info about download for dwonload resuming
		SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(getDownloadKey(), true);
		editor.putBoolean(MCProduct.PRODUCT_WAS_DOWNLOADED, true);
		editor.commit();
		
		String archivePath = MCProduct.this.getDestinationPath()
				+ "archive.zip";

		// Check how much was downloaded already in case of download
		// recovery
		File file = new File(archivePath);
		if (file.exists()) {
			downloaded = file.length();
		}

		// Prepare connection
		URL url = new URL(contentUrl);
		HttpURLConnection connection = (HttpURLConnection) url
				.openConnection();
		connection.setRequestProperty("Range", "bytes="
				+ downloaded + "-");

		connection.setDoInput(true);

		MCProduct.this.fileSize = connection.getContentLength();

		// Create app files directory if needed
		File destinationDirectory = new File(MCProduct.this
				.getDestinationPath());
		if (!destinationDirectory.exists()) {
			destinationDirectory.mkdirs();
		}

		// Download file from server
		InputStream inputStream = connection.getInputStream();
		BufferedInputStream in = new BufferedInputStream(inputStream);
		FileOutputStream fos = (downloaded == 0) ? new FileOutputStream(
				archivePath) : new FileOutputStream(archivePath,
				true);
		@SuppressWarnings("resource")
		BufferedOutputStream bout = new BufferedOutputStream(fos,
				1024);
		byte[] data = new byte[1024];
		int bytesRetrieved = 0;
		int iterationIndex = 0;
		while ((bytesRetrieved = in.read(data, 0, 1024)) >= 0) {
			bout.write(data, 0, bytesRetrieved);
			downloaded += bytesRetrieved;

			++iterationIndex;

			if ((iterationIndex % 100) == 0) {
				MCProduct.this.sendProductDidUpdateNotification();
			}
		}

		try {
			Decompress unarchiver = new Decompress(archivePath,
					MCProduct.this.getDestinationPath());
			unarchiver.unzip();

			File archiveFile = new File(archivePath);
			archiveFile.delete();
			
			editor.remove(getDownloadKey());
			editor.commit();
			
			String eventDescription = "Download " + this.getName();
			MCAnalyticsHelper.logEvent(eventDescription, true, getContext());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void restoreDownload() {
		SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Activity.MODE_PRIVATE);
		boolean hasDownload = preferences.getBoolean(getDownloadKey(), false);
		if (hasDownload && (getStatus() != MCProductStatus.MCProductStatusDownloading)) {
			download();
		}
	}
	
	private String getDownloadKey() {
		return "downloadIssue" + issueNo;
	}

	public boolean isDownloaded() {
		File directory = new File(this.getDestinationPath());
		File[] contents = directory.listFiles();

		File archiveFile = new File(this.getDestinationPath() + "archive.zip");
		if ((contents != null)
				&& (contents.length != 0)
				&& (archiveFile.exists() == false)) {
			return true;
		}

		return false;
	}

	public String getDestinationPath() {
		if (this.context != null) {
			return context.getExternalFilesDir(null).getAbsolutePath() + "/magcastappFiles/" + this.getProductId() + "/";
		} else {
			return "";
		}
	}
	
	public void sendProductDidUpdateNotification() {
		Intent intent = new Intent(MCProduct.PRODUCT_STATUS_UPDATED);
		intent.putExtra("productId", this.getProductId());
    	broadCastManager.sendBroadcast(intent);
	}
	
	public void view(Activity parentActivity) {
		SharedPreferences preferences = parentActivity.getSharedPreferences("MyPreferences", Activity.MODE_PRIVATE);
		
		int defaultDisplayType = MCProductDisplayType.TEXT;
		if (this.displayType == MCProductDisplayType.FULL) {
			defaultDisplayType = MCProductDisplayType.FULL;
		} else if (this.displayType == MCProductDisplayType.ALL) {
			defaultDisplayType = this.defaultDisplayType;
		}
		
		int versionToOpen = preferences.getInt(getDisplayTypeKey(), defaultDisplayType);
		if (versionToOpen == MCProductDisplayType.FULL) {
			viewFullVersion(parentActivity);
		} else {
			viewTextVersion(parentActivity);
		}
		
		String eventDescription = "View " + this.getName();
		MCAnalyticsHelper.logEvent(eventDescription, true, getContext());
	}
	
	public void viewFullVersion(Activity parentActivity) {
		Intent previewActivity = new Intent(parentActivity, FullVersionActivity.class);
		displayIntentWithType(previewActivity, MCProductDisplayType.FULL, parentActivity);
	}
	
	public void viewTextVersion(Activity parentActivity) {
		Intent previewActivity = new Intent(parentActivity, TextVersionActivity.class);
		displayIntentWithType(previewActivity, MCProductDisplayType.TEXT, parentActivity);
	}
	
	private void displayIntentWithType(Intent intent, int displayType, Activity parentActivity) {
		SharedPreferences preferences = parentActivity.getSharedPreferences("MyPreferences", Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(getDisplayTypeKey(), displayType);
		editor.commit();
		
		
		Bundle bundle = new Bundle();
		bundle.putString("issueNo", this.issueNo);
		intent.putExtras(bundle);
		
		parentActivity.startActivityForResult(intent, BasePreviewActivity.RESULT_CODE);
	}
	
	public String getDisplayTypeKey() {
		return "issue" + this.issueNo + "DisplayType";
	}
	
	@Override
	public String getProductType() {
		return "inapp";
	}
	
	 @Override
	public String savePlayStorePurchaseUrl() {
		return Constants.SERVER_PRODUCTS_URL + "/android_purchase";
	}
	
	@Override
	public void succesfullPurchase() {
		if (this.purchasing) {
			download();
		}
		
		this.purchased = true;
		this.purchasing = false;

		SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putBoolean(MCProduct.PRODUCT_WAS_PURCHASED, true);
		editor.apply();
		
		sendProductDidUpdateNotification();
	}
	
	@Override
	public void failedToPurchase() {
		this.purchasing = false;
		
		sendProductDidUpdateNotification();
	}
}
