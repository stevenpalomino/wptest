package com.netbloo.magcast.models;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.bebaibcebe.badabebaibcebe.R;

import com.netbloo.magcast.constants.MCSubscriptionProductStatus;
import com.netbloo.magcast.constants.Constants;
import com.netbloo.magcast.helpers.HttpRequestHelper;
import com.netbloo.magcast.helpers.MCAlertDialog;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.helpers.PlayStoreBillingHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import static android.content.ContentValues.TAG;

public class MCSubscriptionProduct extends MCBaseProduct {
	public static final String SUBSCRIPTION_STATUS_UPDATED = "subscriptionStatusDidUpdate";
	
	private boolean hasPrice;
	private int magsubNo;
	private String name;
	private boolean purchased;

	public int getMagsubNo() {
		return magsubNo;
	}

	public String getName() {
		return name;
	}

	public int getStatus() {
		int currentSubscriptionStatus = MCSubscriptionProductStatus.MCSubscriptionProductStatusNotAvailable;
	    
	    if(this.purchased) {
	        currentSubscriptionStatus = MCSubscriptionProductStatus.MCSubscriptionProductStatusPurchased;
	    } else if (this.isPurchasing()) {
	        currentSubscriptionStatus = MCSubscriptionProductStatus.MCSubscriptionProductStatusPurchasing;
	    }
	    
	    return currentSubscriptionStatus;
	}

	public MCSubscriptionProduct(JSONObject json, Context context) {
		super(json, context);
		
		try {
			this.magsubNo = json.getInt("magsubNo");
		} catch (JSONException e) {
		}
		
		try {
			this.name = json.getString("name");
			this.name = this.name.replace(".subscription", "android.subscription");
		} catch (JSONException e) {
		}
	}
	
	@Override
	public String getProductType() {
		return "subs";
	}
	
	@Override
	public void setPrice(String price) {
		super.setPrice(price);
		
		this.hasPrice = true;
	}

	@Override
	public String savePlayStorePurchaseUrl() {
		return Constants.SERVER_SUBSCRIPTION_URL + "/android_purchase";
	}
	
	public void sendProductDidUpdateNotification() {
		Intent intent = new Intent(MCSubscriptionProduct.SUBSCRIPTION_STATUS_UPDATED);
		intent.putExtra("productId", this.getProductId());
    	broadCastManager.sendBroadcast(intent);
	}

	@Override
	public void purchase(PlayStoreBillingHelper playStoreBillingHelper) {
		if (this.isFree()) {
			this.saveFreeSubscriptionOnServer(playStoreBillingHelper);
		} else {
			super.purchase(playStoreBillingHelper);
		}
	}
	
	private void saveFreeSubscriptionOnServer(PlayStoreBillingHelper playStoreBillingHelper) {
		final Activity context = (Activity)playStoreBillingHelper.getContext();
		new Thread(new Runnable() {
			@Override
			public void run() {
				purchasing = true;
				try {
		        	String url = Constants.SERVER_SUBSCRIPTION_URL + "/free_subscription";
		        	ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		        	params.add(new BasicNameValuePair("productId", getProductId()));
		        	JSONObject response = HttpRequestHelper.makePostRequest(url, params, context);
		        	
		        	String status = response.getString("status");
		        	if (status.equals("success")) {
//						if (MCPublisher.sharedPublisher(context).isHasBranchUserCode(context)){
//							Log.i(TAG, "FREE ISSUE LOG ~~~~~");
//							try {
//								HttpRequestHelper.logEventWithEventCode("android_free_issue_download", context);
//							} catch (JSONException e) {
//								e.printStackTrace();
//							}
//						}
						
		        		succesfullPurchase();
		        	} else {
		        		context.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								MCAlertDialog.showErrorWithText(context.getString(R.string.cannot_save_purchase), context);
							}
						});
		        		failedToPurchase();
		        	}
		        } catch (JSONException e) {
		        	context.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							MCAlertDialog.showErrorWithText(context.getString(R.string.cannot_save_purchase), context);
						}
					});
		        	e.printStackTrace();
		        	
		        	failedToPurchase();
				} catch (NullPointerException e) {
					context.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							MCAlertDialog.showErrorWithText(context.getString(R.string.cannot_save_purchase), context);
						}
					});
		        	e.printStackTrace();
		        	
		        	failedToPurchase();
				}
			}
		}).start();
	}

	@Override
	public void succesfullPurchase() {
		if (this.purchasing) {
			MCPublisher publisher = MCPublisher.sharedPublisher(context);
			MCProduct productToDownload = publisher.issueToDownloadAfterSubscription();
			if (productToDownload != null) {
				productToDownload.download();
			}
		}
		
		this.purchasing = false;
		this.purchased = true;
		
		sendProductDidUpdateNotification();
	}

	@Override
	public void failedToPurchase() {
		this.purchasing = false;
		
		sendProductDidUpdateNotification();
	}

	public boolean isHasPrice() {
		return hasPrice;
	}
	
	public boolean isFree() {
		return this.getProductId().endsWith("subscriptionfree");
	}
}
