package com.netbloo.magcast.models.magazineSettings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.netbloo.magcast.activities.WebViewActivity;
import com.netbloo.magcast.constants.MCMagazineStatus;
import com.netbloo.magcast.helpers.HtmlHelper;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.models.MCMagazine;
import com.netbloo.magcast.models.MCProduct;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Controls upsell page popup work
 *
 * Created by izmaylav on 5/4/16.
 */
public class MCUpsellPagePopup {

    // Properties

    private String content;
    private String displayCondition;
    private boolean enabled = false;
    private String issueNo;

    static private ArrayList<MCUpsellPagePopup> firedUpsellPages = new ArrayList<MCUpsellPagePopup>();

    public MCUpsellPagePopup(JSONObject json) {
        try {
            this.content = json.getString("content");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            this.displayCondition = json.getString("displayCondition");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            this.enabled = json.getString("status").equals("active");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            this.issueNo = json.getString("issueNo");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setContext(Context context) {
        if (displayCondition.equals("onIssuePurchase")) {
            LocalBroadcastManager.getInstance(context).registerReceiver(this.productWasPurchasedReceiver, new IntentFilter(MCProduct.PRODUCT_WAS_PURCHASED));
        } else if (displayCondition.equals("onSubscription")) {
            LocalBroadcastManager.getInstance(context).registerReceiver(this.subscriptionWasPurchasedReceiver, new IntentFilter(MCMagazine.MAGAZINE_STATUS_CHANGED));
        }
    }

    private void display(Context context) {
        if (!enabled) {
            return;
        }

        if (wasFired()) {
            return;
        }
        firedUpsellPages.add(this);

        Log.d("test", "test display");

        Intent webViewIntent = new Intent(context,
                WebViewActivity.class);
        webViewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        String preparedContent = HtmlHelper.prepareAppHtml(content);

        webViewIntent.putExtra("htmlContent", preparedContent);
        webViewIntent.putExtra("displayBottomBar", false);
        webViewIntent.putExtra("displayCloseButton", true);
        webViewIntent.putExtra("displayTopBar", false);
        webViewIntent.putExtra("scaleToDefaultPageWidth", true);
        context.startActivity(webViewIntent);
    }

    private boolean wasFired() {
        boolean wasFired = false;
        for (MCUpsellPagePopup upsellPagePopup: firedUpsellPages) {
            if (upsellPagePopup.displayCondition.equals(displayCondition)
                    && upsellPagePopup.issueNo.equals(issueNo)) {
                wasFired = true;
            }
        }

        return wasFired;
    }

    // Notifications handling

    private BroadcastReceiver productWasPurchasedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String productId = intent.getStringExtra("productId");

            MCPublisher publisher = MCPublisher.sharedPublisher(context);
            MCProduct product = publisher.issueWithProductId(productId);
            if (product == null) {
                return;
            }

            if (!product.getIssueNo().equals(issueNo)) {
                return;
            }

            display(context);
        }
    };

    private BroadcastReceiver subscriptionWasPurchasedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            MCPublisher publisher = MCPublisher.sharedPublisher(context);
            if (publisher.getMagazine().getSubscriptionStatus() != MCMagazineStatus.MCMagazineStatusPurchased) {
                return;
            }

            display(context);
        }
    };

}
