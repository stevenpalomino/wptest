package com.netbloo.magcast.models.magazineSettings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.netbloo.magcast.activities.WebViewActivity;
import com.netbloo.magcast.constants.MCMagazineStatus;
import com.netbloo.magcast.helpers.HtmlHelper;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.models.MCProduct;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Controls splash page
 *
 * Created by izmaylav on 5/2/16.
 */
public class MCSplashPage {

    // Constants

    public static final String SplashPageDisplayConditionUntilDownload = "untilDownload";
    public static final String SplashPageDisplayConditionUntilPurchase = "untilPurchase";
    public static final String SplashPageDisplayConditionUntilSubscription = "untilSubscription";
    public static final String SplashPageDisplayConditionAllTime = "allTime";

    public static final String SplashPageWasDisplayed = "SplashPageWasDisplayed";

    // Variables

    private static boolean hasFired = false;

    private String content;
    private String displayCondition;

    public MCSplashPage(JSONObject json) {
        try {
            this.content = json.getString("content");
        } catch (JSONException e) {
        }

        try {
            this.displayCondition = json.getString("displayCondition");
        } catch (JSONException e) {
        }
    }

    public void displayWithCheckingConditions(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Activity.MODE_PRIVATE);
        if (displayCondition.equals(MCSplashPage.SplashPageDisplayConditionUntilDownload)) {
            if (preferences.getBoolean(MCProduct.PRODUCT_WAS_DOWNLOADED, false)) {
                return;
            }
        } else if (displayCondition.equals(MCSplashPage.SplashPageDisplayConditionUntilPurchase)) {
            if (preferences.getBoolean(MCProduct.PRODUCT_WAS_PURCHASED, false)) {
                return;
            }
        } else if (displayCondition.equals(MCSplashPage.SplashPageDisplayConditionUntilSubscription)) {
            MCPublisher publisher = MCPublisher.sharedPublisher(context);
            if (publisher.getMagazine().getSubscriptionStatus() == MCMagazineStatus.MCMagazineStatusPurchased) {
                return;
            }
        } else if (!displayCondition.equals(MCSplashPage.SplashPageDisplayConditionAllTime)) {
            if (preferences.getBoolean(MCSplashPage.SplashPageWasDisplayed, false)) {
                return;
            }
        }

        display(context);
    }

    private void display(Context context) {
        if (MCSplashPage.hasFired) {
            return;
        }
        MCSplashPage.hasFired = true;

        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(MCSplashPage.SplashPageWasDisplayed, true);
        editor.apply();

        Intent webViewIntent = new Intent(context,
                WebViewActivity.class);

        String preparedContent = HtmlHelper.prepareAppHtml(content);

        webViewIntent.putExtra("htmlContent", preparedContent);
        webViewIntent.putExtra("displayBottomBar", false);
        webViewIntent.putExtra("displayCloseButton", true);
        webViewIntent.putExtra("displayTopBar", false);
        webViewIntent.putExtra("scaleToDefaultPageWidth", true);
        context.startActivity(webViewIntent);
    }


}
