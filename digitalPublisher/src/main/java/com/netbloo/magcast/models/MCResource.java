package com.netbloo.magcast.models;

import org.json.JSONException;
import org.json.JSONObject;

public class MCResource {
	private int index;
	private String title;
	private String description;
	private String link;
	private String imagePath;
	
	public MCResource(JSONObject json) {
		super();
		
		try {
			index = json.getInt("resourceNo");
		} catch (JSONException e) {
		}
		try {
			title = json.getString("resource_title");
		} catch (JSONException e) {
		}
		try {
			description = json.getString("resource_desc");
		} catch (JSONException e) {
		}
		try {
			link = json.getString("resource_link");
		} catch (JSONException e) {
		}
		try {
			imagePath = json.getString("resource_img");
		} catch (JSONException e) {
		}
	}

	public int getIndex() {
		return index;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getLink() {
		return link;
	}

	public String getImagePath() {
		return imagePath;
	}
}
