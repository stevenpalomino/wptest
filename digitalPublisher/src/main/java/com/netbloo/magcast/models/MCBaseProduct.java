package com.netbloo.magcast.models;

import org.json.JSONException;
import org.json.JSONObject;

import com.netbloo.magcast.helpers.PlayStoreBillingHelper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;

abstract public class MCBaseProduct {
	// Notifications
	protected LocalBroadcastManager broadCastManager;
	
	@SuppressLint("ParserError")
	protected Context context;
	
	protected String price;
	protected String rawPrice;
	protected String currencyCode;
	protected String productId;
	protected boolean purchasing;
	protected String purchaseRequestId;

	public MCBaseProduct(JSONObject json, Context context) {
		this.broadCastManager = LocalBroadcastManager.getInstance(context);
		this.context = context;
		
		try {
			price = new String("$").concat(json.getString("issuePrice"));
		} catch (JSONException e) {
			
		}
		try {
			productId = json.getString("productID");
		} catch (JSONException e) {
			
		}
	}
	
	public String getName() {
		return "";
	}
	
	public Context getContext() {
		return context;
	}
	
	public boolean isPurchasing() {
		return purchasing;
	}
	
	public String getPrice() {
		return price;
	}

	public String getRawPrice(){
		return rawPrice;
	}

	public String getCurrencyCode() {
		return  currencyCode;
	}

	
	public void setPrice(String price) {
		this.price = price;
	}

	public void setRawPrice(String rawPrice){
		this.rawPrice = rawPrice;
	}

	public void setCurrencyCode(String currencyCode){
		this.currencyCode = currencyCode;
	}

	public String getProductId() {
		return productId;
	}
	
	public String getPurchaseRequestId() {
		return purchaseRequestId;
	}
	
	public void purchase(PlayStoreBillingHelper playStoreBillingHelper) {
		this.purchasing = true;
		
		playStoreBillingHelper.purchaseProduct(this);
		
		this.sendProductDidUpdateNotification();
	}
	
	abstract public String getProductType();
	abstract public String savePlayStorePurchaseUrl();
	abstract public void succesfullPurchase();
	abstract public void failedToPurchase();
	
	abstract public void sendProductDidUpdateNotification();
}
