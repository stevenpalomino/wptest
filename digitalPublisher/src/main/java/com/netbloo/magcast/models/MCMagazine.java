package com.netbloo.magcast.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.netbloo.magcast.constants.MCMagazineStatus;
import com.netbloo.magcast.constants.MCSubscriptionProductStatus;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.helpers.UserHelper;
import com.netbloo.magcast.models.magazineSettings.MCPrivateSubscription;
import com.netbloo.magcast.models.magazineSettings.MCSplashPage;
import com.netbloo.magcast.models.magazineSettings.MCUpsellPagePopup;

import java.util.ArrayList;

public class MCMagazine {

	// Notifications

	protected LocalBroadcastManager broadCastManager;
	public static final String MAGAZINE_STATUS_CHANGED = "magazineStatusChanged";
	
	// Data

	private String affiliateId;
	private boolean bonus;
	private Context context;
	private boolean doExternalIntegration;
	private boolean doOptinPage;
	private boolean doPrivateSubscription;
	private boolean doSplashPage;
	private String name;
	private String optinPage;
	private MCPrivateSubscription privateSubscription;
	private MCSplashPage splashPage;
	private int subscriptionStatus;
	private String supportEmail;
	private String teaser;
	private ArrayList<MCUpsellPagePopup> upsellPages = new ArrayList<MCUpsellPagePopup>();

	public MCMagazine(JSONObject json, Context context) {
		try {
			this.affiliateId = json.getString("affilateId");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		this.context = context;

		try {
			this.name = json.getString("name");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		try {
			if (json.getInt("status") == 1) {
				subscriptionStatus = MCMagazineStatus.MCMagazineStatusPurchased;
			} else {
				subscriptionStatus = MCMagazineStatus.MCMagazineStatusNotAvailable;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			this.teaser = json.getString("teaser");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			this.doPrivateSubscription = (json.getInt("doPrivateSubscription") == 1);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			this.doSplashPage = (json.getInt("doSplashContentPage") == 1);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			this.bonus = (json.getInt("bonus") == 1);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		try {
			this.doExternalIntegration = (json.getInt("doExternalIntegration") == 1);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			this.supportEmail = json.getString("supportEmail");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		try {
			this.doOptinPage = (json.getInt("doOptinPage") == 1);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		try {
			this.optinPage = json.getJSONObject("optinPage").getString("content");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			JSONObject privateSubscription = json.getJSONObject("privateSubscription");
			this.privateSubscription = new MCPrivateSubscription(privateSubscription);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			JSONObject splashPage = json.getJSONObject("splashPage");
			this.splashPage = new MCSplashPage(splashPage);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		upsellPages.clear();
		try {
			JSONArray upsellPagePopupJsons = json.getJSONArray("upsellPagePopups");
			for (int upsellPageIndex = 0; upsellPageIndex < upsellPagePopupJsons.length(); ++upsellPageIndex) {
				JSONObject upsellPagePopupJson = upsellPagePopupJsons.getJSONObject(upsellPageIndex);
				MCUpsellPagePopup upsellPagePopup = new MCUpsellPagePopup(upsellPagePopupJson);
				upsellPages.add(upsellPagePopup);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		this.broadCastManager = LocalBroadcastManager.getInstance(context);
		BroadcastReceiver subscriptionProductStatusChangedReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				magazineStatusChanged();
			}
		};
		LocalBroadcastManager.getInstance(context).registerReceiver(
				subscriptionProductStatusChangedReceiver,
				new IntentFilter(MCSubscriptionProduct.SUBSCRIPTION_STATUS_UPDATED));
	}

	public String getAffiliateId() {
		return affiliateId;
	}

	public boolean isBonus() {
		return bonus;
	}
	
	public boolean isDoExternalIntegration() {
		return doExternalIntegration;
	}

	public boolean isDoOptinPage() {
		return doOptinPage;
	}

	public boolean isDoPrivateSubscription() {
		return doPrivateSubscription;
	}

	public boolean isDoSplashPage() {
		return doSplashPage;
	}

	public String getName() {
		return name;
	}

	public String getOptinPage() {
		return optinPage;
	}

	public MCPrivateSubscription getPrivateSubscription() {
		return privateSubscription;
	}

	public MCSplashPage getSplashPage() { return splashPage; }

	public ArrayList<MCUpsellPagePopup> getUpsellPages() { return upsellPages; }

	public int getSubscriptionStatus() {
		MCPublisher publisher = MCPublisher.sharedPublisher(null);
		int subscriptionProductsCount = publisher
				.numberOfSubscriptionProducts();
		MCSubscriptionProduct subscriptionProduct;
		for (int subscriptionProductIndex = 0; subscriptionProductIndex < subscriptionProductsCount; ++subscriptionProductIndex) {
			subscriptionProduct = publisher
					.subscriptionProductAtIndex(subscriptionProductIndex);
			if (subscriptionProduct != null && subscriptionProduct.getStatus() == MCSubscriptionProductStatus.MCSubscriptionProductStatusPurchased) {
				subscriptionStatus = MCMagazineStatus.MCMagazineStatusPurchased;
			}
		}

		UserHelper userHelper = new UserHelper(context);
		if (userHelper.isAuthenticated(context)) {
			subscriptionStatus = MCMagazineStatus.MCMagazineStatusPurchased;
		}

		return subscriptionStatus;
	}
	
	public void setSubscriptionStatus(int subscriptionStatus) {
		this.subscriptionStatus = subscriptionStatus;
		
		magazineStatusChanged();
	}

	public String getSupportEmail() {
		return supportEmail;
	}

	public String getTeaser() {
		return teaser;
	}
	
	private void magazineStatusChanged() {
		Intent intent = new Intent(MCMagazine.MAGAZINE_STATUS_CHANGED);
    	broadCastManager.sendBroadcast(intent);
	}
}
