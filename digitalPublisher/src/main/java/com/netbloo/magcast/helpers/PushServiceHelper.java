package com.netbloo.magcast.helpers;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.netbloo.magcast.constants.Constants;

import android.content.Context;

public class PushServiceHelper {
	public static void updateDeviceStatus(Context context) {
		MCPublisher publisher = MCPublisher.sharedPublisher(context);
		String deviceToken = publisher.getDeviceToken();
		
		if (deviceToken != null) {
			final Context currentContext = context;
			new Thread(new Runnable() {
				@Override
				public void run() {
					ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair("pushNotificationsEnabled", "1"));
					params.add(new BasicNameValuePair("magazineId", Constants.MAGAZINE_ID + ""));
					params.add(new BasicNameValuePair("development", Constants.DEVELOPMENT + ""));
					HttpRequestHelper.makePostRequest(Constants.PUSH_NOTIFICATION_SERVER_URL, params, currentContext);
				}
			}).start();
		}
	}
}
