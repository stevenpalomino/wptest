package com.netbloo.magcast.helpers;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.netbloo.magcast.models.magazineSettings.MCUpsellPagePopup;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

public class MCServicesHelper {
	private static MCServicesHelper servicesHelper = null;

	public static MCServicesHelper getInstance() {
		if (MCServicesHelper.servicesHelper == null) {
			MCServicesHelper.servicesHelper = new MCServicesHelper();
		}

		return MCServicesHelper.servicesHelper;
	}

	public void startServicesAtApplicationCreated(Application application) {
		FacebookSdk.sdkInitialize(application.getApplicationContext());
		AppEventsLogger.activateApp(application);
	}
	
	public void startServicesAtMainActivityOnReady(Context context) {
		MCPublisher publisher = MCPublisher.sharedPublisher(context);
		if (publisher.getMagazine().isDoSplashPage()) {
			//publisher.getMagazine().getSplashPage().displayWithCheckingConditions(context);
		}

		MCServicesHelper servicesHelper = MCServicesHelper.getInstance();
		servicesHelper.updateContextForServices(context);
	}

	public void updateContextForServices(Context context) {
		MCPublisher publisher = MCPublisher.sharedPublisher(context);
		if (publisher.getMagazine() == null) {
			return;
		}

		ArrayList<MCUpsellPagePopup> upsellPages = publisher.getMagazine().getUpsellPages();
		for (MCUpsellPagePopup upsellPage : upsellPages) {
			upsellPage.setContext(context);
		}
	}
}
