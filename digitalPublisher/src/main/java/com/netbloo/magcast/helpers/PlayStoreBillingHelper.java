package com.netbloo.magcast.helpers;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;
import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.constants.MCMagazineStatus;
import com.netbloo.magcast.constants.MCProductStatus;
import com.netbloo.magcast.models.MCBaseProduct;
import com.netbloo.magcast.models.MCProduct;
import com.netbloo.magcast.models.MCSubscriptionProduct;

import static com.netbloo.magcast.helpers.GCMHelper.TAG;

public class PlayStoreBillingHelper {
	// Billing response codes
    public static final int BILLING_RESPONSE_RESULT_OK = 0;
    public static final int BILLING_RESPONSE_RESULT_USER_CANCELED = 1;
    public static final int BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE = 3;
    public static final int BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE = 4;
    public static final int BILLING_RESPONSE_RESULT_DEVELOPER_ERROR = 5;
    public static final int BILLING_RESPONSE_RESULT_ERROR = 6;
    public static final int BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED = 7;
    public static final int BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED = 8;
    
    // Keys for the responses from InAppBillingService
    public static final String RESPONSE_CODE = "RESPONSE_CODE";
    public static final String RESPONSE_GET_SKU_DETAILS_LIST = "DETAILS_LIST";
    public static final String RESPONSE_BUY_INTENT = "BUY_INTENT";
    public static final String RESPONSE_INAPP_PURCHASE_DATA = "INAPP_PURCHASE_DATA";
    public static final String RESPONSE_INAPP_SIGNATURE = "INAPP_DATA_SIGNATURE";
    public static final String RESPONSE_INAPP_ITEM_LIST = "INAPP_PURCHASE_ITEM_LIST";
    public static final String RESPONSE_INAPP_PURCHASE_DATA_LIST = "INAPP_PURCHASE_DATA_LIST";
    public static final String RESPONSE_INAPP_SIGNATURE_LIST = "INAPP_DATA_SIGNATURE_LIST";
    public static final String INAPP_CONTINUATION_TOKEN = "INAPP_CONTINUATION_TOKEN";
	
	Context context;
	
	boolean delayedLoadFromPlayStore;

	public PlayStoreBillingHelper() {
		super();
	}
	
	public Context getContext() {
		return this.context;
	}
	
	public void setContext(Context context) {
		if (this.context != context) {
			Log.d("play store", "Start service with activity");
			stopService();		
			this.context = context;
			startService();
		}
	}

	public void startService() {
		this.stopService();
	
		this.serviceConn = new ServiceConnection() {
			@Override
			public void onServiceDisconnected(ComponentName name) {
				service = null;
			}

			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				PlayStoreBillingHelper.this.service = IInAppBillingService.Stub.asInterface(service);
				
				if (delayedLoadFromPlayStore) {
					MCPublisher publisher = MCPublisher.sharedPublisher(context);
					retrieveProductsFromStore(publisher.getProducts(), publisher.getSubscriptionProducts());
				}
			}
		};
		
        Intent bindIntent = new Intent(
                                       "com.android.vending.billing.InAppBillingService.BIND");
        bindIntent.setPackage("com.android.vending");
        this.context.bindService(bindIntent,
                                 serviceConn, Context.BIND_AUTO_CREATE);
	}

	public void stopService() {
		if ((this.serviceConn != null)
				&& (this.context != null)) {
			this.context.unbindService(serviceConn);
			
			this.serviceConn = null;
			this.context = null;
		}
	}

	public void retrieveProductsFromStore(ArrayList<MCProduct> products, ArrayList<MCSubscriptionProduct> subscriptionProducts) {
		// Request products from store
		ArrayList<String> skuList = prepareSkuList(products,
				subscriptionProducts);

		try {
			if ((this.service != null)
					&& (this.context != null)) {
				this.delayedLoadFromPlayStore = false;
				
				// Due to bug in play store there is need to request max 20 products from play store
				int numberOfPlayStoreRequests = (int)Math.ceil((double)skuList.size() / 20);
				ArrayList<String> currentSkuList = null;
				int originalSkuIndex = 0;
				Bundle querySkus = null;
				for (int playStoreRequestIndex = 0; playStoreRequestIndex < numberOfPlayStoreRequests; ++playStoreRequestIndex) {
					// Prepeare skus list for current request(max 20 items)
					currentSkuList = new ArrayList<String>();
					for (int skuIndex = 0; skuIndex < 20; ++skuIndex) {
						originalSkuIndex = skuIndex + 20 * playStoreRequestIndex;
						if (originalSkuIndex >= skuList.size()) {
							break;
						}
						
						currentSkuList.add(skuList.get(originalSkuIndex));
					}
					
					// Prepare request bundle
					querySkus = new Bundle();
					querySkus.putStringArrayList("ITEM_ID_LIST", currentSkuList);
					
					// Parse response
					try {
						Bundle productsSkuDetails = service.getSkuDetails(3,
								this.context.getPackageName(), "inapp", querySkus);
						parsePlayStoreResponse(products, subscriptionProducts,
								productsSkuDetails);
					} catch (NullPointerException e) {
						e.printStackTrace();  
					}
					
					// Parse response
					try {
						Bundle subscriptionsSkuDetails = service.getSkuDetails(3,
								this.context.getPackageName(), "subs", querySkus);
						parsePlayStoreResponse(products, subscriptionProducts,
								subscriptionsSkuDetails);
					} catch (NullPointerException e) {
						e.printStackTrace();  
					}
				}
				
			} else {
				this.delayedLoadFromPlayStore = true;
			}
		} catch (RemoteException e) {
			e.printStackTrace();  
		}
	}
	
	public void restorePurchases() {
		MCPublisher publisher = MCPublisher.sharedPublisher(context);
		if (publisher.getMagazine().getSubscriptionStatus() == MCMagazineStatus.MCMagazineStatusNotAvailable) {
			restoreSubscriptions();
		}
		
		restoreIssues();
	}
	
	private void restoreSubscriptions() {
		restoreProducts("subs", null);
	}
	
	private void restoreIssues() {
		restoreProducts("inapp", null);
	}
	
	private void restoreProducts(String productType, String continuationToken) {
		try {
			if ((this.context != null) && (this.service != null)) {
				Bundle ownedItems = service.getPurchases(3, this.context.getPackageName(), productType, continuationToken);
				
				int response = ownedItems.getInt("RESPONSE_CODE");
				if (response == 0) {
				   ArrayList<String> ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
				   ArrayList<String> purchaseDataList = ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
				   ArrayList<String> signatureList = ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
				   String newContinuationToken = ownedItems.getString("INAPP_CONTINUATION_TOKEN");
				   MCPublisher publisher = MCPublisher.sharedPublisher(context);
				   MCBaseProduct product = null;
				   Log.d("restore", "Restore products with type " + productType + " response code " + response + " purchase data list size " + purchaseDataList.size());
				   for (int i = 0; i < purchaseDataList.size(); ++i) {
				      String purchaseData = purchaseDataList.get(i);
				      String signature = signatureList.get(i);
				      String sku = ownedSkus.get(i);
				      
				      Log.d("restore", "Subscription restore " + purchaseData + " sku " + sku + " signature " + signature);
				  
				      if (productType.equals("subs")) {
				    	  product = publisher.subscriptionProductWithProductId(sku);
					      if (product != null) {
					    	  savePurchaseOnServer(purchaseData, signature, product);
					      }
				      } else {
				    	  product = publisher.issueWithProductId(sku);
				    	  MCProduct issue = (MCProduct)product;
				    	  if ((product != null) 
				    			  && (issue.getStatus() == MCProductStatus.MCProductStatusNotAvailable)) {
				    		  savePurchaseOnServer(purchaseData, signature, product);
				    	  }
				      }
				   } 

				   if (newContinuationToken != null) {
					   restoreProducts(productType, newContinuationToken);
				   }
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	private void parsePlayStoreResponse(ArrayList<MCProduct> products,
			ArrayList<MCSubscriptionProduct> subscriptionProducts,
			Bundle skuDetails) {
		int response = skuDetails.getInt("RESPONSE_CODE");
		if (response == 0) {
			ArrayList<String> responseList = skuDetails
					.getStringArrayList("DETAILS_LIST");
			Log.d("product request", "Product request response " + responseList);

			// Retrieve prices for products
			if (responseList != null) {
				for (String thisResponse : responseList) {
					try {
						JSONObject object = new JSONObject(thisResponse);
						String price = object.getString("price");
						String currency = object.getString("price_currency_code");
						Log.i(TAG, "~~~~~ parsePlayStoreResponse: "+price);

						setPriceForProduct(products, subscriptionProducts,
								thisResponse);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void setPriceForProduct(ArrayList<MCProduct> products,
			ArrayList<MCSubscriptionProduct> subscriptionProducts,
			String thisResponse) throws JSONException {
		JSONObject object = new JSONObject(thisResponse);
		String sku = object.getString("productId");
		
		float priceAmount = (float)object.getInt("price_amount_micros") / 1000000;
		String priceCurrency = object.getString("price_currency_code");
		String price = String.format("%10.2f", priceAmount) + " " + priceCurrency;
		
		for (MCProduct product : products) {
			if (sku.equals(product.getProductId())) {
				product.setPrice(price);
				product.setRawPrice(String.format("%.2f",priceAmount));
				product.setCurrencyCode(priceCurrency);
				break;
			}
		}
		
		for (MCSubscriptionProduct subscriptionProduct : subscriptionProducts) {
			if (sku.equals(subscriptionProduct.getProductId())) {
				subscriptionProduct.setPrice(price);
				subscriptionProduct.setRawPrice(String.format("%.2f",priceAmount));
				subscriptionProduct.setCurrencyCode(priceCurrency);
				break;
			}
		}
	}

	private ArrayList<String> prepareSkuList(ArrayList<MCProduct> products,
			ArrayList<MCSubscriptionProduct> subscriptionProducts) {
		ArrayList<String> skuList = new ArrayList<String>();
		
		for (MCProduct product : products) {
				skuList.add(product.getProductId());
		}
		
		for (MCSubscriptionProduct subscriptionProduct : subscriptionProducts) {
			skuList.add(subscriptionProduct.getProductId());
		}
		
		return skuList;
	}
	
	public void purchaseProduct(MCBaseProduct product) {
		final MCPublisher publisher = MCPublisher.sharedPublisher(this.context);
		if (!publisher.isOnline()) {
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
			alertDialogBuilder.setTitle(this.context.getString(R.string.error));
			alertDialogBuilder.setMessage(this.context.getString(R.string.cant_get_issue_from_server));
			alertDialogBuilder.setPositiveButton(context.getString(R.string.reload), new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            publisher.requestProductsFromServer(PlayStoreBillingHelper.this);
		        }
		     });
			
			AlertDialog alertDialog = alertDialogBuilder.create();
			alertDialog.show();
			
			failedToPurchaseWithoutKnowingTheProduct();
			
			return;
		}
		
		if (this.service == null) {
			MCAlertDialog.showErrorWithText(this.context.getString(R.string.check_play_store_setup), this.context);
			
			failedToPurchaseWithoutKnowingTheProduct();
			
			return;
		}
		
		try {
			Bundle buyIntentBundle = service.getBuyIntent(3, context.getPackageName(),
					   product.getProductId(), product.getProductType(), "");
			int responseCode = getResponseCodeFromBundle(buyIntentBundle);
			if (responseCode != BILLING_RESPONSE_RESULT_OK) {
                product.failedToPurchase();
                
                parseResponseCode(responseCode);
                return;
            }
			
			PendingIntent pendingIntent = buyIntentBundle.getParcelable(RESPONSE_BUY_INTENT);
			if (pendingIntent != null) {
				((Activity)context).startIntentSenderForResult(pendingIntent.getIntentSender(),
						   1001, new Intent(), Integer.valueOf(0), Integer.valueOf(0),
						   Integer.valueOf(0));
			} else {
				MCAlertDialog.showErrorWithText(this.context.getString(R.string.cant_start_purchase_process),
						this.context);
				product.failedToPurchase();
			}
		} catch (RemoteException e) {
			MCAlertDialog.showErrorWithText(e.getLocalizedMessage(),
					this.context);
			e.printStackTrace();
		} catch (SendIntentException e) {
			MCAlertDialog.showErrorWithText(e.getLocalizedMessage(),
					this.context);
			e.printStackTrace();
		}
	}
	
	private void parseResponseCode(int responseCode) {
		switch (responseCode) {
		case BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE:
			MCAlertDialog.showErrorWithText(context.getString(R.string.billing_unavailable), context);
			break;
		case BILLING_RESPONSE_RESULT_DEVELOPER_ERROR:
		case BILLING_RESPONSE_RESULT_ERROR:
			MCAlertDialog.showErrorWithText("Fatal error during the API action", context);
			break;
		case BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED:
			MCAlertDialog.showErrorWithText(context.getString(R.string.item_already_owned), context);
			break;
		case BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE:
			MCAlertDialog.showErrorWithText(context.getString(R.string.item_unavailable), context);
			break;
		}
	}
	
	// Workaround to bug where sometimes response codes come as Long instead of Integer
    int getResponseCodeFromBundle(Bundle b) {
        Object o = b.get(RESPONSE_CODE);
        if (o == null) {
            return BILLING_RESPONSE_RESULT_OK;
        }
        else if (o instanceof Integer) return ((Integer)o).intValue();
        else if (o instanceof Long) return (int)((Long)o).longValue();
        else {
            throw new RuntimeException("Unexpected type for bundle response code: " + o.getClass().getName());
        }
    }
	
	public void parseResponseFromPlayStore(int resultCode, Intent data) {
		final String purchaseData = data.getStringExtra(RESPONSE_INAPP_PURCHASE_DATA);
		final String dataSignature = data.getStringExtra(RESPONSE_INAPP_SIGNATURE);
		
		if (purchaseData == null) {
			failedToPurchaseWithoutKnowingTheProduct();
			Log.d("purchase", "Purchase extend data" + data.getExtras().toString());
			return;
		}

		try {
			JSONObject jo = new JSONObject(purchaseData);
			String productId = jo.getString("productId");
			String purchaseToken = jo.getString("purchaseToken");
			Log.d("purchase token", "purchase token " + purchaseToken + " result code " + resultCode);
			
			MCPublisher publisher = MCPublisher.sharedPublisher(this.context);
			MCBaseProduct product = publisher.issueWithProductId(productId);
			if(product != null) {
				String eventDescription = "Buy " + product.getName();
				MCAnalyticsHelper.logEvent(eventDescription, true, this.context);
			}
			
			if (product == null) {
				product = publisher.subscriptionProductWithProductId(productId);
				if (product != null) {
					String eventDescription = "Subscribe " + product.getName();
					MCAnalyticsHelper.logEvent(eventDescription, true, this.context);
				}
			}
			
			if (product == null) {
				Log.d("purchase", "product wasn't found");
				return;
			}

			final MCBaseProduct currentProduct = product;
			if (resultCode == Activity.RESULT_OK) {
				// TODO: Product consuming for testing
				/*try {
					service.consumePurchase(3, context.getPackageName(), purchaseToken);
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}*/
				
				// Store purchase on server
				new Thread(new Runnable() {
			        public void run() {
			            savePurchaseOnServer(purchaseData, dataSignature,
								currentProduct);
			        }					
				}).start();
			} else if (resultCode != Activity.RESULT_CANCELED) {
				currentProduct.failedToPurchase();
			}
		} catch (JSONException e) {
			MCAlertDialog.showErrorWithText("Failed to parse purchase data.",
					this.context);
			e.printStackTrace();
			
			failedToPurchaseWithoutKnowingTheProduct();
		}
	}
	
	private void savePurchaseOnServer(
			final String purchaseData,
			final String dataSignature,
			final MCBaseProduct currentProduct) {
		try {
			Map<String,String> map =  new HashMap<String,String>();
			map.put("product cost", currentProduct.getRawPrice());
			map.put("currency code", currentProduct.getCurrencyCode());


			try{
				if (MCPublisher.sharedPublisher(context).isHasBranchUserCode(context)) {
					if (currentProduct.getProductType().equals("subs")) {
						HttpRequestHelper.logEventWithEventCodeAndCustomVars("android_purchase_subscription", map, context);
						Log.i(TAG, "savePurchaseOnServer: ANDROID_PURCHASE_SUBSCRIPTION");
					} else if (currentProduct.getProductType().equals("inapp")) {
						HttpRequestHelper.logEventWithEventCodeAndCustomVars("android_purchase_issue", map, context);
						Log.i(TAG, "savePurchaseOnServer: ANDROID_PURCHASE_ISSUE");
					}
				}
			}catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}


        	String url = currentProduct.savePlayStorePurchaseUrl();
        	ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        	params.add(new BasicNameValuePair("signature", dataSignature));
        	params.add(new BasicNameValuePair("responseData", purchaseData));
        	JSONObject response = HttpRequestHelper.makePostRequest(url, params, context);
        	
        	String status = response.getString("status");
        	Log.d("Server response", "Server response " + response);
        	if (status.equals("success")) {
        		currentProduct.succesfullPurchase();
        	} else {
        		currentProduct.failedToPurchase();
        	}
        } catch (JSONException e) {
        	e.printStackTrace();
        	
        	currentProduct.failedToPurchase();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}
	
	private void failedToPurchaseWithoutKnowingTheProduct() {
		Log.d("purchase", "purchase failed without knowing the product");
		
		MCPublisher publisher = MCPublisher.sharedPublisher(context);
		int productsCount = publisher.numberOfIssues();
		MCProduct product = null;
		for (int productIndex = 0; productIndex < productsCount; ++productIndex) {
			product = publisher.issueAtIndex(productIndex);
			product.failedToPurchase();
		}
		
		int subscriptionProductsCount = publisher.numberOfSubscriptionProducts();
		MCSubscriptionProduct subscriptionProduct = null;
		for (int subscriptionProductIndex = 0; subscriptionProductIndex < subscriptionProductsCount; ++subscriptionProductIndex) {
			subscriptionProduct = publisher.subscriptionProductAtIndex(subscriptionProductIndex);
			subscriptionProduct.failedToPurchase();
		}
	}

	IInAppBillingService service;
	ServiceConnection serviceConn = null;
}
