package com.netbloo.magcast.helpers;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.PopupWindow;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.activities.BasePreviewActivity;
import com.netbloo.magcast.activities.SubscriptionActivity;
import com.netbloo.magcast.activities.WebViewActivity;
import com.netbloo.magcast.constants.MCMagazineStatus;
import com.netbloo.magcast.constants.MCProductStatus;
import com.netbloo.magcast.customInterfaces.BillingActivity;
import com.netbloo.magcast.models.MCMagazine;
import com.netbloo.magcast.models.MCProduct;
import com.netbloo.magcast.models.MCSubscriptionProduct;

import java.net.URLDecoder;

/**
 * Class that performs required actions based on input url
 *
 * Created by izmaylav on 4/20/16.
 */
public class UrlActionHelper {
    public boolean performAction(String url, Activity activity, ViewGroup rootView) {
        boolean performedAction = true;

        boolean localContent = url.startsWith("file:///") || url.startsWith("?mca_");
        boolean mailtoLink = url.contains("mailto:");
        boolean marketLink = url.startsWith("market://");
        boolean pdfLink = url.endsWith(".pdf");
        boolean iframeLoad = url.startsWith("data:text/html;");
        if (localContent) {
            String lastPathComponent = url.substring(url
                    .lastIndexOf('/') + 1);
            boolean issuePurchase = lastPathComponent
                    .contains("mca_issue_purchase=");
            boolean issueDownload = lastPathComponent
                    .contains("mca_issue_download=");
            boolean subscriptionPurchase = lastPathComponent
                    .contains("mca_subscription_purchase=");
            boolean showAllSubscriptions = lastPathComponent
                    .contains("mca_all_subscriptions");
            boolean goToHomeScreen = lastPathComponent
                    .contains("mca_go_to_home_screen");
            boolean displayPopover = lastPathComponent
                    .contains("mca_display_toolbar=");
            boolean noOptions = !lastPathComponent.contains("?");
            boolean htmlPage = lastPathComponent.endsWith(".html");

            if (issuePurchase) {
                startIssuePurchaseWithUrl(url, activity);
            } else if (issueDownload) {
                startIssueDownloadWithUrl(url, activity);
            } else if (subscriptionPurchase) {
                startSubscriptionPurchaseWithUrl(url, activity);
            } else if (showAllSubscriptions) {
                showAllSubscriptions(activity);
            } else if (goToHomeScreen) {
                activity.finish();
            } else if (displayPopover) {
                displayPopoverBox(url, activity, rootView);
            } else if (noOptions && htmlPage) {
                if (activity instanceof BasePreviewActivity) {
                    ((BasePreviewActivity)activity).goToPageWithFilename(lastPathComponent);
                }
            } else {
                performedAction = false;
            }
        } else if (mailtoLink) {
            mailToWithUrl(url, activity);
        } else if (marketLink) {
            openLinkInExternalBrowser(url, activity);
        } else if (pdfLink) {
            openPdf(url, activity);
        } else if (!iframeLoad) {
            Intent webViewActivity = new Intent(activity,
                    WebViewActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("url", url);
            bundle.putBoolean("displayTopBar", true);
            bundle.putBoolean("displayBottomBar", false);
            webViewActivity.putExtras(bundle);

            activity.startActivity(webViewActivity);
        } else {
            performedAction = false;
        }

        return performedAction;
    }

    private void startIssuePurchaseWithUrl(String url, Activity activity) {
        String lastPathComponent = url.substring(url.lastIndexOf('?') + 1);
        String issueNo = lastPathComponent.replace("mca_issue_purchase=", "");

        MCPublisher publisher = MCPublisher
                .sharedPublisher(activity);
        MCProduct product = publisher.issueWithIssueNo(issueNo);
        if ((product != null)
                && (product.getStatus() == MCProductStatus.MCProductStatusNotAvailable)) {
            BillingActivity billingActivity = (BillingActivity)activity;
            product.purchase(billingActivity.getPlayStoreBillingHelper());
        } else if ((product == null)
                || (product.getStatus() != MCProductStatus.MCProductStatusPurchasing)) {
            if (product != null) {
                MCAlertDialog.showAlert(activity
                                .getString(R.string.thank_you), activity
                                .getString(R.string.issue_is_already_purchased),
                        activity);
            } else {
                MCAlertDialog.showErrorWithText(activity
                                .getString(R.string.cant_find_product),
                        activity);
            }
        }
    }

    private void startIssueDownloadWithUrl(String url, Activity activity) {
        String lastPathComponent = url.substring(url.lastIndexOf('?') + 1);
        String issueNo = lastPathComponent.replace("mca_issue_download=", "");

        MCPublisher publisher = MCPublisher
                .sharedPublisher(activity);
        MCProduct product = publisher.issueWithIssueNo(issueNo);
        if ((product != null)
                && (product.getStatus() == MCProductStatus.MCProductStatusNotAvailable)) {
            product.downloadForFree();

            MCAlertDialog.showAlert(activity
                            .getString(R.string.thank_you), activity
                            .getString(R.string.issue_is_downloading),
                    activity);
        } else if (product != null) {
            MCAlertDialog.showAlert(activity
                            .getString(R.string.thank_you), activity
                            .getString(R.string.issue_is_already_downloaded),
                    activity);
        } else {
            MCAlertDialog.showErrorWithText(
                    activity.getString(R.string.cant_find_product),
                    activity);
        }
    }

    private void startSubscriptionPurchaseWithUrl(String url, Activity activity) {
        String lastPathComponent = url.substring(url.lastIndexOf('?') + 1);
        String magsubNo = lastPathComponent.replace(
                "mca_subscription_purchase=", "");

        MCPublisher publisher = MCPublisher
                .sharedPublisher(activity);
        MCMagazine magazine = publisher.getMagazine();
        MCSubscriptionProduct subscriptionProduct = publisher
                .subscriptionProductWithMagsubNo(Integer.parseInt(magsubNo));
        if ((subscriptionProduct != null)
                && (magazine.getSubscriptionStatus() == MCMagazineStatus.MCMagazineStatusNotAvailable)) {
            BillingActivity billingActivity = (BillingActivity)activity;
            subscriptionProduct.purchase(billingActivity.getPlayStoreBillingHelper());
        } else if (magazine.getSubscriptionStatus() == MCMagazineStatus.MCMagazineStatusPurchased) {
            MCAlertDialog.showAlert(
                    activity.getString(R.string.thank_you),
                    activity.getString(R.string.you_are_subscribed),
                    activity);
        } else {
            MCAlertDialog.showErrorWithText(
                    activity.getString(R.string.cant_find_product),
                    activity);
        }
    }

    private void showAllSubscriptions(Activity activity) {
        MCPublisher publisher = MCPublisher.sharedPublisher(activity);
        MCMagazine magazine = publisher.getMagazine();
        if (magazine.getSubscriptionStatus() == MCMagazineStatus.MCMagazineStatusPurchased) {
            MCAlertDialog.showErrorWithText(
                    activity.getString(R.string.you_are_subscribed),
                    activity);
        } else {
            Intent webViewIntent = new Intent(activity,
                    SubscriptionActivity.class);
            activity.startActivity(webViewIntent);
        }
    }

    private void mailToWithUrl(String url, Activity activity) {
        MailTo parsedUrl = MailTo.parse(url);
        Intent i = newEmailIntent(parsedUrl.getTo(),
                parsedUrl.getSubject(), parsedUrl.getBody(), parsedUrl.getCc());
        activity.startActivity(i);
    }

    private void openLinkInExternalBrowser(String urlString, Activity activity) {
        Uri uri = Uri.parse(urlString);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            activity.startActivity(goToMarket);

            if (activity instanceof BasePreviewActivity) {
                BasePreviewActivity previewActivity = (BasePreviewActivity)activity;
                String eventDescription = "Issue " + previewActivity.getProduct() + " - Page " + previewActivity.getCurrentPageIndex() + " - Link: " + urlString;
                MCAnalyticsHelper.logEvent(eventDescription, false, activity);
            }

        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void openPdf(String urlString, Activity activity) {
        try {
            Uri uri = Uri.parse(urlString);
            Intent intentUrl = new Intent(Intent.ACTION_VIEW);
            intentUrl.setDataAndType(uri, "application/pdf");
            intentUrl.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(intentUrl);

            if (activity instanceof BasePreviewActivity) {
                BasePreviewActivity previewActivity = (BasePreviewActivity)activity;
                String eventDescription = "Issue " + previewActivity.getProduct() + " - Page " + previewActivity.getCurrentPageIndex() + " - Link: " + urlString;
                MCAnalyticsHelper.logEvent(eventDescription, false, activity);
            }
        } catch (ActivityNotFoundException e) {
            openLinkInExternalBrowser(urlString, activity);
        }
    }

    private Intent newEmailIntent(String address,
                                  String subject, String body, String cc) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_CC, cc);
        intent.setType("message/rfc822");
        return intent;
    }

    private void displayPopoverBox(String urlString, final Activity context, ViewGroup rootView) {
        String lastPathComponent = urlString.substring(urlString.lastIndexOf('?') + 1);
        String popoverParametersString = lastPathComponent.replace(
                "mca_display_toolbar=&", "");

        String content = "";
        String[] popoverParameters = popoverParametersString.split("&");
        for (String paramString: popoverParameters) {
            String[] paramParts = paramString.split("=", 2);
            if (paramParts.length != 2) {
                continue;
            }

            if (paramParts[0].equals("content")) {
                content = paramParts[1];
            }
        }

        try {
            content = URLDecoder.decode(content, "UTF-8");
            content = content.replaceAll("<span class=\"mca_tooltiplink\"", "<a");
            content = content.replaceAll("</span>", "</a>");
            content = content.replaceAll("data-href", "href");
            content = "<style>div { color: white; } a { color: #2479bb; }</style><div>" + content + "</div>";
        } catch (Exception e) {
            e.printStackTrace();
        }

        LayoutInflater layoutInflater
                = (LayoutInflater)context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup nullView = null;
        View popupView = layoutInflater.inflate(R.layout.info_box_popup, nullView);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        Button btnDismiss = (Button)popupView.findViewById(R.id.dismiss);
        btnDismiss.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupWindow.dismiss();
            }
        });

        WebView webView = (WebView)popupView.findViewById(R.id.webView);

        final Activity finalActivity = context;
        final ViewGroup finalRootView = rootView;
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                UrlActionHelper actionHelper = new UrlActionHelper();
                actionHelper.performAction(url, finalActivity, finalRootView);

                return true;
            }
        });

        webView.setBackgroundColor(Color.argb(1, 0, 0, 0));
        webView.loadDataWithBaseURL(urlString, content, "text/html", "utf-8", null);

        popupWindow.showAtLocation(rootView, Gravity.BOTTOM, 0, 0);
    }
}
