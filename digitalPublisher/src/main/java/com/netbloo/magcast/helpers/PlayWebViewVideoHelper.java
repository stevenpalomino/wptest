package com.netbloo.magcast.helpers;

import com.bebaibcebe.badabebaibcebe.R;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

public class PlayWebViewVideoHelper extends Object {
	public static final String ON_HIDE_CUSTOM_VIEW = "PlayWebViewVideoHelper_OnHideCustomView";
	public static final String ON_GO_BACK = "PlayWebViewVideoHelper_OnGoBack";
	public static final String ON_PAUSE = "PlayWebViewVideoHelper_OnPause";
	public static final String ON_RESUME = "PlayWebViewVideoHelper_OnResume";
	
	private Activity activity;
    private FrameLayout videoPresentationContainer;
    private boolean videoViewDisplayed;
    private WebChromeClient.CustomViewCallback videoViewCallback;
    
    public void setWebViewForVideoPlay(Activity activity, WebView webView, View viewToHide) {
    	this.activity = activity;
    	
    	if (activity != null) {        	
        	VideoPlayWebChromeClient videoPlayWebChromeClient = new VideoPlayWebChromeClient();
        	videoPlayWebChromeClient.setViewToHide(viewToHide);
        	videoPlayWebChromeClient.setWebView(webView);
        	videoPlayWebChromeClient.startBroadcastReceive(activity);
        	webView.setWebChromeClient(videoPlayWebChromeClient);
    	}
    }
    
    public boolean inCustomView() {
        return videoViewDisplayed;
    }

    public void hideCustomView() {
    	this.sendBroadcast(PlayWebViewVideoHelper.ON_HIDE_CUSTOM_VIEW);
    }
    
    /**
     * Enables/Disables all child views in a view group.
     * 
     * @param viewGroup the view group
     * @param enabled <code>true</code> to enable, <code>false</code> to disable
     * the views.
     */
    public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled, View viewToExclude) {
      int childCount = viewGroup.getChildCount();
      for (int i = 0; i < childCount; i++) {
        View view = viewGroup.getChildAt(i);
        
        if (view != viewToExclude) {
        	view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
              enableDisableViewGroup((ViewGroup) view, enabled, viewToExclude);
            }
        }
      }
    }
	
    public void onPause() {
    	this.sendBroadcast(PlayWebViewVideoHelper.ON_PAUSE);
    }

    public void onResume() {
    	this.sendBroadcast(PlayWebViewVideoHelper.ON_RESUME);
    }

    public void onStop() {
    	Log.d("webview", "webview on stop");
        if (inCustomView()) {
            hideCustomView();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (inCustomView()) {
                hideCustomView();
                return true;
            }
        }
        
        return false;
    }
    
    private void sendBroadcast(String message) {
    	if (this.activity != null) {
    		Intent intent = new Intent(message);                	  
    		LocalBroadcastManager.getInstance(this.activity).sendBroadcast(intent);
    	}
    }
    
    class VideoPlayWebChromeClient extends WebChromeClient {
    	private View videoView;
    	private View viewToHide;
    	private WebView webView;
    	
    	public void setViewToHide(View viewToHide) {
    		this.viewToHide = viewToHide;
    	}
    	
    	public void setWebView(WebView webView) {
			this.webView = webView;
		}
    	
    	public void startBroadcastReceive(Activity activity) {
    		LocalBroadcastManager.getInstance(activity).registerReceiver(
    				this.onHideCustomViewReceiver,
    				new IntentFilter(PlayWebViewVideoHelper.ON_HIDE_CUSTOM_VIEW));
    	}
    	
        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
        	Log.d("webview", "webview on show video");
            onShowCustomView(view, callback);
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
        	Log.d("webview", "webview on show video");
        	activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        	
            // if a view already exists then immediately terminate the new one
            if (this.videoView != null) {
                callback.onCustomViewHidden();
                return;
            }
            this.videoView = view;
            this.webView.setVisibility(View.GONE);
            PlayWebViewVideoHelper.this.videoViewDisplayed = true;
            
            videoPresentationContainer = (FrameLayout) activity.findViewById(R.id.videoPresentationContainer);
            videoPresentationContainer.setVisibility(View.VISIBLE);
            videoPresentationContainer.addView(view);
            videoViewCallback = callback;
            
            if (this.viewToHide != null) {
            	this.viewToHide.setVisibility(View.INVISIBLE);
            }
            
            PlayWebViewVideoHelper.enableDisableViewGroup((ViewGroup)activity.findViewById(R.id.mainContainer), false, videoPresentationContainer);
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();
            if (this.videoView == null)
                return;

            this.webView.setVisibility(View.VISIBLE);
            videoPresentationContainer.setVisibility(View.GONE);

            // Hide the custom view.
            this.videoView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            videoPresentationContainer.removeView(videoView);
            videoViewCallback.onCustomViewHidden();
            
            if (this.viewToHide != null) {
            	this.viewToHide.setVisibility(View.VISIBLE);
            }
            PlayWebViewVideoHelper.enableDisableViewGroup((ViewGroup)activity.findViewById(R.id.mainContainer), true, videoPresentationContainer);

            this.videoView = null;
            PlayWebViewVideoHelper.this.videoViewDisplayed = false;
            
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            
            Log.d("webview", "reload webview");
            VideoPlayWebChromeClient.this.webView.reload();         
        }
        
        private BroadcastReceiver onHideCustomViewReceiver = new BroadcastReceiver() {
    		@Override
    		public void onReceive(Context context, Intent intent) {
    			VideoPlayWebChromeClient.this.onHideCustomView();
    		}
    	};
    }
}
