package com.netbloo.magcast.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.netbloo.magcast.constants.Constants;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.netbloo.magcast.helpers.GCMHelper.TAG;

public class HttpRequestHelper {
	public static JSONObject makePostRequest(String url, List<NameValuePair> params, Context context) {
		// Create a new HttpClient and Post Header
    	HttpClient httpclient = new DefaultHttpClient();
    	HttpPost httppost = new HttpPost(url);

        try {
        	if (params == null) {
        		params = new ArrayList<NameValuePair>(1);
        	}
        	
        	// Add your data
            MCPublisher publisher = MCPublisher.sharedPublisher(context);
            params.add(new BasicNameValuePair("device_token", publisher.getDeviceToken()));
            params.add(new BasicNameValuePair("device_id", publisher.getDeviceId()));
            Log.i(TAG, "device_token: "+publisher.getDeviceToken());
            Log.i(TAG, "device_id: "+publisher.getDeviceId());

            UserHelper userHelper = new UserHelper(context);
            if (userHelper.isAuthenticated(context)) {
                params.add(new BasicNameValuePair("user_id", userHelper.getUserId(context).toString()));
                params.add(new BasicNameValuePair("password", userHelper.getPassword(context)));
            }

            httppost.setEntity(new UrlEncodedFormEntity(params));

            // Execute HTTP Post Request
            ResponseHandler<String> responseHandler=new BasicResponseHandler();
            String responseBody = httpclient.execute(httppost, responseHandler);
            
            return new JSONObject(responseBody);
        } catch (MalformedURLException e) {
        	Log.d("sender", "Wrong URL error");
            e.printStackTrace();
        } catch (IOException e) {
        	Log.d("sender", "IO error");
            e.printStackTrace();
        } catch (JSONException e) {
        	Log.d("sender", "JSON parse error");
			e.printStackTrace();
		}
        
        return null;
    }

    //+ (void)logEventWithEventCode:(NSString*)eventCode

    public static void logEventWithEventCode(final String eventCode, Context context) throws JSONException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String userCode = preferences.getString(Constants.EVENT_USER_CODE,null);
        String fullURL = Constants.MAGSFAST_EVENTS_URL+"publication_id="+Constants.MAGAZINE_ID+"&user_code="+userCode+"&event_code="+eventCode;
        Log.i(TAG, "logEventWithEventCode: testing: "+fullURL);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(fullURL)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {

                // Observe reason of failure using
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    // Use response here
                    Log.i(TAG, "logEventWithEventCode: "+eventCode);
                }
                else{
                    // Observe error
                }
            }
        });
    }





    //+ (void)logEventWithEventCode:(NSString*)eventCode andCustomVars:(NSDictionary*)vars
    public static void logEventWithEventCodeAndCustomVars(final String eventCode, Map<String,String> vars,Context context) throws JSONException, UnsupportedEncodingException {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String userCode = preferences.getString(Constants.EVENT_USER_CODE,null);
        String customVars = HttpRequestHelper.formatCustomVars(vars);
        String fullURL = Constants.MAGSFAST_EVENTS_URL+"publication_id="+Constants.MAGAZINE_ID+"&user_code="+userCode+"&event_code="+eventCode+"&custom_vars="+customVars;
        Log.i(TAG, "logEventWithEventCode: testing: "+fullURL);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(fullURL)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {

                // Observe reason of failure using
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    // Use response here
                    Log.i(TAG, "logEventWithEventCode: "+eventCode);
                }
                else{
                    // Observe error
                }
            }
        });
    }

    //+ (NSString*)formatCustomVars:(NSDictionary*)vars
    public static String formatCustomVars(Map<String,String> vars) throws UnsupportedEncodingException {
	    String full = "";
	    for (Map.Entry<String,String> entry : vars.entrySet()){
            String key = entry.getKey();
            key = key.replaceAll(" ", "_");
            String value = entry.getValue();
	        if (full.isEmpty()){
	            full = key+"__"+value;
            }else{
                full = full+"||"+key+"__"+value;
            }
        }
        full = full.toLowerCase();
	    //full = URLEncoder.encode(full, "utf-8");
        full = TextUtils.htmlEncode(full);

	    return full;
    }
}
