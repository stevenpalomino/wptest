package com.netbloo.magcast.helpers;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.constants.Constants;

import android.content.Context;
import android.os.Build;

public class MCErrorLog {
	
	static class MCErrorEntity {
		public String code;
		public String description;
		public String device;
		public String deviceToken;
		public String name;
	}
	
	public static void sendError(Throwable exception, Context context) {
		StringWriter sw = new StringWriter();
        exception.printStackTrace(new PrintWriter(sw));
        String stacktrace = sw.toString();
        
		MCErrorEntity error = new MCErrorEntity();
		error.code = "Exception";
		error.description = exception.getLocalizedMessage() + "\n" + stacktrace;
		error.name = exception.getMessage();
		
		MCErrorLog.sendError(error, context);
	}

	private static void sendError(MCErrorEntity error, Context context) {
		MCPublisher publisher = MCPublisher.sharedPublisher(null);
		error.deviceToken = publisher.getDeviceToken();
		if ((error.deviceToken == null) || (error.deviceToken.length() == 0)) {
			error.deviceToken = publisher.getDeviceId();
		}
		
		String applicationName = context.getString(R.string.app_name);
		if ((publisher.getMagazine() != null) 
				&& (publisher.getMagazine().getName() != null)) {
			applicationName = publisher.getMagazine().getName();
		}
		
		error.device = MCErrorLog.getDeviceName() + " " + Build.VERSION.RELEASE;
		
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("device_token", error.deviceToken));
		params.add(new BasicNameValuePair("error_code", error.code));
		params.add(new BasicNameValuePair("error_description", error.description));
		params.add(new BasicNameValuePair("error_name", error.name));
		params.add(new BasicNameValuePair("error_app_name", applicationName));
		params.add(new BasicNameValuePair("error_device", error.device));
		
		HttpRequestHelper.makePostRequest(Constants.ERROR_LOG_URL, params, context);
	}
	
	private static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer) + " " + model;
		}
	}

	private static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}
}
