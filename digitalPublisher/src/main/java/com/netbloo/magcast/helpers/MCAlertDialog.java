package com.netbloo.magcast.helpers;

import com.bebaibcebe.badabebaibcebe.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.WindowManager.BadTokenException;

public class MCAlertDialog {
	public static void showAlert(String title, String text, Context context) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setMessage(text);
		alertDialogBuilder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	            // continue with delete
	        }
	     });
		
		AlertDialog alertDialog = alertDialogBuilder.create();
		try {
			alertDialog.show();
		} catch(BadTokenException e) {
			e.printStackTrace();
		}
	}
	
	public static void showErrorWithText(String errorText, Context context) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle(context.getString(R.string.error));
		alertDialogBuilder.setMessage(errorText);
		alertDialogBuilder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	            // continue with delete
	        }
	     });
		
		AlertDialog alertDialog = alertDialogBuilder.create();
		try {
			alertDialog.show();
		} catch(BadTokenException e) {
			e.printStackTrace();
		}
	}
}
