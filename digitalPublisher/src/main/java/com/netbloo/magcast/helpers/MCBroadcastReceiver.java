package com.netbloo.magcast.helpers;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.activities.HomeViewActivity;
import com.netbloo.magcast.constants.Constants;
import com.netbloo.magcast.models.MCProduct;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

public class MCBroadcastReceiver extends BroadcastReceiver {

	static final String TAG = "GCMDemo";
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    Context ctx;
    @Override
    public void onReceive(Context context, Intent intent) {
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        ctx = context;
        String messageType = gcm.getMessageType(intent);
        if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
            sendNotification("Send error: " + intent.getExtras().toString());
        } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
            sendNotification("Deleted messages on server: " +
                    intent.getExtras().toString());
        } else {
        	Log.d(TAG, "Received notification " + intent.getExtras().toString());

        	String contentAvailable = intent.getExtras().getString("content-available", "1");
        	if (contentAvailable.equals("1")) {
        		receivedContentAvailableMessage(intent.getExtras());
        	} else {
        		sendNotification(intent.getExtras().getString("message"));
        	}
        }
        setResultCode(Activity.RESULT_OK);
    }

    // Put the GCM message into a notification and post it.
	@SuppressWarnings("deprecation")
	private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        
        final PackageManager pm = ctx.getApplicationContext().getPackageManager();
        ApplicationInfo ai;
        try {
            ai = pm.getApplicationInfo( ctx.getPackageName(), 0);
        } catch (final NameNotFoundException e) {
            ai = null;
        }
        final String applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(ctx)
        .setSmallIcon(R.drawable.ic_launcher)
        .setContentTitle(applicationName)
        .setContentText(msg);
        
     // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this.ctx, HomeViewActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this.ctx);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(HomeViewActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        // mId allows you to update the notification later on.
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.getNotification());
    }

    private void receivedContentAvailableMessage(Bundle extras) {
    	String productId = extras.getString("productId", null);
    	if (productId != null) {
    		new OnContentAvailableAsyncTask().execute(extras);
    	}
    }
    
    private class OnContentAvailableAsyncTask extends AsyncTask<Bundle, Integer, Void> {
		@Override
        protected void onPostExecute(Void msg) {
        	super.onPostExecute(msg);
        }

		@Override
		protected Void doInBackground(Bundle... extras) {
			String productId = extras[0].getString("productId");
			
			// Try to get product from publisher object
    		MCPublisher publisher = MCPublisher.sharedPublisher(ctx);
    		MCProduct product = publisher.issueWithProductId(productId);
    		if (product == null) {
    			// If app doesn't now anything about product then get info from server
    			String url = Constants.SERVER_PRODUCTS_URL + "/" + productId;
            	JSONObject response = HttpRequestHelper.makePostRequest(url, null, ctx);
            	if (response != null) {
            		try {
    					String status = response.getString("status");
    					if (status.equals("success")) {
    	            		product = new MCProduct(response.getJSONObject("result"), ctx);
    	            	} else {
    	            		Log.d(TAG, "Failed to retrieve product info from server");
    	            		return null;
    	            	}
    				} catch (JSONException e) {
    					Log.d(TAG, "Failed to retrieve product info from server");
                		return null;
    				}
            	} else {
            		Log.d(TAG, "Failed to retrieve product info from server");
            		return null;
            	}
    		}
    		
    		product.download();
    		
    		sendNotification(extras[0].getString("message", ""));
    		
    		PlayStoreBillingHelper playstoreBillingHelper = new PlayStoreBillingHelper();
    		publisher.requestProductsFromServer(playstoreBillingHelper);
			
            return null;
		}
	}
}
