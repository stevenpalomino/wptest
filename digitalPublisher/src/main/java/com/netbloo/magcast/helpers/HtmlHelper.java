package com.netbloo.magcast.helpers;

/**
 * Created by izmaylav on 5/3/16.
 */
public class HtmlHelper {

    public static String prepareAppHtml(String html) {
        // Put right path to fonts
        html = html.replace("\"fonts/", "\"file:///android_asset/fonts/");

        String fonts = "<style type=\"text/css\">" +
                "@font-face {" +
                "font-family: 'Oswald';" +
                "src: url('file:///android_asset/fonts/Oswald-Bold.otf');" +
                "}" +
                "@font-face {" +
                "font-family: 'Lucida Grande';" +
                "src: url('file:///android_asset/fonts/LucidaGrande.ttc');" +
                "}" +
                "@font-face {" +
                "font-family: \"Avenir\";" +
                "src: url(\"file:///android_asset/fonts/Avenir-Book.otf\");" +
                "}" +

                "@font-face {" +
                "font-family: \"DIN-Regular\";" +
                "src: url(\"file:///android_asset/fonts/DIN-Regular.ttf\");" +
                "}" +

                "@font-face {" +
                "font-family: \"LucidaGrande\";" +
                "src: url(\"file:///android_asset/fonts/LucidaGrande.ttc\");" +
                "}" +

                "@font-face {" +
                "font-family: \"MyriadPro\";" +
                "src: url(\"file:///android_asset/fonts/MyriadPro.otf\");" +
                "}" +

                "@font-face {" +
                "font-family: \"steelfis\";" +
                "src: url(\"file:///android_asset/fonts/steelfis.ttf\");" +
                "}" +

                "@font-face {" +
                "font-family: \"HelveticaNeue\";" +
                "src: url(\"file:///android_asset/fonts/HelveticaNeue.dfont\");" +
                "}" +

                "@font-face {" +
                "font-family: \"Merriweather\";" +
                "src: url(\"file:///android_asset/fonts/Merriweather-Bold.ttf\");" +
                "font-weight: bold;" +
                "font-style: normal;" +
                "}" +

                "@font-face {" +
                "font-family: \"Merriweather\";" +
                "src: url(\"file:///android_asset/fonts/Merriweather-BoldItalic.ttf\");" +
                "font-weight: bold;" +
                "font-style: italic;" +
                "}" +

                "@font-face {" +
                "font-family: \"Merriweather\";" +
                "src: url(\"file:///android_asset/fonts/Merriweather-Italic.ttf\");" +
                "font-weight: normal;" +
                "font-style: italic;" +
                "}" +

                "@font-face {" +
                "font-family: \"Merriweather\";" +
                "src: url(\"file:///android_asset/fonts/Merriweather-Regular.ttf\");" +
                "font-weight: normal;" +
                "font-style: normal;" +
                "}" +

                "@font-face {" +
                "font-family: \"Lato\";" +
                "src: url(\"file:///android_asset/fonts/Lato-Bold.ttf\");" +
                "font-weight: bold;" +
                "font-style: normal;" +
                "}" +

                "@font-face {" +
                "font-family: \"Lato\";" +
                "src: url(\"file:///android_asset/fonts/Lato-BoldItalic.ttf\");" +
                "font-weight: bold;" +
                "font-style: italic;" +
                "}" +

                "@font-face {" +
                "font-family: \"Lato\";" +
                "src: url(\"file:///android_asset/fonts/Lato-Italic.ttf\");" +
                "font-weight: normal;" +
                "font-style: italic;" +
                "}" +

                "@font-face {" +
                "font-family: \"Lato\";" +
                "src: url(\"file:///android_asset/fonts/Lato-Regular.ttf\");" +
                "font-weight: normal;" +
                "font-style: normal;" +
                "}" +

                "@font-face {" +
                "font-family: \"Roboto\";" +
                "src: url(\"file:///android_asset/fonts/Roboto-Bold.ttf\");" +
                "font-weight: bold;" +
                "font-style: normal;" +
                "}" +

                "@font-face {" +
                "font-family: \"Roboto\";" +
                "src: url(\"file:///android_asset/fonts/Roboto-BoldItalic.ttf\");" +
                "font-weight: bold;" +
                "font-style: italic;" +
                "}" +

                "@font-face {" +
                "font-family: \"Roboto\";" +
                "src: url(\"file:///android_asset/fonts/Roboto-Italic.ttf\");" +
                "font-weight: normal;" +
                "font-style: italic;" +
                "}" +

                "@font-face {" +
                "font-family: \"Roboto\";" +
                "src: url(\"file:///android_asset/fonts/Roboto-Regular.ttf\");" +
                "font-weight: normal;" +
                "font-style: normal;" +
                "}" +

                "@font-face {" +
                "font-family: \"Asap\";" +
                "src: url(\"file:///android_asset/fonts/Asap-Bold.ttf\");" +
                "font-weight: bold;" +
                "font-style: normal;" +
                "}" +

                "@font-face {" +
                "font-family: \"Asap\";" +
                "src: url(\"file:///android_asset/fonts/Asap-BoldItalic.ttf\");" +
                "font-weight: bold;" +
                "font-style: italic;" +
                "}" +

                "@font-face {" +
                "font-family: \"Asap\";" +
                "src: url(\"file:///android_asset/fonts/Asap-Italic.ttf\");" +
                "font-weight: normal;" +
                "font-style: italic;" +
                "}" +

                "@font-face {" +
                "font-family: \"Asap\";" +
                "src: url(\"file:///android_asset/fonts/Asap-Regular.ttf\");" +
                "font-weight: normal;" +
                "font-style: normal;" +
                "}" +

                "@font-face {" +
                "font-family: \"Open Sans\";" +
                "src: url(\"file:///android_asset/fonts/OpenSans-Bold.ttf\");" +
                "font-weight: bold;" +
                "font-style: normal;" +
                "}" +

                "@font-face {" +
                "font-family: \"Open Sans\";" +
                "src: url(\"file:///android_asset/fonts/OpenSans-BoldItalic.ttf\");" +
                "font-weight: bold;" +
                "font-style: italic;" +
                "}" +

                "@font-face {" +
                "font-family: \"Open Sans\";" +
                "src: url(\"file:///android_asset/fonts/OpenSans-Italic.ttf\");" +
                "font-weight: normal;" +
                "font-style: italic;" +
                "}" +

                "@font-face {" +
                "font-family: \"Open Sans\";" +
                "src: url(\"file:///android_asset/fonts/OpenSans-Regular.ttf\");" +
                "font-weight: normal;" +
                "font-style: normal;" +
                "}" +

                "@font-face {" +
                "font-family: \"HelveticaNeue\";" +
                "src: url(\"file:///android_asset/fonts/HelveticaNeue.dfont\");" +
                "}";
        html = html.replace("<style type=\"text/css\">", fonts);

        return html;
    }

}
