package com.netbloo.magcast.helpers;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.bebaibcebe.badabebaibcebe.R;

public class MCAnalyticsHelper {
	public static void logEvent(String eventDescription, boolean trackOnFlurry, Context context) {
		// May return null if EasyTracker has not yet been initialized with a property
		// ID.
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
		Tracker easyTracker = analytics.newTracker(R.string.ga_trackingId);

		if (easyTracker != null) {
			// This screen name value will remain set on the tracker and sent with
			// hits until it is set to a new value or to null.
			easyTracker.setScreenName(eventDescription);
			easyTracker.send(new HitBuilders.ScreenViewBuilder().build());
		}
	}
}
