package com.netbloo.magcast.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;

import com.netbloo.magcast.constants.Constants;
import com.netbloo.magcast.models.MCMagazine;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Model for work with members area user
 *
 * Created by izmaylav on 4/29/16.
 */
public class UserHelper {

    private static String AUTHENTICATED_KEY = "UserIsAuthenticatedKey";
    private static String AUTHENTICATED_USER_ID_KEY = "UserAuthenticatedUserIdKey";
    private static String AUTHENTICATED_PASSWORD_KEY = "UserAuthenticatedPasswordKey";

    public static String RESETED_PASSWORD = "UserResetedPasswordNotification";
    public static String SIGNED_IN = "UserIsSignedInNotification";
    public static String SIGNED_OUT = "UserIsSignedOutNotification";
    public static String FAILED_TO_RESET_PASSWORD = "UserFailedToResetPasswordNotification";
    public static String FAILED_TO_SIGN_IN = "UserFailedToSignInNotification";
    public static String NOTIFICATION_MESSAGE = "message";

    protected LocalBroadcastManager broadCastManager;

    public UserHelper(Context context) {
        this.broadCastManager = LocalBroadcastManager.getInstance(context);
    }

    public boolean isAuthenticated(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Activity.MODE_PRIVATE);
        return preferences.getBoolean(UserHelper.AUTHENTICATED_KEY, false);
    }

    public Integer getUserId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Activity.MODE_PRIVATE);
        return preferences.getInt(UserHelper.AUTHENTICATED_USER_ID_KEY, 0);
    }

    public String getPassword(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Activity.MODE_PRIVATE);
        return preferences.getString(UserHelper.AUTHENTICATED_PASSWORD_KEY, "");
    }

    public void signIn(String email, String passsword, final Context context) {
        final String url = Constants.SERVER_USERS_URL + "/signIn";
        final ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("password", passsword));

        new Thread(new Runnable() {
            @Override
            public void run() {
                JSONObject response = HttpRequestHelper.makePostRequest(url, params, context);
                parseSignInResponse(response, context);
            }
        }).start();
    }

    public void signUp(String name, String email, String passsword, final Context context) {
        final String url = Constants.SERVER_USERS_URL + "/signUp";
        final ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("password", passsword));

        new Thread(new Runnable() {
            @Override
            public void run() {
                JSONObject response = HttpRequestHelper.makePostRequest(url, params, context);
                parseSignInResponse(response, context);
            }
        }).start();
    }

    private void parseSignInResponse(JSONObject response, Context context) {
        if (response == null) {
            sendUserFailedToSignInNotification("Server error");
            return;
        }

        try {
            String status = response.getString("status");
            if (status.equals("success")) {
                JSONObject result = response.getJSONObject("result");
                Integer userId = result.getInt("userId");
                String password = result.getString("password");

                SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt(UserHelper.AUTHENTICATED_USER_ID_KEY, userId);
                editor.putString(UserHelper.AUTHENTICATED_PASSWORD_KEY, password);
                editor.putBoolean(UserHelper.AUTHENTICATED_KEY, true);
                editor.apply();

                sendUserSignedInNotification();
            } else if (status.equals("error")) {
                String result = response.getString("result");
                sendUserFailedToSignInNotification(result);
            } else {
                sendUserFailedToSignInNotification("Server error");
            }
        } catch (JSONException e) {
            e.printStackTrace();

            sendUserFailedToSignInNotification(e.getLocalizedMessage());
        }
    }

    public void signOut(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("MyPreferences", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(UserHelper.AUTHENTICATED_USER_ID_KEY);
        editor.remove(UserHelper.AUTHENTICATED_PASSWORD_KEY);
        editor.putBoolean(UserHelper.AUTHENTICATED_KEY, false);
        editor.apply();

        sendUserSignedOutNotification();
    }

    public void resetPassword(String email, final Context context) {
        final String url = Constants.SERVER_USERS_URL + "/resetPassword";
        final ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("email", email));

        new Thread(new Runnable() {
            @Override
            public void run() {
                JSONObject response = HttpRequestHelper.makePostRequest(url, params, context);
                parseResetPasswordResponse(response);
            }
        }).start();
    }

    private void parseResetPasswordResponse(JSONObject response) {
        if (response == null) {
            sendUserFailedToResetPasswordNotification("Server error");
            return;
        }

        try {
            String status = response.getString("status");
            if (status.equals("success")) {
                sendUserResetedPassword();
            } else if (status.equals("error")) {
                String result = response.getString("result");
                sendUserFailedToResetPasswordNotification(result);
            } else {
                sendUserFailedToResetPasswordNotification("Server error");
            }
        } catch (JSONException e) {
            e.printStackTrace();

            sendUserFailedToResetPasswordNotification(e.getLocalizedMessage());
        }
    }

    private void sendUserSignedInNotification() {
        Intent intent = new Intent(UserHelper.SIGNED_IN);
        broadCastManager.sendBroadcast(intent);

        intent = new Intent(MCMagazine.MAGAZINE_STATUS_CHANGED);
        broadCastManager.sendBroadcast(intent);
    }

    private void sendUserSignedOutNotification() {
        Intent intent = new Intent(UserHelper.SIGNED_OUT);
        broadCastManager.sendBroadcast(intent);
    }

    private void sendUserFailedToSignInNotification(String message) {
        Intent intent = new Intent(UserHelper.FAILED_TO_SIGN_IN);
        intent.putExtra(UserHelper.NOTIFICATION_MESSAGE, message);
        broadCastManager.sendBroadcast(intent);
    }

    private void sendUserResetedPassword() {
        Intent intent = new Intent(UserHelper.RESETED_PASSWORD);
        broadCastManager.sendBroadcast(intent);
    }

    private void sendUserFailedToResetPasswordNotification(String message) {
        Intent intent = new Intent(UserHelper.FAILED_TO_RESET_PASSWORD);
        intent.putExtra(UserHelper.NOTIFICATION_MESSAGE, message);
        broadCastManager.sendBroadcast(intent);
    }
}
