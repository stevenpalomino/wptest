package com.netbloo.magcast.helpers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.constants.Constants;
import com.netbloo.magcast.models.MCMagazine;
import com.netbloo.magcast.models.MCProduct;
import com.netbloo.magcast.models.MCResource;
import com.netbloo.magcast.models.MCSubscriptionProduct;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;
import android.preference.PreferenceManager;


import static android.content.Context.MODE_PRIVATE;
import static com.netbloo.magcast.helpers.GCMHelper.TAG;

public final class MCPublisher {
	private Context context;

	// Cache
	private final static String CACHE_FILENAME = "cacheData.txt"; 
	
	// Notification sending
	private LocalBroadcastManager broadCastManager;
	public final static String PUBLISHER_STARTED_TO_UPDATE = "PublisherStartedToUpdate";
	public final static String PUBLISHER_DID_UPDATE = "PublisherDidUpdate";
	public final static String PUBLISHER_FAILED_TO_UPDATE = "PublisherFailedToUpdate";
	
	// Store Singleton object
	private static MCPublisher sharedPublisher;
	
	// Publisher attributes	
	private boolean firstLoad;
	private boolean hasSubscribersOnlyIssues;
	private boolean hasSpecialIssues;
	private boolean issuesListChanged;
	private MCMagazine magazine;
	private boolean online;
	private JSONObject parsedResponseResult;
	private ArrayList<MCProduct> products;	
	private boolean ready;
	private ArrayList<MCResource> resourcesList;
	private ArrayList<MCSubscriptionProduct> subscriptionProducts;
	
	private MCPublisher(Context context) {
		this.context = context;
		this.firstLoad = true;
		
		GCMHelper.getSharedHelper().registerDevice(context.getApplicationContext());
	}
	
	public ArrayList<MCProduct> getProducts() {
		return products;
	}

	public ArrayList<MCSubscriptionProduct> getSubscriptionProducts() {
		return subscriptionProducts;
	}
	
	public boolean isIssuesListChanged() {
		return issuesListChanged;
	}
	
	public boolean isOnline() {
		return this.online;
	}

	public boolean isReady() {
		return this.ready;
	}
	
	public void setBroadCastManager(LocalBroadcastManager broadCastManager) {
		this.broadCastManager = broadCastManager;
	}
	
	// Singleton function
	public static MCPublisher sharedPublisher(Context context) {
		if (sharedPublisher == null) {
			sharedPublisher = new MCPublisher(context);
		}
		
		return sharedPublisher;
	}
	
	public boolean hasSubscribersOnlyIssues() {
		return hasSubscribersOnlyIssues;
	}

	public boolean hasSpecialIssues() {
		return hasSpecialIssues;
	}
	
	public MCMagazine getMagazine() {
		return this.magazine;
	}
	
	public String getDeviceToken() {
		GCMHelper gcmHelper = GCMHelper.getSharedHelper();
		Calendar rightNow = Calendar.getInstance();
		int currentHourIn24Format = rightNow.get(Calendar.MINUTE);
		Log.i(TAG, "~~~~~~~~~ getDeviceToken: "+ gcmHelper.getRegistrationId()+currentHourIn24Format);// return the hour in 24 hrs format (ranging from 0-23)
		//return gcmHelper.getRegistrationId()+currentHourIn24Format;
		return gcmHelper.getRegistrationId();
	}
	
	public String getDeviceId() {
		return "android_" + Secure.getString(this.context.getContentResolver(), Secure.ANDROID_ID);
	}
	
	// Make request to server to get data about magazine and issues
	public void requestProductsFromServer(final PlayStoreBillingHelper playStoreBillingHelper) {
		this.ready = false;
		
		// If in background then igonre loading of data from the server
		if (this.broadCastManager == null) {
			return;
		}
		
		Intent intent = new Intent(MCPublisher.PUBLISHER_STARTED_TO_UPDATE);                	  
    	this.broadCastManager.sendBroadcast(intent);
		
		new Thread(new Runnable() {
	        public void run() {
	            try {
	            	// Retrieve products from server
	            	MCPublisher.this.parsedResponseResult = HttpRequestHelper.makePostRequest(Constants.NEW_SERVER_PRODUCTS_URL, null, context);
					//Log.i(TAG, "Request New URL: "+MCPublisher.this.parsedResponseResult.toString(2));
	                if ((MCPublisher.this.parsedResponseResult != null)
	                		&& MCPublisher.this.parsedResponseResult.getString("status").equals("success")) {
	                	MCPublisher.this.parsedResponseResult = MCPublisher.this.parsedResponseResult.getJSONObject("result");

	                	parseData();
	                	playStoreBillingHelper.restorePurchases();
	                	retrieveProductsFromStore(playStoreBillingHelper);
		                saveDataInCache();
		                cleanNotRetrievedSubscriptions();

		                MCPublisher.this.ready = true;
		                Intent intent = new Intent(MCPublisher.PUBLISHER_DID_UPDATE);
		                MCPublisher.this.broadCastManager.sendBroadcast(intent);

		                MCPublisher.this.online = true;
		                MCPublisher.this.firstLoad = false;
	                } else {
						MCPublisher.this.parsedResponseResult = HttpRequestHelper.makePostRequest(Constants.SERVER_PRODUCTS_URL, null, context);
						//Log.i(TAG, "Request OLD URL: "+MCPublisher.this.parsedResponseResult.toString(2));
						if ((MCPublisher.this.parsedResponseResult != null)
								&& MCPublisher.this.parsedResponseResult.getString("status").equals("success")) {
							MCPublisher.this.parsedResponseResult = MCPublisher.this.parsedResponseResult.getJSONObject("result");

							parseData();
							playStoreBillingHelper.restorePurchases();
							retrieveProductsFromStore(playStoreBillingHelper);
							saveDataInCache();
							cleanNotRetrievedSubscriptions();

							MCPublisher.this.ready = true;
							Intent intent = new Intent(MCPublisher.PUBLISHER_DID_UPDATE);
							MCPublisher.this.broadCastManager.sendBroadcast(intent);

							MCPublisher.this.online = true;
							MCPublisher.this.firstLoad = false;
						}else{
							MCPublisher.this.getDataFromCache();
							cleanNotRetrievedSubscriptions();
							failedToRetrieveDataFromServer();
						}

	                }
	            } catch (JSONException e) {
	            	MCPublisher.this.getDataFromCache();
	            	cleanNotRetrievedSubscriptions();
	            	failedToRetrieveDataFromServer();
				}
	        }
	    }).start();
	}

	// Make request to server to get data about magazine and issues
	public void requestProductsFromServerWithVerificationCode(final PlayStoreBillingHelper playStoreBillingHelper, final String validationCode) {
		this.ready = false;

		// If in background then igonre loading of data from the server
		if (this.broadCastManager == null) {
			return;
		}

		Intent intent = new Intent(MCPublisher.PUBLISHER_STARTED_TO_UPDATE);
		this.broadCastManager.sendBroadcast(intent);

		new Thread(new Runnable() {
			public void run() {
				try {
					// Retrieve products from server
					String validate = Constants.NEW_SERVER_URL+"&a=validate";
					ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair("validationCode", validationCode));

					MCPublisher.this.parsedResponseResult = HttpRequestHelper.makePostRequest(validate, params, context);
					Log.i(TAG, "JSON: "+MCPublisher.this.parsedResponseResult.toString(2));
                    if ((MCPublisher.this.parsedResponseResult != null)
                            && MCPublisher.this.parsedResponseResult.getString("status").equals("success")) {

                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("isUsingNewURL", true);
                        editor.apply();

						Log.i(TAG, "run: ~~~~~~~~ SUCCESS!");
						Handler handler = new Handler(Looper.getMainLooper());
						handler.post(new Runnable() {

							@Override
							public void run() {
								//Toast.makeText(context,"Subscription successfully unlocked",Toast.LENGTH_SHORT).show();
								MCAlertDialog.showErrorWithText("Purchase Successfully Unlocked", context);

							}
						});

						MCPublisher.this.requestProductsFromServer(playStoreBillingHelper);
					} else {
						MCPublisher.this.requestProductsFromServer(playStoreBillingHelper);
					}
				} catch (JSONException e) {
					MCPublisher.this.getDataFromCache();
					cleanNotRetrievedSubscriptions();
					failedToRetrieveDataFromServer();
				}
			}
		}).start();
	}

	public boolean isHasBranchUserCode(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if(preferences.contains(Constants.EVENT_USER_CODE)){
            return true;
        }else{
            return false;
        }
    }

	private void parseData() {
		getMagazineInfoFromResponse();
        getProductsInfoFromResponse();
        getSubscriptionProductsInfoFromResponse();
        getResourcesInfoFromResponse();
	}
	
	private void getMagazineInfoFromResponse() {
		JSONObject magazineJSONObject;
		try {
			magazineJSONObject = parsedResponseResult.getJSONObject("details");
			this.magazine = new MCMagazine(magazineJSONObject, this.context);
		} catch (JSONException e) {
		}
		
		if (this.firstLoad) {
			// TODO : perform additional actions at first load
		}
	}
	
	private void getProductsInfoFromResponse() {
		this.hasSpecialIssues = false;
		this.hasSubscribersOnlyIssues = false;
		
		try {
			JSONArray issues = parsedResponseResult.getJSONArray("issues");
			if (this.products == null) {
				this.products = new ArrayList<MCProduct>();
			}
			
			// Parse and create list of products
			int issuesCount = issues.length();
			JSONObject productJSON = null;
			MCProduct product = null;
			MCProduct checkProduct = null;
			ArrayList<MCProduct> newProductsList = new ArrayList<MCProduct>();
			for (int issueIndex = 0; issueIndex < issuesCount; ++issueIndex) {
				productJSON = issues.getJSONObject(issueIndex);
				product = new MCProduct(productJSON, context);
				if (product.isAvailable()) {
					checkProduct = issueWithIssueNo(product.getIssueNo());
					if (checkProduct == null) {
						this.issuesListChanged = true;
					} else {
						product = checkProduct;
						if (product.updateProduct(productJSON)) {
							this.issuesListChanged = true;
						}
					}
					newProductsList.add(product);
					
					// TODO Add update need check
					
					if (product.isSpecial()) {
						this.hasSpecialIssues = true;
					}
					
					if (product.isSubscribersOnly()) {
						this.hasSubscribersOnlyIssues = true;
					}
				}
			}
			
			this.products = newProductsList;
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private void getSubscriptionProductsInfoFromResponse() {
		this.subscriptionProducts = new ArrayList<MCSubscriptionProduct>();
		
		try {
			JSONArray subscriptions = parsedResponseResult.getJSONObject("details").getJSONArray("androidMagproducts");
			
			JSONObject subscription = null;
			MCSubscriptionProduct subscriptionProduct;
			for (int subscriptionIndex = 0; subscriptionIndex < subscriptions.length(); ++ subscriptionIndex) {
				subscription = subscriptions.getJSONObject(subscriptionIndex);
				subscriptionProduct = new MCSubscriptionProduct(subscription, this.context);
				this.subscriptionProducts.add(subscriptionProduct);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private void getResourcesInfoFromResponse() {
		try {
			JSONArray resourcesFromResponse = this.parsedResponseResult.getJSONArray("resources");
			
			if (resourcesFromResponse != null) {
		        this.resourcesList = new ArrayList<MCResource>();
		        
		        int resourcesCount = resourcesFromResponse.length();
		        MCResource resourceItem;
		        for (int resourceIndex = 0; resourceIndex < resourcesCount; ++resourceIndex) {
		            resourceItem = new MCResource(resourcesFromResponse.getJSONObject(resourceIndex));
		            this.resourcesList.add(resourceItem);
		        }
		    }
		} catch (JSONException e) {
		}
	}
	
	public void retrieveProductsFromStore(PlayStoreBillingHelper playStoreBillingHelper) {
		playStoreBillingHelper.retrieveProductsFromStore(this.products, this.subscriptionProducts);
	}
	
	// Caching
	
	private void saveDataInCache() {
		if (this.context == null) {
			return;
		}
		
		try {
			String dataToCache = this.parsedResponseResult.toString();
			FileOutputStream fos = this.context.openFileOutput(MCPublisher.CACHE_FILENAME, MODE_PRIVATE);
			fos.write(dataToCache.getBytes());
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void getDataFromCache() {
		try {
			// Read data from cache file
			FileInputStream fis = this.context.openFileInput(MCPublisher.CACHE_FILENAME);
			byte[] buffer = new byte[1024];
			StringBuffer cachedData = new StringBuffer("");
			while (fis.read(buffer) != -1) {
				cachedData.append(new String(buffer));
			}
			fis.close();
			
			this.parsedResponseResult = new JSONObject(cachedData.toString());
			
			parseData();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private void failedToRetrieveDataFromServer() {
		this.ready = true;
		
		Intent intent = new Intent(MCPublisher.PUBLISHER_FAILED_TO_UPDATE);                	  
    	this.broadCastManager.sendBroadcast(intent);
	}
	
	private void cleanNotRetrievedSubscriptions() {
		ArrayList<MCSubscriptionProduct> cleanSubscriptionsList = new ArrayList<MCSubscriptionProduct>();
		
		if (this.subscriptionProducts != null) {
			for (MCSubscriptionProduct subscriptionProduct : this.subscriptionProducts) {
				if (subscriptionProduct.isHasPrice() || subscriptionProduct.isFree()) {
					cleanSubscriptionsList.add(subscriptionProduct);
				}
			}
		}
		
		this.subscriptionProducts = cleanSubscriptionsList;
	}
	
	// Issues
	
	public int numberOfIssues() {
	    if(this.isReady() && (this.products != null)) {
	        return this.products.size();
	    } else {
	        return 0;
	    }
	}
	
	public void addIssue(MCProduct issue) {
		if (this.products == null) {
			this.products = new ArrayList<MCProduct>();
		}
		
		this.products.add(issue);
	}

	public MCProduct issueAtIndex(int index) {
	    if (this.numberOfIssues() <= index) {
	        return null;
	    }
	    
	    return this.products.get(index);
	}
	
	public MCProduct issueWithIssueNo(String issueNo) {
		if (this.products == null) {
			return null;
		}
		
		for (MCProduct product : this.products) {
			if (issueNo.equals(product.getIssueNo())) {
				return product;
			}
		}
		
		return null;
	}
	
	public MCProduct issueWithProductId(String productId) {
		if (this.products == null) {
			return null;
		}
		
		for (MCProduct product : this.products) {
			if (productId.equals(product.getProductId())
					|| productId.equals(product.getIosProductId())) {
				return product;
			}
		}
		
		return null;
	}
	
	public MCProduct issueToDownloadAfterSubscription() {
		for (MCProduct product : this.products) {
			if (product.isSpecial() == false) {
				return product;
			}
		}
		
		return null;
	}
	
	// Subscriptions
	
	public int numberOfSubscriptionProducts() {
		if(this.isReady() && (this.subscriptionProducts != null)) {
	        return this.subscriptionProducts.size();
	    } else {
	        return 0;
	    }
	}
	
	public MCSubscriptionProduct subscriptionProductAtIndex(int index) {
		if (this.numberOfSubscriptionProducts() <= index) {
	        return null;
	    }
	    
	    return this.subscriptionProducts.get(index);
	}
	
	public MCSubscriptionProduct subscriptionProductWithMagsubNo(int magsubNo) {
		for (MCSubscriptionProduct subscriptionProduct : this.subscriptionProducts) {
			if (magsubNo == subscriptionProduct.getMagsubNo()) {
				return subscriptionProduct;
			}
		}
		
		return null;
	}
	
	public MCSubscriptionProduct subscriptionProductWithProductId(String productId) {
		String disabledProductId = productId + "_disabled";
		
		for (MCSubscriptionProduct subscriptionProduct : this.subscriptionProducts) {
			if (productId.equals(subscriptionProduct.getProductId())
					|| disabledProductId.equals(subscriptionProduct.getProductId())) {
				return subscriptionProduct;
			}
		}
		
		return null;
	}
	
	// Resources
	
	public int numberOfResources() {
		if (this.resourcesList != null) {
			return this.resourcesList.size();
		} else {
			return 0;
		}
	}
	
	public MCResource resourceAtIndex(int index) {
		return this.resourcesList.get(index);
	}
}
