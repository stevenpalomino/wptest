package com.netbloo.magcast.constants;

import com.bebaibcebe.badabebaibcebe.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FolderSelectItem extends RelativeLayout {
	private TextView title;
	
	public FolderSelectItem(Context context) {
		super(context);
		
		LayoutInflater layoutInflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.folder_select_item_view, this);
		
		this.title = (TextView) findViewById(R.id.title);
	}
	
	public void setTitle(String title) {
		this.title.setText(title);
	}
	
}
