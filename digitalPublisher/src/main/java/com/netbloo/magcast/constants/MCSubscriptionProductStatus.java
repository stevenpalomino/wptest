package com.netbloo.magcast.constants;

public class MCSubscriptionProductStatus {
	public static final int MCSubscriptionProductStatusPurchased = 0;
	public static final int MCSubscriptionProductStatusNotAvailable = 1;
	public static final int MCSubscriptionProductStatusPurchasing = 2;
}
