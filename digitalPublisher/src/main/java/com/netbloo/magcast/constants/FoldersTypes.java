package com.netbloo.magcast.constants;

public class FoldersTypes {
	public static final int ALL = 0;
	public static final int SPECIAL = 1;
	public static final int SUBSCRIBERS_ONLY = 2;
	public static final int DOWNLOADS = 3;
}
