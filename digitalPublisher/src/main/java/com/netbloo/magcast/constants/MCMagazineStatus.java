package com.netbloo.magcast.constants;

public class MCMagazineStatus {
	public final static int MCMagazineStatusPurchased = 1;
	public final static int MCMagazineStatusNotAvailable = 0;
	public final static int MCMagazineStatusPurchasing = 2;
}
