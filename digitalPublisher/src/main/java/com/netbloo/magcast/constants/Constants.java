package com.netbloo.magcast.constants;

					public class Constants {
						public static int DEVELOPMENT = 0;
						public static int MAGAZINE_ID = 1030;
						public static int ACCOUNT_ID = 2304;

                        public static String SCRIPTS_VERSION = "7.5";
                        public static String SERVER_API_URL = "http://www.magcastmags.com/api/scripts/index.php?aid=2304&mid=1030&sv=7.5";
						public static String PUSH_NOTIFICATION_SERVER_URL = "http://push.magcastmags.com/updateDevice.php";
	
						// Production
						public static String SERVER_PRODUCTS_URL = "http://www.magcastmags.com/accounts/2304/23041030/Live/scripts7.5/index.php?a=products";
						public static String SERVER_SUBSCRIPTION_URL = "http://www.magcastmags.com/accounts/2304/23041030/Live/scripts7.5/index.php?a=subscription";
						public static String SERVER_USERS_URL = "http://www.magcastmags.com/accounts/2304/23041030/Live/scripts7.5/index.php?a=users";
	
						public static String NEW_SERVER_PRODUCTS_URL = "http://www.magcastmags.com/api/scripts/index.php?aid=2304&mid=1030&sv=7.5&a=products";
						public static String NEW_SERVER_URL = "http://www.magcastmags.com/api/scripts/index.php?aid=2304&mid=1030&sv=7.5";
						public static String MAGSFAST_EVENTS_URL = "http://api.magsfast.com/API/MC/UserActivityLog.php?";
						public static String EVENT_USER_CODE = "mcaUserCode";
	
						public static String ERROR_LOG_URL = "http://www.magcastapp.com/AppErrorLog/errors";
	
						public static int DEFAULT_PAGE_WIDTH = 768;
						public static int DEFAULT_PAGE_HEIGHT = 1024;
	
						public static String GCM_SENDER_ID = "30259737610";
					}