package com.netbloo.magcast.constants;

public class MCProductStatus {
	public static final int MCProductStatusNotAvailable = 0;
	public static final int MCProductStatusDownloading = 1;
	public static final int MCProductStatusAvailable = 2;
	public static final int MCProductStatusPurchased = 3;
	public static final int MCProductStatusPurchasing = 4;
}
