package com.netbloo.magcast.constants;

public class MCProductDisplayType {
	public static final int FULL = 0;
	public static final int TEXT = 1;
	public static final int ALL = 2;
}
