package com.netbloo.magcast.activities;

import android.app.Activity;

import com.netbloo.magcast.helpers.MCServicesHelper;

/**
 * Base activity
 *
 * Created by izmaylav on 5/5/16.
 */
public class BaseActivity extends Activity {
    @Override
    protected void onResume() {
        super.onResume();

        /*MCServicesHelper servicesHelper = MCServicesHelper.getInstance();
        servicesHelper.updateContextForServices(this);*/
    }
}
