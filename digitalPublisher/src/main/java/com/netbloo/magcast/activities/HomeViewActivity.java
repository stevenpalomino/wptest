package com.netbloo.magcast.activities;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.adapters.FolderSelectAdapter;
import com.netbloo.magcast.adapters.IssuesListAdapter;
import com.netbloo.magcast.constants.Constants;
import com.netbloo.magcast.constants.FoldersTypes;
import com.netbloo.magcast.constants.MCMagazineStatus;
import com.netbloo.magcast.constants.MCProductDisplayType;
import com.netbloo.magcast.customInterfaces.BillingActivity;
import com.netbloo.magcast.helpers.HttpRequestHelper;
import com.netbloo.magcast.helpers.MCAlertDialog;
import com.netbloo.magcast.helpers.MCAnalyticsHelper;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.helpers.MCServicesHelper;
import com.netbloo.magcast.helpers.PlayStoreBillingHelper;
import com.netbloo.magcast.helpers.PlayWebViewVideoHelper;
import com.netbloo.magcast.helpers.UserHelper;
import com.netbloo.magcast.models.MCMagazine;
import com.netbloo.magcast.models.MCProduct;
import com.netbloo.magcast.views.BottomBarView;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.validators.IntegrationValidator;

import static android.content.ContentValues.TAG;

/**
 * Home view activity
 */
public class HomeViewActivity extends BaseActivity implements
		OnItemSelectedListener, BillingActivity {
	// Purchases
	PlayStoreBillingHelper playStoreBillingHelper;

	// Interface
	private ProgressBar activityIndicator;
	private BottomBarView bottomBarView;
	private GridView issuesList;
	private ImageView offlineIndicator;
	private Button signOutButton;
	private Button subscribeButton;
	private View topBarView;
	private Branch branch;
	private boolean isSuccessful;

	
	// Grid view adapter
	private IssuesListAdapter issuesListAdapter;

	// Web view video player helper
	private PlayWebViewVideoHelper playVideoHelper;

	public PlayWebViewVideoHelper getPlayVideoHelper() {
		return playVideoHelper;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.home_view_activity);

		// Get views pointers
		this.activityIndicator = (ProgressBar) findViewById(R.id.activityIndicator);
		this.bottomBarView = (BottomBarView) findViewById(R.id.bottomBar);
		Spinner folderSelect = (Spinner) findViewById(R.id.folderSelect);
		this.issuesList = (GridView) findViewById(R.id.issuesList);
		this.offlineIndicator = (ImageView) findViewById(R.id.offlineIndicator);
		this.signOutButton = (Button) findViewById(R.id.signOutButton);
		this.subscribeButton = (Button) findViewById(R.id.subscribeButton);
		this.topBarView = findViewById(R.id.topBar);

		this.bottomBarView.setParentActivity(this);
		this.bottomBarView.setVisibility(View.INVISIBLE);
		this.offlineIndicator.setVisibility(View.INVISIBLE);
		this.topBarView.setVisibility(View.INVISIBLE);

		// Start content loading and start listening to changes
		final MCPublisher publisher = MCPublisher.sharedPublisher(this);

		this.issuesListAdapter = new IssuesListAdapter(this);
		this.issuesList.setAdapter(this.issuesListAdapter);

		FolderSelectAdapter folderSelectAdapter = new FolderSelectAdapter(this);
		folderSelect.setAdapter(folderSelectAdapter);
		folderSelect.setOnItemSelectedListener(this);

		LocalBroadcastManager.getInstance(this).registerReceiver(
				this.publisherStartedToUpdateReceiver,
				new IntentFilter(MCPublisher.PUBLISHER_STARTED_TO_UPDATE));
		publisher.setBroadCastManager(LocalBroadcastManager.getInstance(this));

		LocalBroadcastManager.getInstance(this).registerReceiver(
				this.publisherUpdatedSuccessFullyReceiver,
				new IntentFilter(MCPublisher.PUBLISHER_DID_UPDATE));
		publisher.setBroadCastManager(LocalBroadcastManager.getInstance(this));

		LocalBroadcastManager.getInstance(this).registerReceiver(
				this.publisherFailedToUpdateReceiver,
				new IntentFilter(MCPublisher.PUBLISHER_FAILED_TO_UPDATE));
		publisher.setBroadCastManager(LocalBroadcastManager.getInstance(this));

		this.playStoreBillingHelper = new PlayStoreBillingHelper();
		if (!publisher.isReady()) {
			//publisher.requestProductsFromServer(this.playStoreBillingHelper);
		} else {
			displayLoadedContent();
		}

		this.playVideoHelper = new PlayWebViewVideoHelper();

		this.signOutButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				UserHelper userHelper = new UserHelper(HomeViewActivity.this);
				userHelper.signOut(HomeViewActivity.this);
			}
		});

		this.subscribeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (publisher.isOnline()) {
					Intent webViewIntent = new Intent(HomeViewActivity.this,
							SubscriptionActivity.class);
					startActivity(webViewIntent);
				} else {
					displayReloadAlert();
				}
			}
		});

		LocalBroadcastManager.getInstance(this).registerReceiver(
				new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						publisher.requestProductsFromServer(playStoreBillingHelper);
					}
				}, new IntentFilter(UserHelper.SIGNED_IN));

		LocalBroadcastManager.getInstance(this).registerReceiver(
				new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						displayLoadedContent();

						publisher.requestProductsFromServer(playStoreBillingHelper);
					}
				}, new IntentFilter(UserHelper.SIGNED_OUT));
		
		// TODO: register device
	}

	@Override
	protected void onStart() {
		super.onStart();
		//MCAlertDialog.showErrorWithText("on start - pre", this);

		Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
			@Override
			public void onInitFinished(JSONObject referringParams, BranchError error) {
				final MCPublisher publisher = MCPublisher.sharedPublisher(HomeViewActivity.this);
				isSuccessful = true;
				
				if (error == null) {
					Log.i("BRANCH SDK", referringParams.toString());

					try {
						//MCAlertDialog.showErrorWithText(referringParams.toString(), HomeViewActivity.this);
						Log.i(TAG, "ReferringParams!!! - " +referringParams.toString(2));
					} catch (JSONException e) {
						e.printStackTrace();
					}

					if (referringParams.has("validationCode")){
						//includes validationCode in response - OneTap
                        try {
                            publisher.requestProductsFromServerWithVerificationCode(playStoreBillingHelper, referringParams.getString("validationCode"));
							//MCAlertDialog.showAlert(null,"Subscription successfully unlocked", HomeViewActivity.this);
						} catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else if(referringParams.has("customer-email")){
						try {
							// IA
							String userCode = referringParams.getString("~referring_link");
							userCode = userCode.substring(userCode.lastIndexOf("/")+1);
							String userId = referringParams.getString("~id");
							String fullCode = userCode+"__"+userId;
							//MCAlertDialog.showErrorWithText("pre-user code", HomeViewActivity.this);

							SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(HomeViewActivity.this);
							if (!preferences.contains(Constants.EVENT_USER_CODE)){
								SharedPreferences.Editor editor = preferences.edit();
								editor.putString(Constants.EVENT_USER_CODE, fullCode);
								editor.apply();
                                HttpRequestHelper.logEventWithEventCode("android_app_installed", HomeViewActivity.this);
								//MCAlertDialog.showErrorWithText("post-user code", HomeViewActivity.this);

							}
							publisher.requestProductsFromServer(playStoreBillingHelper);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
                    else{
						publisher.requestProductsFromServer(playStoreBillingHelper);
					}
				} else {
					publisher.requestProductsFromServer(playStoreBillingHelper);

					Log.i("BRANCH SDK", error.getMessage());
				}
			}
		}, this.getIntent().getData(), this);
		
		//get products after a while if branch is not initted
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {

				if (!isSuccessful){
					Log.i(TAG, "run: NOT SUCCESSFUL, RUNNING OLD PUBLISHER");
					final MCPublisher publisher = MCPublisher.sharedPublisher(HomeViewActivity.this);
					publisher.requestProductsFromServer(playStoreBillingHelper);
				}
			}
		}, 3000);
	}

	@Override
	public void onNewIntent(Intent intent) {
		this.setIntent(intent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("activity", "On activity request Home activity " + requestCode
				+ " " + resultCode);

		if (requestCode == 1001) {
			Log.d("purchase", "Received purchase result");

			this.playStoreBillingHelper.parseResponseFromPlayStore(resultCode,
					data);
		} else if ((requestCode == BasePreviewActivity.RESULT_CODE)
				&& (resultCode == RESULT_OK) && (data.hasExtra("issueNo"))
				&& (data.hasExtra("displayType"))) {
			MCPublisher publisher = MCPublisher.sharedPublisher(this);
			MCProduct product = publisher.issueWithIssueNo(data.getExtras()
					.getString("issueNo"));
			if (product != null) {
				int displayType = data.getExtras().getInt("displayType");
				if (displayType == MCProductDisplayType.FULL) {
					product.viewFullVersion(this);
				} else {
					product.viewTextVersion(this);
				}
			}
		}
	}

	public void onPause() {
		super.onPause();

		this.playVideoHelper.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		Log.d("activity", "Home activity destroy");
		this.issuesList.setAdapter(null);
		this.issuesListAdapter = null;
		this.playStoreBillingHelper.stopService();
	}

	public void onResume() {
		super.onResume();

		// Check magazine status
		MCPublisher publisher = MCPublisher
				.sharedPublisher(this);
		MCMagazine magazine = publisher.getMagazine(); 
		if ((magazine != null) && (magazine.getSubscriptionStatus() == MCMagazineStatus.MCMagazineStatusPurchased)) {
			this.subscribeButton.setVisibility(View.INVISIBLE);
		}

		Log.d("activity", "Home activity resume");

		this.playStoreBillingHelper.setContext(this);
		if (this.playVideoHelper != null) {
			this.playVideoHelper.onResume();
		}
		
		MCAnalyticsHelper.logEvent("Home Page View", true, this);
		
		if (!publisher.isOnline() && publisher.isReady()) {
			publisher.requestProductsFromServer(this.getPlayStoreBillingHelper());
		}
	}

	public void onStop() {
		super.onStop();

		this.playVideoHelper.onStop();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return this.playVideoHelper.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
	}

	// Listen to publisher notifications

	private BroadcastReceiver publisherStartedToUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			HomeViewActivity.this.activityIndicator.setVisibility(View.VISIBLE);
		}
	};

	private BroadcastReceiver publisherUpdatedSuccessFullyReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			displayLoadedContent();

			MCServicesHelper serviceHelper = MCServicesHelper.getInstance();
			serviceHelper.startServicesAtMainActivityOnReady(HomeViewActivity.this);
		}
	};

	private void displayLoadedContent() {
		this.activityIndicator.setVisibility(View.INVISIBLE);
		this.bottomBarView.setVisibility(View.VISIBLE);
		this.topBarView.setVisibility(View.VISIBLE);

		// Refresh issues list
		if (this.issuesListAdapter != null) {
			this.issuesListAdapter.notifyDataSetChanged();
			this.issuesList.invalidateViews();
		}

		MCPublisher publisher = MCPublisher
				.sharedPublisher(this);
		MCMagazine magazine = publisher.getMagazine();
		if ((magazine != null) && (magazine.getSubscriptionStatus() == MCMagazineStatus.MCMagazineStatusPurchased)) {
			this.subscribeButton.setVisibility(View.INVISIBLE);
		} else {
			this.subscribeButton.setVisibility(View.VISIBLE);
		}
		
		if (publisher.isOnline()) {
			this.offlineIndicator.setVisibility(View.INVISIBLE);
		} else {
			this.offlineIndicator.setVisibility(View.VISIBLE);
		}

		UserHelper userHelper = new UserHelper(this);
		if (userHelper.isAuthenticated(this)) {
			this.signOutButton.setVisibility(View.VISIBLE);
		} else {
			this.signOutButton.setVisibility(View.INVISIBLE);
		}
	}

	private BroadcastReceiver publisherFailedToUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			HomeViewActivity.this.activityIndicator
					.setVisibility(View.INVISIBLE);
			HomeViewActivity.this.offlineIndicator.setVisibility(View.VISIBLE);

			MCPublisher publisher = MCPublisher
					.sharedPublisher(HomeViewActivity.this);
			if (publisher.numberOfIssues() != 0) {
				HomeViewActivity.this.bottomBarView.setVisibility(View.VISIBLE);
				HomeViewActivity.this.topBarView.setVisibility(View.VISIBLE);

				// Refresh issues list
				if (HomeViewActivity.this.issuesListAdapter != null) {
					HomeViewActivity.this.issuesListAdapter.notifyDataSetChanged();
					HomeViewActivity.this.issuesList.invalidateViews();
				}
				
				// Check magazine status
				MCMagazine magazine = publisher.getMagazine(); 
				if ((magazine != null) && (magazine.getSubscriptionStatus() == MCMagazineStatus.MCMagazineStatusPurchased)) {
					subscribeButton.setVisibility(View.INVISIBLE);
				}
			} else {
				displayReloadAlert();
			}
		}
	};

	// Menu select

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
			long arg3) {
		MCPublisher publisher = MCPublisher.sharedPublisher(this);
		int folderType = pos;
		if ((folderType >= FoldersTypes.SPECIAL) && (!publisher.hasSpecialIssues())) {
			++folderType;
		}
		if ((folderType >= FoldersTypes.SUBSCRIBERS_ONLY) && (!publisher.hasSubscribersOnlyIssues())) {
			++folderType;
		}
		
		this.issuesListAdapter.setFolderType(folderType);
		this.issuesList.invalidateViews();
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	@Override
	public PlayStoreBillingHelper getPlayStoreBillingHelper() {
		return this.playStoreBillingHelper;
	}

	private void displayReloadAlert() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle(this.getString(R.string.error));
		alertDialogBuilder.setMessage(this.getString(R.string.cant_get_issue_from_server));
		alertDialogBuilder.setPositiveButton(
				this.getString(R.string.reload),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int which) {
						MCPublisher publisher = MCPublisher
								.sharedPublisher(HomeViewActivity.this);
						publisher
								.requestProductsFromServer(playStoreBillingHelper);
					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
}
