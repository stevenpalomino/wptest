package com.netbloo.magcast.activities;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.customInterfaces.BillingActivity;
import com.netbloo.magcast.helpers.PlayStoreBillingHelper;
import com.netbloo.magcast.helpers.PlayWebViewVideoHelper;
import com.netbloo.magcast.helpers.UrlActionHelper;
import com.netbloo.magcast.views.BottomBarView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class WebViewActivity extends BaseActivity implements BillingActivity {
	// Preferences
	private boolean displayBottomBar;
	private boolean displayCloseButton;
	private boolean displayTopBar;

	// Subviews
	private ProgressBar activityView;
	private String htmlContent;
	private String url;
	private WebView webView;

	// Purchases
	PlayStoreBillingHelper playStoreBillingHelper;

	// Web view video player helper
	private PlayWebViewVideoHelper playVideoHelper;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.web_view_activity);

		Intent i = getIntent();
		Bundle extras = i.getExtras();
		this.displayBottomBar = extras.getBoolean("displayBottomBar", true);
		this.displayCloseButton = extras.getBoolean("displayCloseButton", false);
		this.displayTopBar = extras.getBoolean("displayTopBar");
		this.htmlContent = extras.getString("htmlContent");
		this.url = extras.getString("url");

		if (this.htmlContent != null) {
			this.htmlContent = this.htmlContent.replace("<style>", "<style>img {max-width: 100%;}");
		}

		this.playStoreBillingHelper = new PlayStoreBillingHelper();

		this.playVideoHelper = new PlayWebViewVideoHelper();
		this.loadViewComponents();
	}

	public void onPause() {
		super.onPause();

		this.playVideoHelper.onPause();
	}

	public void onResume() {
		super.onResume();

		this.playVideoHelper.onResume();
		this.playStoreBillingHelper.setContext(this);
	}

	public void onStop() {
		super.onStop();

		this.playVideoHelper.onStop();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return this.playVideoHelper.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("activity", "On activity request Subscription activity " + requestCode + " " + resultCode);
		if (requestCode == 1001) {
			Log.d("purchase", "Received purchase result");

			this.playStoreBillingHelper.parseResponseFromPlayStore(resultCode, data);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		this.playStoreBillingHelper.stopService();
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void loadViewComponents() {
		this.activityView = (ProgressBar) findViewById(R.id.activityView);
		BottomBarView bottomBarView = (BottomBarView) findViewById(R.id.bottomBar);
		Button closeButton = (Button) findViewById(R.id.closeButton);
		RelativeLayout topBar = (RelativeLayout) findViewById(R.id.topBar);
		this.webView = (WebView) findViewById(R.id.webView);

		closeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		// Configure subviews
		bottomBarView.setParentActivity(this);

		if (this.displayTopBar) {
			topBar.setVisibility(View.VISIBLE);
		} else {
			topBar.setVisibility(View.GONE);
		}
		
		if (this.displayBottomBar) {
			bottomBarView.setVisibility(View.VISIBLE);
		} else {
			bottomBarView.setVisibility(View.GONE);
		}

		if (this.displayCloseButton) {
			closeButton.setVisibility(View.VISIBLE);
		} else {
			closeButton.setVisibility(View.GONE);
		}
		
		// Setup buttons actions
		Button doneButton = (Button) findViewById(R.id.doneButton);
		doneButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		// Configure web view
		
		 WebSettings webSettings = this.webView.getSettings();
		 webSettings.setJavaScriptEnabled(true);
		 webSettings.setBuiltInZoomControls(true);
		 webSettings.setAllowFileAccess(true);
		 webSettings.setAllowContentAccess(true);
		 //webSettings.setUseWideViewPort(true);
		 //webSettings.setLoadWithOverviewMode(true);
		 webSettings.setAllowFileAccess(true);
		 webSettings.setDomStorageEnabled(true);

		 
		// webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		  
		 /*if (this.scaleToDefaultPageWidth) {
			 Display display = getWindowManager().getDefaultDisplay(); 
			 Point size = new Point(); 
			 display.getSize(size); 
			 int initialScale = size.x * 100 /  Constants.DEFAULT_PAGE_WIDTH;
			 webView.setInitialScale(initialScale);
		 }*/

		 final String currentUrl = this.url;
		 webView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (currentUrl == null) {
					UrlActionHelper actionHelper = new UrlActionHelper();
					return actionHelper.performAction(url, WebViewActivity.this, WebViewActivity.this.webView) || super.shouldOverrideUrlLoading(view, url);
				} else {
					return false;
				}
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				
				activityView.setVisibility(View.INVISIBLE);
			}
		 });
		 
		 this.playVideoHelper.setWebViewForVideoPlay(this, this.webView, null);
		 
		 if (this.url != null) {
			 this.webView.loadUrl(this.url);
		 } else {
			 this.webView.loadDataWithBaseURL("file:///" ,this.htmlContent, "text/html; charset=utf-8", "utf-8", null);
		 }
	}

	@Override
	public PlayStoreBillingHelper getPlayStoreBillingHelper() {
		return this.playStoreBillingHelper;
	}
}
