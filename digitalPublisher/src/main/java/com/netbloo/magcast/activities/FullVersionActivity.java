package com.netbloo.magcast.activities;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.adapters.FullVersionActivityPagerAdapter;
import com.netbloo.magcast.constants.MCProductDisplayType;
import com.netbloo.magcast.entities.Thumbnail;
import com.netbloo.magcast.helpers.MCAlertDialog;
import com.netbloo.magcast.helpers.MCXmlParser;
import com.netbloo.magcast.views.FullVersionThumbnailView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

public class FullVersionActivity extends BasePreviewActivity {
	/** maintains the pager adapter */
	private FullVersionActivityPagerAdapter pagerAdapter;

	private LinearLayout thumbnailsView;
	protected HorizontalScrollView thumbnailsContainerViewScrollView;
	private ArrayList<FullVersionThumbnailView> thumbnailsViewsList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.full_version_view_activity);
		
		if (this.product != null) {
			this.loadViewComponents();
		}
	}
	
	protected void finalize() {
		Log.d("fin", "destroy full preview");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	public void goToPageWithFilename(String pageFilename) {
		int pageIndex = this.pagerAdapter.getPageIndexWithFilename(pageFilename);
		
		if (pageIndex != -1) {
			goToPageWithIndex(pageIndex);
		} else {
			MCAlertDialog.showErrorWithText(this.getString(R.string.page_not_available), this);
		}
	}
	
	private void goToPageWithIndex(int pageIndex) {
		this.contentView.setCurrentItem(pageIndex);
	}

	protected void loadViewComponents() {
		super.loadViewComponents();
		
		this.thumbnailsView = (LinearLayout) findViewById(R.id.thumbnailsView);
		this.thumbnailsContainerViewScrollView = (HorizontalScrollView) findViewById(R.id.thumbnailsContainerViewScrollView);

		prepareTopBarButtons();
		prepareThumbnails();
		prepareContentView();
	}

	private void prepareTopBarButtons() {
		Button doneButton = (Button) findViewById(R.id.doneButton);
		doneButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		Button printButton = (Button) findViewById(R.id.printButton);
		printButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Add code to print screen content
			}
		});
		
		Button shareButton = (Button) findViewById(R.id.shareButton);
		shareButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Action on click on share button
				
			}
		});
		
		Button textVersionButton = (Button) findViewById(R.id.textVersionButton);
		if (this.product.getDisplayType() == MCProductDisplayType.FULL) {
			textVersionButton.setVisibility(View.INVISIBLE);
		} else {
			textVersionButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					enableSwitchTpDisplayType(MCProductDisplayType.TEXT);
					finish();
				}
			});
		}
	}

	private void prepareContentView() {
		this.pagerAdapter = new FullVersionActivityPagerAdapter(
				this.getSupportFragmentManager());
		this.pagerAdapter.setProduct(this.product);
		
		this.contentView = (ViewPager) findViewById(R.id.contentView);
		this.contentView.setOffscreenPageLimit(1);
		this.contentView.setAdapter(this.pagerAdapter);
		this.contentView
				.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						setCurrentPageIndex(position);
						
						displayThumbnailSelected(position);
					}

					@Override
					public void onPageScrolled(int position, float arg1,
							int arg2) {

					}

					@Override
					public void onPageScrollStateChanged(int position) {

					}
				});

		// Move to last viewed page
		SharedPreferences preferences = FullVersionActivity.this
				.getSharedPreferences("MyPreferences", MODE_PRIVATE);
		int currentPageIndex = preferences.getInt(getCurrentPageIndexFieldName(),
				0);
		goToPageWithIndex(currentPageIndex);
		
		this.pagerAdapter.notifyDataSetChanged();
		this.contentView.invalidate();
	}
	
	private void displayThumbnailSelected(int position) {
		if (position > 0) {
			int containerWidth = thumbnailsContainerView.getWidth();
	
			final int thumbnailViewWidth = (int)FullVersionActivity.convertDpToPixel((float)110.0, this);
			int visibleThumbnailsViewsCount = containerWidth
					/ thumbnailViewWidth;
			int elementsOnOneSide = (visibleThumbnailsViewsCount - 1) / 2;
			int startElementPosition = position - elementsOnOneSide;

			this.thumbnailsContainerViewScrollView.setScrollX(startElementPosition * thumbnailViewWidth);
		}
	}
	
	/**
	 * This method converts dp unit to equivalent pixels, depending on device density. 
	 * 
	 * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
	 * @param context Context to get resources and device specific display metrics
	 * @return A float value to represent px equivalent to dp depending on device density
	 */
	public static float convertDpToPixel(float dp, Context context){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float px = dp * (metrics.densityDpi / 160f);
	    return px;
	}

	private void prepareThumbnails() {
		this.thumbnailsViewsList = new ArrayList<FullVersionThumbnailView>();
		
		try {
			// Find enclosing directory
			File issueDirectory = new File(this.product.getDestinationPath());
			File[] files = issueDirectory.listFiles();
		    for (File possibleDirectory : files) {
		    	if (possibleDirectory.isDirectory()) {
		    		String thumbnailsXmlPath = possibleDirectory.getPath() + "/thumbnail.xml";
					File thumbnailsXmlFile = new File(thumbnailsXmlPath);
					FileInputStream fis = new FileInputStream(thumbnailsXmlFile);
					
					XmlPullParser xmlParser = Xml.newPullParser();
					xmlParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
					xmlParser.setInput(fis, null);
					xmlParser.nextTag();
					
					String retrievedString = null;
					Thumbnail thumbnailEntity = null;
					String thumbnailImagePath = null;
					FullVersionThumbnailView thumbnailView = null;
					int thumbnailIndex = 1;
					xmlParser.require(XmlPullParser.START_TAG, retrievedString, "thumbnails");
					MCXmlParser mcXmlParcer = new MCXmlParser();
			        while (xmlParser.next() != XmlPullParser.END_TAG) {
			            if (xmlParser.getEventType() != XmlPullParser.START_TAG) {
			                continue;
			            }
			            String name = xmlParser.getName();
			            // Starts by looking for the entry tag
			            if (name.equals("thumbnail")) {
			                thumbnailEntity = new Thumbnail(xmlParser);
			                thumbnailImagePath = possibleDirectory.getPath() + "/images/" + thumbnailEntity.imagePath;
			                thumbnailView = new FullVersionThumbnailView(thumbnailImagePath, thumbnailIndex, this);
			                
			                final int currentThumbnailIndex = thumbnailIndex;
			                thumbnailView.setOnImageViewClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									goToPageWithIndex(currentThumbnailIndex - 1);
									hideBars();
								}
							});
			                
			                this.thumbnailsView.addView(thumbnailView);
			                
			                this.thumbnailsViewsList.add(thumbnailView);
			                
			                ++thumbnailIndex;
			            } else {
			                mcXmlParcer.skip(xmlParser);
			            }
			        }
					
					fis.close();
					
					this.hasThumbnailsView = true;
		    	}
		    }
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
	}
	
	private Handler scrollHandler = new Handler();
	@Override
	public void onLongClick() {
		super.onLongClick();
		
		// To be sure that scrollView is already drawn
		this.scrollHandler.postDelayed(new Runnable(){
		    public void run() {
		    	displayThumbnailSelected(getCurrentPageIndex());
		     }
		 }, 1000);
	}
}
