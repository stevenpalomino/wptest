package com.netbloo.magcast.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.helpers.MCAlertDialog;
import com.netbloo.magcast.helpers.UserHelper;

/**
 * Recover password activity
 *
 * Created by izmaylav on 5/6/16.
 */
public class RecoverPasswordActivity extends BaseActivity {
    private EditText emailEditText;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recover_password_view_activity);

        emailEditText = (EditText) findViewById(R.id.emailEditText);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        Button closeButton = (Button) findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button signInButton = (Button) findViewById(R.id.signInButton);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        progressBar.setVisibility(View.INVISIBLE);
                        MCAlertDialog.showAlert(context.getString(R.string.information), context.getString(R.string.check_your_email_for_new_password), RecoverPasswordActivity.this);
                    }
                }, new IntentFilter(UserHelper.RESETED_PASSWORD));

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        progressBar.setVisibility(View.INVISIBLE);

                        String message = intent.getStringExtra(UserHelper.NOTIFICATION_MESSAGE);
                        MCAlertDialog.showErrorWithText(message, RecoverPasswordActivity.this);
                    }
                }, new IntentFilter(UserHelper.FAILED_TO_RESET_PASSWORD));
    }

    private void signIn() {
        String email = emailEditText.getText().toString();

        if (email.isEmpty()) {
            MCAlertDialog.showErrorWithText("Please, fill all required fields", this);
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            MCAlertDialog.showErrorWithText("Please, provide valid email", this);
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        UserHelper userHelper = new UserHelper(this);
        userHelper.resetPassword(email, this);
    }
}
