package com.netbloo.magcast.activities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.adapters.TextVersionActivityPagerAdapter;
import com.netbloo.magcast.constants.MCProductDisplayType;
import com.netbloo.magcast.entities.Article;
import com.netbloo.magcast.helpers.MCAlertDialog;
import com.netbloo.magcast.helpers.MCXmlParser;
import com.netbloo.magcast.views.TextVersionThumbnailView;

public class TextVersionActivity extends BasePreviewActivity {
	// Notification sending
	private LocalBroadcastManager broadCastManager;
	public static final String FONT_SIZE_CHANGED = "TextVersionFontSizeChanged";
	
	/** maintains the pager adapter */
	private ArrayList<Article> articlesList;
	private TextVersionActivityPagerAdapter pagerAdapter;

	private LinearLayout thumbnailsView;
	protected ScrollView thumbnailsContainerViewScrollView;
	private ArrayList<TextVersionThumbnailView> thumbnailsViewsList;

	// Font size change
	private final int MIN_FONT_SIZE = 100;
	private final int MAX_FONT_SIZE = 200;
	private final int FONT_SIZE_CHANGE_STEP = 20;
	private Button decreaseFontButton;
	private Button increaseFontButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.text_version_view_activity);
		
		this.broadCastManager = LocalBroadcastManager.getInstance(this);

		if (this.product != null) {
			this.loadViewComponents();
		}
	}

	public void goToPageWithFilename(String pageFilename) {
		int pageIndex = this.pagerAdapter
				.getPageIndexWithFilename(pageFilename);

		if (pageIndex != -1) {
			goToPageWithIndex(pageIndex);
		} else {
			MCAlertDialog.showErrorWithText(
					this.getString(R.string.page_not_available), this);
		}
	}

	private void goToPageWithIndex(int pageIndex) {
		this.contentView.setCurrentItem(pageIndex);
	}

	protected void loadViewComponents() {
		super.loadViewComponents();

		this.thumbnailsView = (LinearLayout) findViewById(R.id.thumbnailsView);
		this.thumbnailsContainerViewScrollView = (ScrollView) findViewById(R.id.thumbnailsContainerViewScrollView);

		prepareTopBarButtons();
		prepareContentView();
	}

	private void prepareTopBarButtons() {
		Button doneButton = (Button) findViewById(R.id.doneButton);
		doneButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Button printButton = (Button) findViewById(R.id.printButton);
		printButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Print content of the screen
			}
		});

		Button shareButton = (Button) findViewById(R.id.shareButton);
		shareButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Action on click on share button

			}
		});

		Button textVersionButton = (Button) findViewById(R.id.textVersionButton);
		if (this.product.getDisplayType() == MCProductDisplayType.TEXT) {
			textVersionButton.setVisibility(View.GONE);
		} else {
			textVersionButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					enableSwitchTpDisplayType(MCProductDisplayType.FULL);
					finish();
				}
			});
		}

		this.increaseFontButton = (Button) findViewById(R.id.increaseFontButton);
		this.increaseFontButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int currentFontSize = getCurrentFontSize();
			    if (currentFontSize < MAX_FONT_SIZE) {
			        currentFontSize += FONT_SIZE_CHANGE_STEP;
			        
			        setCurrentFontSize(currentFontSize);
			        updateFontsControls();
			    }
			}
		});

		this.decreaseFontButton = (Button) findViewById(R.id.decreaseFontButton);
		this.decreaseFontButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int currentFontSize = getCurrentFontSize();
				if (currentFontSize > MIN_FONT_SIZE) {
			        currentFontSize -= FONT_SIZE_CHANGE_STEP;
			        
			        setCurrentFontSize(currentFontSize);
			        updateFontsControls();
			    }
			}
		});
	}
	
	private void updateFontsControls() {
		int currentFontSize = getCurrentFontSize();
		
		this.increaseFontButton.setEnabled(currentFontSize != MAX_FONT_SIZE);
		this.decreaseFontButton.setEnabled(currentFontSize != MIN_FONT_SIZE);
	}

	private void prepareContentView() {
		preparePagerAdapter();

		this.contentView = (ViewPager) findViewById(R.id.contentView);
		this.contentView.setOffscreenPageLimit(3);
		this.contentView.setAdapter(this.pagerAdapter);
		this.contentView
				.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						setCurrentPageIndex(position);

						displayThumbnailSelected(position);
					}

					@Override
					public void onPageScrolled(int position, float arg1,
							int arg2) {

					}

					@Override
					public void onPageScrollStateChanged(int position) {

					}
				});

		// Move to last viewed page
		SharedPreferences preferences = getSharedPreferences("MyPreferences",
				MODE_PRIVATE);
		int currentPageIndex = preferences.getInt(
				getCurrentPageIndexFieldName(), 0);
		goToPageWithIndex(currentPageIndex);
		displayThumbnailSelected(currentPageIndex);
	}

	private void displayThumbnailSelected(int position) {
		for (TextVersionThumbnailView thumbnailView : this.thumbnailsViewsList) {
			thumbnailView.deselect();
		}

		TextVersionThumbnailView thumbnailView = this.thumbnailsViewsList
				.get(position);
		thumbnailView.select();

		// Scroll thumbnails view to appropriate thumbnail
		if (position > 0) {
			int previousPosition = position - 1;
			TextVersionThumbnailView previousThumbnailView = this.thumbnailsViewsList
					.get(previousPosition);
			int previousItemTop = previousThumbnailView.getTop();
			this.thumbnailsContainerViewScrollView.setScrollY(previousItemTop);
		}
	}

	private void preparePagerAdapter() {
		getArticles();
		this.pagerAdapter = new TextVersionActivityPagerAdapter(
				this.getSupportFragmentManager());
		this.pagerAdapter.setArticlesList(this.articlesList);
	}

	private void getArticles() {
		this.articlesList = new ArrayList<Article>();
		this.thumbnailsViewsList = new ArrayList<TextVersionThumbnailView>();

		try {
			String articlesXmlPath = getContentDirectoryPath()
					+ "/articles.xml";
			File articlesXmlFile = new File(articlesXmlPath);
			FileInputStream fis = new FileInputStream(articlesXmlFile);

			XmlPullParser xmlParser = Xml.newPullParser();
			xmlParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES,
					false);
			xmlParser.setInput(fis, null);
			xmlParser.nextTag();

			String retrievedString = null;
			Article articleEntity = null;
			int articleIndex = 1;
			String articlePath = null;
			TextVersionThumbnailView thumbnailView = null;
			xmlParser.require(XmlPullParser.START_TAG, retrievedString,
					"articles");
			MCXmlParser mcXmlParcer = new MCXmlParser();
			while (xmlParser.next() != XmlPullParser.END_TAG) {
				if (xmlParser.getEventType() != XmlPullParser.START_TAG) {
					continue;
				}
				String name = xmlParser.getName();
				// Starts by looking for the entry tag
				if (name.equals("article")) {
					articlePath = getContentDirectoryPath()
							+ String.format("/page%03d.html", articleIndex);
					articleEntity = new Article(xmlParser, articlePath);
					this.articlesList.add(articleEntity);

					thumbnailView = new TextVersionThumbnailView(articleEntity,
							this);
					this.thumbnailsView.addView(thumbnailView);

					final int currentArticleIndex = articleIndex - 1;
					thumbnailView
							.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									goToPageWithIndex(currentArticleIndex);
									hideBars();
								}
							});

					this.thumbnailsViewsList.add(thumbnailView);

					++articleIndex;
				} else {
					mcXmlParcer.skip(xmlParser);
				}
			}

			fis.close();

			this.hasThumbnailsView = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
	}

	private String getContentDirectoryPath() {
		File issueDirectory = new File(this.product.getDestinationPath());
		File[] files = issueDirectory.listFiles();
		String textVersionPath = null;
		// Find enclosing directory
		for (File possibleDirectory : files) {
			if (possibleDirectory.isDirectory()) {
				textVersionPath = possibleDirectory.getAbsolutePath()
						+ "/textVersion";
			}
		}

		return textVersionPath;
	}

	protected String getCurrentPageIndexFieldName() {
		return super.getCurrentPageIndexFieldName() + "textVersion";
	}

	private Handler scrollHandler = new Handler();

	@Override
	public void onLongClick() {
		super.onLongClick();

		// To be sure that scrollView is already drawn
		this.scrollHandler.postDelayed(new Runnable() {
			public void run() {
				displayThumbnailSelected(getCurrentPageIndex());
			}
		}, 1000);
	}

	public int getCurrentFontSize() {
		// Move to last viewed page
		SharedPreferences preferences = getSharedPreferences("MyPreferences",
				MODE_PRIVATE);
		return preferences.getInt(getCurrentPageIndexFieldName() + "textVersionFontSize", MIN_FONT_SIZE);
	}
	
	private void setCurrentFontSize(int fontSize) {
		SharedPreferences preferences = getSharedPreferences("MyPreferences", MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(getCurrentPageIndexFieldName() + "textVersionFontSize", fontSize);
		editor.commit();
		
		 Intent intent = new Intent(TextVersionActivity.FONT_SIZE_CHANGED);                	  
         this.broadCastManager.sendBroadcast(intent);
	}
}
