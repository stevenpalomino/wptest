package com.netbloo.magcast.activities;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.adapters.ResourcesListAdapter;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.models.MCResource;
import com.netbloo.magcast.views.BottomBarView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ResourcesActivity extends BaseActivity implements OnItemClickListener {
	private BottomBarView bottomBarView;
	private ListView resourcesListView;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.resources_activity);
        
        bottomBarView = (BottomBarView) findViewById(R.id.bottomBarView1);
        bottomBarView.setParentActivity(this); 
        resourcesListView = (ListView) findViewById(R.id.resourcesList);
        resourcesListView.setAdapter(new ResourcesListAdapter(this));
        resourcesListView.setOnItemClickListener(this);
    }

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {
		MCPublisher publisher = MCPublisher.sharedPublisher(this);
		MCResource resource = publisher.resourceAtIndex(index);
		
		if (resource.getLink() != null) {
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(resource.getLink()));
			startActivity(browserIntent);			
		}
	}
}
