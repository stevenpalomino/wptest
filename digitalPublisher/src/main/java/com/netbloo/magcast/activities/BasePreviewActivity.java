package com.netbloo.magcast.activities;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.customInterfaces.BillingActivity;
import com.netbloo.magcast.helpers.HttpRequestHelper;
import com.netbloo.magcast.helpers.MCAnalyticsHelper;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.helpers.MCServicesHelper;
import com.netbloo.magcast.helpers.PlayStoreBillingHelper;
import com.netbloo.magcast.helpers.PlayWebViewVideoHelper;
import com.netbloo.magcast.models.MCProduct;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import org.json.JSONException;

abstract public class BasePreviewActivity extends FragmentActivity implements BillingActivity {
	protected MCProduct product;
	
	private int currentPageIndex;
	
	// Web view video player helper
	private PlayWebViewVideoHelper playVideoHelper;
	
	// Purchases
	PlayStoreBillingHelper playStoreBillingHelper;
	
	// Views
	protected ViewPager contentView;
	protected boolean hasThumbnailsView;
	protected View thumbnailsContainerView;
	private RelativeLayout topBar;
	
	// Switching of display types
	boolean switchDisplayType;
	int switchToDisplayType;
	public static final int RESULT_CODE = 10000;
		
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		SharedPreferences preferences = getPreferences(MODE_PRIVATE);
		
		Intent i = getIntent();
		Bundle extras = i.getExtras();
		try {
			String issueNo = extras.getString("issueNo");
			if (issueNo != null) {
				MCPublisher publisher = MCPublisher.sharedPublisher(this);
				this.product = publisher.issueWithIssueNo(issueNo);
			} else {
				finish();
			}
		} catch (NullPointerException e) {
			for (StackTraceElement ste : e.getStackTrace()) {
				Log.e("PreviewActivity exc", ste.toString());
			}
			
			String issueNo = preferences.getString("issueNo", "");
			Log.d("app", "No issue no activity is exception " + issueNo);
			if (issueNo.length() != 0) {
				MCPublisher publisher = MCPublisher.sharedPublisher(this);
				this.product = publisher.issueWithIssueNo(issueNo);
			} else {
				finish();
			}
		}
		
		if (this.product != null) {
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString("issueNo", this.product.getIssueNo());
			editor.apply();
		} else {
			finish();
		}
		
		this.playStoreBillingHelper = new PlayStoreBillingHelper();
		this.playVideoHelper = new PlayWebViewVideoHelper();
	}
	
	protected void loadViewComponents() {
		this.thumbnailsContainerView = findViewById(R.id.thumbnailsContainerView);
		this.topBar = (RelativeLayout) findViewById(R.id.topBar);
		
		this.topBar.setVisibility(View.GONE);
		this.thumbnailsContainerView.setVisibility(View.GONE);
	}
	
	@Override
	public void finish() {
		if (this.switchDisplayType) {
			Intent data = new Intent();
		    // Return some hard-coded values
		    data.putExtra("displayType", this.switchToDisplayType);
		    data.putExtra("issueNo", this.product.getIssueNo());
		    setResult(RESULT_OK, data);
		}
		
		super.finish();
	}
	
	abstract public void goToPageWithFilename(String pageFilename);
	
	protected void enableSwitchTpDisplayType(int displayType) {
		this.switchDisplayType = true;
		this.switchToDisplayType = displayType;
	}
	
	public int getCurrentPageIndex() {
		return currentPageIndex;
	}

	public void setCurrentPageIndex(int currentPageIndex) {
		this.currentPageIndex = currentPageIndex;

		if (currentPageIndex == 15){
			if (MCPublisher.sharedPublisher(this).isHasBranchUserCode(this)){
				Log.i("~~~", "setCurrentPageIndex: PAGE 15 LOG~~~ ");
				try {
					HttpRequestHelper.logEventWithEventCode("android_app_page_15_read", this);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		SharedPreferences preferences = getSharedPreferences("MyPreferences", MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(getCurrentPageIndexFieldName(), currentPageIndex);
		editor.apply();
		
		String eventDescription = "Issue " + product.getIssueNo() + " - Page " + currentPageIndex;
    	MCAnalyticsHelper.logEvent(eventDescription, false, this);
	}
	
	protected String getCurrentPageIndexFieldName() {
		String currentPageIndexFieldName = "";
		if (product != null) {
			currentPageIndexFieldName = "currentPageIndex" + product.getIssueNo();
		}
		return currentPageIndexFieldName;
	}
	
	public MCProduct getProduct() {
		return this.product;
	}
	
	@Override
	public PlayStoreBillingHelper getPlayStoreBillingHelper() {
		return this.playStoreBillingHelper;
	}

	public PlayWebViewVideoHelper getPlayVideoHelper() {
		return playVideoHelper;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("activity", "On activity request Subscription activity " + requestCode + " " + resultCode);
		if (requestCode == 1001) {
			Log.d("purchase", "REceived purchase result");

			this.playStoreBillingHelper.parseResponseFromPlayStore(resultCode, data);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		Log.d("activity", "Subscription activity destroy");
		
		this.playStoreBillingHelper.stopService();
	}
	
	public void onPause() {
		super.onPause();

		this.getPlayVideoHelper().onPause();
	}

	public void onResume() {
		super.onResume();

		MCServicesHelper servicesHelper = MCServicesHelper.getInstance();
		servicesHelper.updateContextForServices(this);

		this.playStoreBillingHelper.setContext(this);
		this.getPlayVideoHelper().onResume();
	}

	public void onStop() {
		super.onStop();

		this.getPlayVideoHelper().onStop();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return this.getPlayVideoHelper().onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		outState.putString("issueNo", this.product.getIssueNo());
	}



	final Handler handler = new Handler(); 
	Runnable mLongPressed = new Runnable() { 
	    public void run() { 
	        onLongClick();
	    }   
	};
	private float startXPoint;
	private float startYPoint;
	private final static double ignoreMoveDistance = 10;
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (event.getPointerCount() == 1) {
				handler.postDelayed(mLongPressed, 700);
				startXPoint = event.getX();
				startYPoint = event.getY();
			}
			break;
		case MotionEvent.ACTION_MOVE:
			float dx = startXPoint - event.getX();
			float dy = startYPoint - event.getY();
			double distance = Math.sqrt(dx * dx + dy * dy);
			if (distance > BasePreviewActivity.ignoreMoveDistance) {
				handler.removeCallbacks(mLongPressed);
			}
			break;
		case MotionEvent.ACTION_UP:
			handler.removeCallbacks(mLongPressed);
			break;
		}
	        
		
		return super.dispatchTouchEvent(event);
	}
	
	public void onLongClick() {
		FrameLayout videoPresentationContainer = (FrameLayout) findViewById(R.id.videoPresentationContainer);
		if (videoPresentationContainer.getVisibility() == View.VISIBLE) {
			return;
		}
		
		if (this.topBar.getVisibility() == View.VISIBLE) {
			this.hideBars();
		} else {
			this.showBars();
		}
	}
	
	protected void hideBars() {
		this.thumbnailsContainerView.setVisibility(View.GONE);
		this.topBar.setVisibility(View.GONE);
	}
	
	protected void showBars() {
		if (this.hasThumbnailsView) {
			this.thumbnailsContainerView.setVisibility(View.VISIBLE);
		}
		this.topBar.setVisibility(View.VISIBLE);
	}
}
