package com.netbloo.magcast.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.helpers.MCAlertDialog;
import com.netbloo.magcast.helpers.UserHelper;

/**
 * Activity for users sign in
 *
 * Created by izmaylav on 4/29/16.
 */
public class SignInActivity extends BaseActivity {

    private EditText emailEditText;
    private EditText passwordEditText;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_activity);

        emailEditText = (EditText) findViewById(R.id.emailEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        Button closeButton = (Button) findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button signInButton = (Button) findViewById(R.id.signInButton);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

        Button recoverPasswordButton = (Button) findViewById(R.id.recoverPasswordButton);
        recoverPasswordButton.setPaintFlags(recoverPasswordButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        recoverPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recoverPasswordActivityIntent = new Intent(SignInActivity.this,
                        RecoverPasswordActivity.class);
                startActivity(recoverPasswordActivityIntent);
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        SignInActivity.this.finish();
                    }
                }, new IntentFilter(UserHelper.SIGNED_IN));

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        progressBar.setVisibility(View.INVISIBLE);

                        String message = intent.getStringExtra(UserHelper.NOTIFICATION_MESSAGE);
                        MCAlertDialog.showErrorWithText(message, SignInActivity.this);
                    }
                }, new IntentFilter(UserHelper.FAILED_TO_SIGN_IN));
    }

    private void signIn() {
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        if (email.isEmpty()
                || password.isEmpty()) {
            MCAlertDialog.showErrorWithText("Please, fill all required fields", this);
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            MCAlertDialog.showErrorWithText("Please, provide valid email", this);
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        UserHelper userHelper = new UserHelper(this);
        userHelper.signIn(email, password, this);
    }
}
