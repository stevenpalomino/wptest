package com.netbloo.magcast.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.helpers.MCAlertDialog;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.helpers.UserHelper;

/**
 * Sign Up activity controller
 *
 * Created by izmaylav on 4/29/16.
 */
public class SignUpActivity extends BaseActivity {
    private EditText emailEditText;
    private EditText nameEditText;
    private EditText passwordEditText;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_view_activity);

        emailEditText = (EditText) findViewById(R.id.emailEditText);
        nameEditText = (EditText) findViewById(R.id.nameEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        MCPublisher publisher = MCPublisher.sharedPublisher(this);
        TextView descriptionTextView = (TextView) findViewById(R.id.descriptionTextView);
        descriptionTextView.setText(publisher.getMagazine().getPrivateSubscription().getPromoText());


        Button closeButton = (Button) findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button signUpButton = (Button) findViewById(R.id.signUpButton);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        SignUpActivity.this.finish();
                    }
                }, new IntentFilter(UserHelper.SIGNED_IN));

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        progressBar.setVisibility(View.INVISIBLE);

                        String message = intent.getStringExtra(UserHelper.NOTIFICATION_MESSAGE);
                        MCAlertDialog.showErrorWithText(message, SignUpActivity.this);
                    }
                }, new IntentFilter(UserHelper.FAILED_TO_SIGN_IN));
    }

    private void signUp() {
        String email = emailEditText.getText().toString();
        String name = nameEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        if (email.isEmpty()
                || name.isEmpty()
                || password.isEmpty()) {
            MCAlertDialog.showErrorWithText("Please, fill all required fields", this);
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            MCAlertDialog.showErrorWithText("Please, provide valid email", this);
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        UserHelper userHelper = new UserHelper(this);
        userHelper.signUp(name, email, password, this);
    }
}
