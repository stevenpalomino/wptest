package com.netbloo.magcast.activities;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.constants.Constants;
import com.netbloo.magcast.constants.MCMagazineStatus;
import com.netbloo.magcast.helpers.HttpRequestHelper;
import com.netbloo.magcast.helpers.MCAlertDialog;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.models.MCMagazine;
import com.netbloo.magcast.models.MCProduct;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class CurrentSubscribersActivity extends BaseActivity {
	private ProgressBar activityView;
	private LinearLayout bonusCodeBlock;
	private EditText bonusCodeEditText;
	private TextView descriptionTextView;
	private LinearLayout externalSiteBlock;
	private TextView orTextView;
	private EditText passwordEditText;
	private EditText usernameEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.current_subscribers_view_activity);

		this.activityView = (ProgressBar) findViewById(R.id.activityView);
		this.bonusCodeEditText = (EditText) findViewById(R.id.bonusCodeEditText);
		this.bonusCodeBlock = (LinearLayout) findViewById(R.id.bonusCodeBlock);
		this.descriptionTextView = (TextView) findViewById(R.id.descriptionTextView);
		this.externalSiteBlock = (LinearLayout) findViewById(R.id.externalSiteBlock);
		this.orTextView = (TextView) findViewById(R.id.orTextView);
		this.passwordEditText = (EditText) findViewById(R.id.passwordEditText);
		this.usernameEditText = (EditText) findViewById(R.id.usernameEditText);

		MCPublisher publisher = MCPublisher.sharedPublisher(this);
		MCMagazine magazine = publisher.getMagazine();
		if ((magazine != null) 
				&& magazine.isBonus() 
				&& magazine.isDoExternalIntegration()) {
			this.descriptionTextView
					.setText(R.string.current_subscribers_description);

			this.bonusCodeBlock.setVisibility(View.VISIBLE);
			this.externalSiteBlock.setVisibility(View.VISIBLE);
			this.orTextView.setVisibility(View.VISIBLE);
		} else if ((magazine != null) 
				&& magazine.isBonus()) {
			this.descriptionTextView
					.setText(R.string.current_subscribers_description_bonus_only);

			this.bonusCodeBlock.setVisibility(View.VISIBLE);
			this.externalSiteBlock.setVisibility(View.GONE);
			this.orTextView.setVisibility(View.GONE);
		} else {
			this.descriptionTextView
					.setText(R.string.current_subscribers_description_extrenal_integration_only);

			this.bonusCodeBlock.setVisibility(View.GONE);
			this.externalSiteBlock.setVisibility(View.VISIBLE);
			this.orTextView.setVisibility(View.GONE);
		}

		this.bonusCodeEditText.setOnEditorActionListener(returnKeyListener);
		this.passwordEditText.setOnEditorActionListener(returnKeyListener);
		this.usernameEditText.setOnEditorActionListener(returnKeyListener);

		Button closeButton = (Button) findViewById(R.id.closeButton);
		closeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Button continueButton = (Button) findViewById(R.id.submitButton);
		continueButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				submitForm();
			}
		});
	}

	private void submitForm() {
		activityView.setVisibility(View.VISIBLE);
		
		new SendBonusCodeTask().execute();
	}

	private String submitFormInNewThread() {
		String errorString = null;
		
		String bonusCode = bonusCodeEditText.getText().toString();
		String password = passwordEditText.getText().toString();
		String username = usernameEditText.getText().toString();
		if (bonusCode.isEmpty() && password.isEmpty()
				&& username.isEmpty()) {
			errorString = this.getString(R.string.current_subscribers_please_fill_required_fields);
		}		

		if ((errorString == null) && !bonusCode.isEmpty()) {
			errorString = bonusCodeSubscription(errorString, bonusCode);
		} else {

		}
		
		return errorString;
	}

	private String bonusCodeSubscription(String errorString, String bonusCode) {
		String url = Constants.SERVER_SUBSCRIPTION_URL;
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("type", "bonus_code"));
		params.add(new BasicNameValuePair("receipt", bonusCode));
		JSONObject response = HttpRequestHelper.makePostRequest(
				url, params, CurrentSubscribersActivity.this);
		if (response != null) {
			try {
				String status = response.getString("status");
				if (status.equals("success")) {
					MCPublisher publisher = MCPublisher.sharedPublisher(CurrentSubscribersActivity.this);
					publisher.getMagazine().setSubscriptionStatus(MCMagazineStatus.MCMagazineStatusPurchased);
					
					MCProduct product = publisher.issueToDownloadAfterSubscription();
					product.download();
				} else {
					errorString = response.getString("result"); 
				}
			} catch (JSONException e) {
				errorString = this.getString(R.string.cannot_save_purchase);
			}
		} else {
			errorString = this.getString(R.string.cannot_save_purchase);
		}
		return errorString;
	}
	
	private class SendBonusCodeTask extends AsyncTask<Void, Integer, String> {
		@Override
        protected String doInBackground(Void... params) {
        	String errorString = submitFormInNewThread();
 
            return errorString;
        }
        
        @Override
		protected void onPostExecute(String errorString) {
			super.onPostExecute(errorString);
			
			if (errorString == null) {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CurrentSubscribersActivity.this);
				alertDialogBuilder.setTitle(CurrentSubscribersActivity.this.getString(R.string.thank_you));
				alertDialogBuilder.setMessage(CurrentSubscribersActivity.this.getString(R.string.subscribed_successfully));
				alertDialogBuilder.setPositiveButton(CurrentSubscribersActivity.this.getString(R.string.ok), new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			        	finish();
			        }
			     });
				
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
				
			} else {
				MCAlertDialog.showErrorWithText(errorString, CurrentSubscribersActivity.this);
			}
			
			activityView.setVisibility(View.INVISIBLE);
		}
    }

	private OnEditorActionListener returnKeyListener = new OnEditorActionListener() {
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
				InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				in.hideSoftInputFromWindow(v.getApplicationWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);

				// Must return true here to consume event
				return true;

			}
			return false;
		}
	};
}
