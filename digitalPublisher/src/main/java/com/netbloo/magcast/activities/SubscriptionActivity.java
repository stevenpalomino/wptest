package com.netbloo.magcast.activities;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.constants.MCMagazineStatus;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.helpers.PlayStoreBillingHelper;
import com.netbloo.magcast.helpers.UserHelper;
import com.netbloo.magcast.models.MCMagazine;
import com.netbloo.magcast.models.MCSubscriptionProduct;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SubscriptionActivity extends BaseActivity {
	// Purchases
	PlayStoreBillingHelper playStoreBillingHelper;
	
	// Interface
	private RelativeLayout mainContainer;
	private TextView subscriptionTeaserTextView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.subscription_view_activity);

		Button closeButton = (Button) findViewById(R.id.closeButton);
		this.mainContainer = (RelativeLayout) findViewById(R.id.mainContainer);
		this.subscriptionTeaserTextView = (TextView) findViewById(R.id.subscriptionTeaserTextView);

		MCPublisher publisher = MCPublisher.sharedPublisher(this);
		MCMagazine magazine = publisher.getMagazine();
		this.subscriptionTeaserTextView.setText(magazine.getTeaser());
		this.subscriptionTeaserTextView
				.setMovementMethod(new ScrollingMovementMethod());

		closeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		// Display subscription buttons
		MCSubscriptionProduct subscriptionProduct;
		Button subscriptionButton;
		final int indexAlignment = 101;
		int currentButtonId = indexAlignment;
		for (int subscriptionProductIndex = 0; subscriptionProductIndex < publisher
				.numberOfSubscriptionProducts(); ++subscriptionProductIndex) {
			subscriptionProduct = publisher
					.subscriptionProductAtIndex(subscriptionProductIndex);

			subscriptionButton = new Button(this);
			if (subscriptionProduct != null) {
				subscriptionButton.setText(subscriptionProduct.getName());
			}
			subscriptionButton.setId(currentButtonId);
			Resources res = getResources();
			Drawable shape = res. getDrawable(R.drawable.subscribe_button);
			subscriptionButton.setBackgroundDrawable(shape);
			subscriptionButton.setTextColor(getResources().getColor(
					R.color.black_color));
			subscriptionButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					int subscriptionProductIndex = v.getId() - indexAlignment;
					MCPublisher publisher = MCPublisher
							.sharedPublisher(SubscriptionActivity.this);
					MCSubscriptionProduct subscriptionProduct = publisher
							.subscriptionProductAtIndex(subscriptionProductIndex);
					if (subscriptionProduct != null) {
						subscriptionProduct.purchase(playStoreBillingHelper);
					}
				}
			});

			addButton(subscriptionButton);

			++currentButtonId;
		}
		
		if (publisher.getMagazine().isBonus()
				|| publisher.getMagazine().isDoExternalIntegration()) {
			Button bonusCodeButton = new Button(this);
			bonusCodeButton.setId(currentButtonId);
			bonusCodeButton.setText(R.string.current_subscribers);
			Resources res = getResources();
			Drawable shape = res. getDrawable(R.drawable.bonus_code_button);
			bonusCodeButton.setBackgroundDrawable(shape);
			bonusCodeButton.setTextColor(getResources().getColor(
					R.color.white_color));
			bonusCodeButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent currentSubscribersActivityIntent = new Intent(SubscriptionActivity.this,
							CurrentSubscribersActivity.class);
					startActivity(currentSubscribersActivityIntent);
				}
			});
			addButton(bonusCodeButton);

			++currentButtonId;
		}

		if (publisher.getMagazine().isDoPrivateSubscription()) {
			UserHelper userHelper = new UserHelper(this);
			if (userHelper.isAuthenticated(this)) {
				Button signOutButton = new Button(this);
				signOutButton.setId(currentButtonId);
				signOutButton.setText(R.string.sign_out);
				Resources res = getResources();
				Drawable shape = res. getDrawable(R.drawable.bonus_code_button);
				signOutButton.setBackgroundDrawable(shape);
				signOutButton.setTextColor(getResources().getColor(
						R.color.white_color));
				signOutButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						UserHelper userHelper = new UserHelper(SubscriptionActivity.this);
						userHelper.signOut(SubscriptionActivity.this);
					}
				});
				addButton(signOutButton);

				++currentButtonId;
			} else {
				Button signUpButton = new Button(this);
				signUpButton.setId(currentButtonId);
				signUpButton.setText(R.string.sign_up);
				Resources res = getResources();
				Drawable shape = res. getDrawable(R.drawable.bonus_code_button);
				signUpButton.setBackgroundDrawable(shape);
				signUpButton.setTextColor(getResources().getColor(
						R.color.white_color));
				signUpButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent signUpActivityIntent = new Intent(SubscriptionActivity.this,
								SignUpActivity.class);
						startActivity(signUpActivityIntent);
					}
				});
				addButton(signUpButton);

				++currentButtonId;

				Button signInButton = new Button(this);
				signInButton.setId(currentButtonId);
				signInButton.setText(R.string.sign_in);
				signInButton.setBackgroundDrawable(shape);
				signInButton.setTextColor(getResources().getColor(
						R.color.white_color));
				signInButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent signInActivityIntent = new Intent(SubscriptionActivity.this,
								SignInActivity.class);
						startActivity(signInActivityIntent);
					}
				});
				addButton(signInButton);

				++currentButtonId;
			}
		}
		
		this.playStoreBillingHelper = new PlayStoreBillingHelper();
		
		LocalBroadcastManager.getInstance(this).registerReceiver(
				new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						MCPublisher publisher = MCPublisher.sharedPublisher(SubscriptionActivity.this);
						MCMagazine magazine = publisher.getMagazine();
						if ((magazine.getSubscriptionStatus() == MCMagazineStatus.MCMagazineStatusPurchased) && (playStoreBillingHelper.getContext() != null)) {
							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(playStoreBillingHelper.getContext());
							alertDialogBuilder.setTitle(context.getString(R.string.thank_you));
							alertDialogBuilder.setMessage(context.getString(R.string.subscribed_successfully));
							alertDialogBuilder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
						        public void onClick(DialogInterface dialog, int which) { 
						        	finish();
						        }
						     });
							
							AlertDialog alertDialog = alertDialogBuilder.create();
							alertDialog.show();
							
						}
					}
				}, new IntentFilter(MCSubscriptionProduct.SUBSCRIPTION_STATUS_UPDATED));

		LocalBroadcastManager.getInstance(this).registerReceiver(
				new BroadcastReceiver() {
					@Override
					public void onReceive(Context context, Intent intent) {
						SubscriptionActivity.this.finish();
					}
				}, new IntentFilter(UserHelper.SIGNED_IN));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("activity", "On activity request Subscription activity " + requestCode + " " + resultCode);
		if (requestCode == 1001) {
			Log.d("purchase", "REceived purchase result");

			this.playStoreBillingHelper.parseResponseFromPlayStore(resultCode, data);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		Log.d("activity", "Subscription activity destroy");
		
		this.playStoreBillingHelper.stopService();
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		MCPublisher publisher = MCPublisher.sharedPublisher(this);
		MCMagazine magazine = publisher.getMagazine();
		if (magazine.getSubscriptionStatus() == MCMagazineStatus.MCMagazineStatusPurchased) {
			finish();
		}

		Log.d("activity", "Subscription activity resume");
		this.playStoreBillingHelper.setContext(this);
	}

	private View previousElement = null;

	public void addButton(Button button) {
		if (this.previousElement == null) {
			this.previousElement = this.subscriptionTeaserTextView;
		}

		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.BELOW, previousElement.getId());
		lp.leftMargin = 18;
		lp.rightMargin = 18;
		lp.topMargin = 20;

		this.mainContainer.addView(button, lp);

		previousElement = button;
	}
}
