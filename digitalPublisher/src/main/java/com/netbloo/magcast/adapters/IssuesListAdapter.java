package com.netbloo.magcast.adapters;

import java.util.ArrayList;

import com.netbloo.magcast.constants.FoldersTypes;
import com.netbloo.magcast.constants.MCProductStatus;
import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.models.MCProduct;
import com.netbloo.magcast.views.IssueSmallView;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class IssuesListAdapter extends BaseAdapter {
	private Context context;
	
	// Data to display
	private int folderType;
	private ArrayList<MCProduct> productsList;
	
	public IssuesListAdapter(Context c) {
		this.context = c;
		
		this.folderType = -1;
		this.setFolderType(FoldersTypes.ALL);
	}
	
	protected void finalize() {
		Log.d("fin", "adapter fin");
	}
	
	public boolean areAllItemsEnabled()
	{
	    return false;
	}

	public boolean isEnabled(int position)
	{
	    return false;
	}
	
	public void setFolderType(int folderType) {
		if (this.folderType != folderType) {
			this.folderType = folderType;
			
			this.notifyDataSetChanged();
		}
	}

	@Override
	public void notifyDataSetChanged() {
		MCPublisher publisher = MCPublisher.sharedPublisher(this.context);
		MCProduct product = null;
		boolean addProduct = false;
		this.productsList = new ArrayList<MCProduct>();
		
		ArrayList<MCProduct> productsList = new ArrayList<MCProduct>();
		ArrayList<MCProduct> specialProductsList = new ArrayList<MCProduct>();
		for (int productIndex = 0; productIndex < publisher.numberOfIssues(); ++productIndex) {
			product = publisher.issueAtIndex(productIndex);
			if (product == null) {
				break;
			}
			
			addProduct = false;
			switch (this.folderType) {
			case FoldersTypes.ALL:
				addProduct = true;
				break;
			case FoldersTypes.SPECIAL:
				if (product.isSpecial()) {
					addProduct = true;
				}
				break;
			case FoldersTypes.SUBSCRIBERS_ONLY:
				if (product.isSubscribersOnly()) {
					addProduct = true;
				}
				break;
			case FoldersTypes.DOWNLOADS:
				if (product.getStatus() == MCProductStatus.MCProductStatusAvailable) {
					addProduct = true;
				}
				break;
			}

			if (product.isInvisible()
					&& (product.getStatus() == MCProductStatus.MCProductStatusNotAvailable)) {
				addProduct = false;
			}
			
			if (addProduct) {
				if (product.isSpecial()) {
					specialProductsList.add(product);
				} else {
					productsList.add(product);
				}
			}
		}
		
		if (productsList.size() > 0) {
			this.productsList.add(productsList.get(0));
			productsList.remove(0);
		}
		
		this.productsList.addAll(specialProductsList);
		if (productsList.size() > 0) {
			this.productsList.addAll(productsList);
		}
		
		super.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return this.productsList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Try to get view from cache
		IssueSmallView issueSmallView = (IssueSmallView) convertView;
		MCProduct product = productsList.get(position);
		// If failed to retrieve view from cache then create new one and save it in cache
		if (issueSmallView == null) {
			issueSmallView = new IssueSmallView(product, this.context);
		} else {
			issueSmallView.setProduct(product);
		}
		
		return issueSmallView;
	}

}
