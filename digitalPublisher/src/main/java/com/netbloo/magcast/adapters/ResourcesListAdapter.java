package com.netbloo.magcast.adapters;

import com.netbloo.magcast.helpers.MCPublisher;
import com.netbloo.magcast.models.MCResource;
import com.netbloo.magcast.views.ResourceCell;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class ResourcesListAdapter extends BaseAdapter {
	
	private Context context;
	
	public ResourcesListAdapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		MCPublisher publisher = MCPublisher.sharedPublisher(this.context);
		return publisher.numberOfResources();
	}

	@Override
	public Object getItem(int arg0) {
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int index, View convertView, ViewGroup parent) {
		ResourceCell resourceCell = (ResourceCell)convertView;
		if (resourceCell == null) {
			resourceCell = new ResourceCell(this.context);
		}
		
		MCPublisher publisher = MCPublisher.sharedPublisher(this.context);
		MCResource resource = publisher.resourceAtIndex(index);
		
		resourceCell.setDescription(resource.getDescription());
		resourceCell.setIconImageUrl(resource.getImagePath());
		resourceCell.setTitle(resource.getTitle());
		 
		return resourceCell;
	}

}
