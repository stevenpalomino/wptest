package com.netbloo.magcast.adapters;

import com.netbloo.magcast.entities.Article;
import com.netbloo.magcast.views.TextVersionPageFragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class TextVersionActivityPagerAdapter extends FragmentStatePagerAdapter {
	private ArrayList<Article> articlesList;
	
	public TextVersionActivityPagerAdapter(FragmentManager fragmentManager) {
		super(fragmentManager);
	}
	
	public int getPageIndexWithFilename(String filename) {
		Article article = null;
		String htmlPageFilename = null;
		for(int htmlPageIndex = 0; htmlPageIndex < this.articlesList.size(); ++htmlPageIndex) {
			article = this.articlesList.get(htmlPageIndex);
			htmlPageFilename = article.getFullPath();
			if (htmlPageFilename.contains(filename)) {
				return htmlPageIndex;
			}
		}
		
		return -1;
	}
	
	public void setArticlesList(ArrayList<Article> articlesList) {
		this.articlesList = articlesList;
	}

	@Override
	public Fragment getItem(int position) {
		return loadPage(position);
	}
	
	private TextVersionPageFragment loadPage(int position) {
		Article article = this.articlesList.get(position);
		String pageURL = "file:///" +  article.getFullPath();
		TextVersionPageFragment pageView = new TextVersionPageFragment();
		
		// Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("url", pageURL);
        pageView.setArguments(args);
		
		return pageView;
	}

	@Override
	public int getCount() {
		return this.articlesList.size();
	}
	
	
}
