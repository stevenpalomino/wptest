package com.netbloo.magcast.adapters;

import com.bebaibcebe.badabebaibcebe.R;
import com.netbloo.magcast.constants.FolderSelectItem;
import com.netbloo.magcast.constants.FoldersTypes;
import com.netbloo.magcast.helpers.MCPublisher;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;

public class FolderSelectAdapter extends BaseAdapter implements SpinnerAdapter {
	private Context context;
	
	public FolderSelectAdapter(Context c) {
		this.context = c;
	}
	
	@Override
	public int getCount() {
		int foldersCount = 2;

		MCPublisher publisher = MCPublisher.sharedPublisher(this.context);
		if (publisher.hasSpecialIssues()) {
			++foldersCount;
		}
		
		if (publisher.hasSubscribersOnlyIssues()) {
			++foldersCount;
		}
		
		return foldersCount;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		FolderSelectItem folderSelectItem = new FolderSelectItem(this.context);
		
		// Adjust item index depending on the number of folders that are available
		int itemIndex = position;
		
		MCPublisher publisher = MCPublisher.sharedPublisher(this.context);
		if ((itemIndex >= FoldersTypes.SPECIAL) && (!publisher.hasSpecialIssues())) {
			++itemIndex;
		}
		if ((itemIndex >= FoldersTypes.SUBSCRIBERS_ONLY) && (!publisher.hasSubscribersOnlyIssues())) {
			++itemIndex;
		}
		
		String itemTitle = "";
		switch (itemIndex) {
		case FoldersTypes.ALL:
			itemTitle = this.context.getString(R.string.all);
			break;
		case FoldersTypes.SPECIAL:
			itemTitle = this.context.getString(R.string.special);
			break;
		case FoldersTypes.SUBSCRIBERS_ONLY:
			itemTitle = this.context.getString(R.string.private_issues);
			break;
		case FoldersTypes.DOWNLOADS:
			itemTitle = this.context.getString(R.string.downloads);
			break;
		}
		folderSelectItem.setTitle(itemTitle);
		return folderSelectItem;
	}
}
