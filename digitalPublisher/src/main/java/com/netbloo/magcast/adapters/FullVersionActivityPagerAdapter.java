package com.netbloo.magcast.adapters;

import com.netbloo.magcast.models.MCProduct;
import com.netbloo.magcast.views.FullVersionPageFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class FullVersionActivityPagerAdapter extends FragmentStatePagerAdapter {
	private ArrayList<String> htmlPages;
	private MCProduct product;
	
	public FullVersionActivityPagerAdapter(FragmentManager fragmentManager) {
		super(fragmentManager);
	}	
	
	public ArrayList<String> getHtmlPages() {
		if (this.htmlPages == null) {
			this.htmlPages = new ArrayList<String>();
			
			File issueDirectory = new File(this.product.getDestinationPath());
			File[] files = issueDirectory.listFiles();
			String fileName = null;
			// Find enclosing directory
		    for (File possibleDirectory : files) {
		    	if (possibleDirectory.isDirectory()) {
		    		// Find html files
		    		File[] enclosingDirectoryFiles = possibleDirectory.listFiles();
		    		for (File file : enclosingDirectoryFiles) {
		    			fileName = file.getName();
				    	if (fileName.endsWith(".html")) {
				    		this.htmlPages.add(file.getPath());
				    	}
		    		}
		    		
		    		Collections.sort(this.htmlPages);
		    		break;
		    	}
		    }
		}
		
		return htmlPages;
	}
	
	public int getPageIndexWithFilename(String filename) {
		String htmlPageFilename = null;
		for(int htmlPageIndex = 0; htmlPageIndex < this.htmlPages.size(); ++htmlPageIndex) {
			htmlPageFilename = this.htmlPages.get(htmlPageIndex);
			if (htmlPageFilename.contains(filename)) {
				return htmlPageIndex;
			}
		}
		
		return -1;
	}

	public void setProduct(MCProduct product) {
		this.product = product;
	}

	@Override
	public Fragment getItem(int position) {
		return loadPage(position);
	}
	
	private FullVersionPageFragment loadPage(int position) {
		String pageURL = "file:///" +  this.htmlPages.get(position);
		FullVersionPageFragment pageView = new FullVersionPageFragment();
		
		// Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("url", pageURL);
        pageView.setArguments(args);
		
		return pageView;
	}

	@Override
	public int getCount() {
		return this.getHtmlPages().size();
	}
	
	
}
