package com.netbloo.magcast.customInterfaces;

import com.netbloo.magcast.helpers.PlayStoreBillingHelper;

public interface BillingActivity {
	PlayStoreBillingHelper getPlayStoreBillingHelper();
}
